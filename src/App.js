import React, { Component } from 'react'
import { Container, Divider, Grid, Header, Input, Select } from 'semantic-ui-react'
import worker from './TrackData/SearchTracks'
import ResultsList from './ResultsList'

export default class App extends Component {
    options = [
        { value: 'title', text: 'Song Title' }, 
        { value: 'artist', text: 'Song Artist' }, 
    ]

    constructor(props) {
        super(props)
        
        this.state = {
            displayResults : [],
            searchByError: false,
            searchTermError: false, 
            loading: false
        }
        this.searchTerm = ''
        this.searchBy = ''
    }

    componentDidMount = () => {
        this.searchWorker = new worker()
    }

    onClick = () => {
        var ret = false
        if (!this.searchBy) {
            this.setState({ searchByError: true })
            ret = true
        } else {
            this.setState({ searchByError: false })
        }
        if (!this.searchTerm) {
            this.setState({ searchTermError: true })
            ret = true
        } else {
            this.setState({ searchTermError: false })
        }
        if (ret) return
        
        this.setState({ loading: true, displayResults: [] })

        this.searchWorker.search(this.searchTerm, this.searchBy)
            .then(resp => this.setState({ displayResults: resp, loading: false }))
    }

    onSearchByChange = (e, { value }) => {
        this.setState({ searchByError: false })
        this.searchBy = value
    }

    onInputChange = (e, { value }) => {
        this.searchTerm = value
    }

    render() {
        return (
            <React.Fragment>
                <Header as='h1' content='Big Bootie Search' style={{margin: '2em 2em 2em'}} textAlign='center' />
                <Grid container columns={2} textAlign='center' stackable>
                    <Grid.Column width={12}>
                        <Input 
                            fluid 
                            action={{ icon: 'search', color: 'blue', onClick: this.onClick }} 
                            placeholder='Search' 
                            onChange={this.onInputChange} 
                            disabled={this.state.loading}
                            error={this.state.searchTermError} 
                            />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Select 
                            placeholder='Search in...' 
                            options={this.options} 
                            fluid 
                            closeOnChange 
                            closeOnEscape 
                            loading={this.state.loading}
                            disabled={this.state.loading}
                            error={this.state.searchByError}
                            onChange={this.onSearchByChange} 
                            />
                    </Grid.Column>
                </Grid>
                <Divider />
                <Container>
                    <ResultsList content={this.state.displayResults} />
                </Container>
            </React.Fragment>
        )
    }
}
