export const bb3 = {
    "title": "Two Friends - Big Bootie Mix Volume 3 2013-03-19",
    "tracks": [
        {
            "main": {
                "artist": "Hardwell ft. Amba Shepherd",
                "title": "Apollo",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Benny Benassi ft. Gary Go",
                    "title": "Cinema (Acappella)",
                    "number": "w/",
                    "timestamp": "0:04"
                }
            ]
        },
        {
            "main": {
                "artist": "Krewella",
                "title": "Alive",
                "number": "02",
                "timestamp": "1:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Krewella",
                "title": "Alive (Acappella)",
                "number": "03",
                "timestamp": "2:10"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Click Five",
                "title": "Just The Girl",
                "number": "04",
                "timestamp": "4:40"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Vicetone ft. Barack Obama",
                "title": "Hope (Instrumental Mix)",
                "number": "05",
                "timestamp": "7:00"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Martin Solveig",
                "title": "The Night Out (Acappella)",
                "number": "06",
                "timestamp": "7:31"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "deadmau5",
                "title": "Strobe",
                "number": "07",
                "timestamp": "8:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "ZIGGY & Dustin Lenji",
                "title": "Shivers",
                "number": "08",
                "timestamp": "10:07"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "P!nk",
                "title": "Blow Me (One Last Kiss)",
                "number": "09",
                "timestamp": "10:35"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sandro Silva & Quintino",
                "title": "Epic",
                "number": "10",
                "timestamp": "11:35"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Blatta & Inesha",
                "title": "My Lady Don't Mind (Modek Remix)",
                "number": "11",
                "timestamp": "11:35"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dillon Francis",
                "title": "I.D.G.A.F.O.S.",
                "number": "12",
                "timestamp": "14:50"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Beatles",
                "title": "In My Life (I Love You More)",
                "number": "13",
                "timestamp": "14:50"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Porter Robinson & Mat Zo",
                "title": "Easy (Extended Mix)",
                "number": "14",
                "timestamp": "17:27"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Daft Punk",
                "title": "One More Time (Acappella)",
                "number": "15",
                "timestamp": "17:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hujje",
                "title": "Haunting Me",
                "number": "16",
                "timestamp": "19:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "OneRepublic",
                "title": "Good Life",
                "number": "17",
                "timestamp": "20:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Clockwork",
                "title": "Airflow (Valerna Remix)",
                "number": "18",
                "timestamp": "20:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tim Berg",
                "title": "Bromance (Avicii Arena Mix)",
                "number": "19",
                "timestamp": "23:42"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Buffalo Springfield",
                "title": "For What It's Worth",
                "number": "20",
                "timestamp": "24:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Train",
                "title": "Hey, Soul Sister",
                "number": "21",
                "timestamp": "26:24"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Jakob Liedholm",
                "title": "Follow Me",
                "number": "22",
                "timestamp": "28:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Third Eye Blind",
                "title": "Jumper",
                "number": "23",
                "timestamp": "28:32"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sidney Samson",
                "title": "Dutchland",
                "number": "24",
                "timestamp": "30:45"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Paris & Simo",
                "title": "Time (Sultan + Shepard Edit)",
                "number": "25",
                "timestamp": "31:42"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Maroon 5",
                "title": "She Will Be Loved",
                "number": "26",
                "timestamp": "32:24"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Cam'ron ft. Juelz Santa, Freaky Zeekey, Toya",
                "title": "Hey Ma",
                "number": "27",
                "timestamp": "34:39"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd ft. Matthew Koma",
                "title": "Spectrum (Acappella)",
                "number": "28",
                "timestamp": "34:40"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd ft. Matthew Koma",
                "title": "Spectrum (3LAU Remix)",
                "number": "29",
                "timestamp": "35:10"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Temper Trap",
                "title": "Sweet Disposition (Acappella)",
                "number": "30",
                "timestamp": "35:55"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Archie & The Living Tombstone",
                "title": "Hush",
                "number": "31",
                "timestamp": "37:12"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Cyndi Lauper",
                "title": "Girls Just Want To Have Fun",
                "number": "32",
                "timestamp": "37:41"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Audien",
                "title": "Sup",
                "number": "33",
                "timestamp": "38:41"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Francesco Diaz & Jeff Rock",
                "title": "Alella",
                "number": "34",
                "timestamp": "39:42"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ray J ft. Yung Berg",
                "title": "Sexy Can I",
                "number": "35",
                "timestamp": "39:55"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "James Egbert",
                "title": "Isle Of Capri",
                "number": "36",
                "timestamp": "41:26"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii & Sebastien Drums",
                "title": "Even",
                "number": "37",
                "timestamp": "42:01"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "NERVO ft. Ollie James",
                "title": "Irresistible (Acappella)",
                "number": "38",
                "timestamp": "42:54"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "JAY-Z & Kanye West",
                "title": "Niggas In Paris",
                "number": "39",
                "timestamp": "44:46"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Swedish House Mafia",
                "title": "One (Congorock Remix)",
                "number": "40",
                "timestamp": "45:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Moiez",
                "title": "You Only Live Once",
                "number": "41",
                "timestamp": "47:19"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Coldplay",
                "title": "Clocks (Acappella)",
                "number": "42",
                "timestamp": "48:03"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Alesso",
                "title": "Clash",
                "number": "43",
                "timestamp": "49:37"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fun. ft. Janelle Monae",
                "title": "We Are Young (Acappella)",
                "number": "44",
                "timestamp": "50:03"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hujje",
                "title": "Agua",
                "number": "45",
                "timestamp": "53:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Vertical Horizon",
                "title": "Everything You Want",
                "number": "46",
                "timestamp": "54:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii ft. Andreas Moe",
                "title": "Fade Into Darkness (Instrumental Mix)",
                "number": "47",
                "timestamp": "55:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Killers",
                "title": "Smile Like You Mean It",
                "number": "48",
                "timestamp": "56:12"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fedde Le Grand & Nicky Romero",
                "title": "Sparks (Aluko Bootleg)",
                "number": "49",
                "timestamp": "58:00"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Katy Perry",
                "title": "Teenage Dream",
                "number": "50",
                "timestamp": "58:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hardwell",
                "title": "The World",
                "number": "51",
                "timestamp": "1:00:07"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Calvin Harris ft. Florence Welch",
                "title": "Sweet Nothing (Acappella)",
                "number": "52",
                "timestamp": "1:00:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sander van Doorn & Adrian Lux",
                "title": "Eagles",
                "number": "53",
                "timestamp": "1:01:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ferry Corsten",
                "title": "Silfra",
                "number": "54",
                "timestamp": "1:02:25"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Adele",
                "title": "Set Fire To The Rain (Acappella)",
                "number": "55",
                "timestamp": "1:02:47"
            },
            "played_with": []
        }
    ]
}