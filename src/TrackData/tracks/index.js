import { bb1 } from './bb_1'
import { bb2 } from './bb_2'
import { bb3 } from './bb_3'
import { bb4 } from './bb_4'
import { bb5 } from './bb_5'
import { bb6 } from './bb_6'
import { bb7 } from './bb_7'
import { bb8 } from './bb_8'
import { bb9 } from './bb_9'
import { bb10 } from './bb_10'
import { bb11 } from './bb_11'
import { bb12 } from './bb_12'
import { bb13 } from './bb_13'
import { bb14 } from './bb_14'
import { bb15 } from './bb_15'
import { bb16 } from './bb_16'
import { bb17 } from './bb_17'
import { bb18 } from './bb_18'
import { bb19 } from './bb_19'
import { bb20 } from './bb_20'

let mixes = {
    'bb1': bb1,
    'bb2': bb2,
    'bb3': bb3,
    'bb4': bb4,
    'bb5': bb5,
    'bb6': bb6,
    'bb7': bb7,
    'bb8': bb8,
    'bb9': bb9,
    'bb10': bb10,
    'bb11': bb11,
    'bb12': bb12,
    'bb13': bb13,
    'bb14': bb14,
    'bb15': bb15,
    'bb16': bb16,
    'bb17': bb17,
    'bb18': bb18, 
    'bb19': bb19, 
    'bb20': bb20
}

let trackNameMap = {};
[...Array(20).keys()].map(x => trackNameMap[`bb${x + 1}`] = `Big Bootie Mix Vol. ${x + 1}`)

export {
    mixes, 
    trackNameMap
}