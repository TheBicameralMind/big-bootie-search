export const bb20 = {
    "title": "Two Friends - Big Bootie Mix 020 2021-10-25",
    "tracks": [
        {
            "main": {
                "artist": "Skulkids",
                "title": "Never Heal",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Dominic Fike",
                    "title": "3 Nights",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Olivia Rodrigo",
                    "title": "Good 4 U",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Notorious B.I.G.",
                    "title": "Juicy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dualities",
                "title": "Lose Myself In You",
                "number": "02",
                "timestamp": "2:22"
            },
            "played_with": [
                {
                    "artist": "Bob Marley & The Wailers",
                    "title": "Buffalo Soldier",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Plastik Funk & Sickrate & Rentz & Repiet",
                    "title": "Never Let Go",
                    "number": "w/",
                    "timestamp": "2:52"
                },
                {
                    "artist": "Ramz",
                    "title": "Barking",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Damon Sharpe & Josh Cumbee",
                "title": "Lost Years (Jeonghyeon Remix)",
                "number": "03",
                "timestamp": "3:22"
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "In My Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BTS",
                    "title": "Butter",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tal Bachman",
                    "title": "She's So High",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "La Roux",
                "title": "Bulletproof (Gamper & Dadoni Remix)",
                "number": "04",
                "timestamp": "4:41"
            },
            "played_with": [
                {
                    "artist": "Chance The Rapper",
                    "title": "Same Drugs",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Akon",
                    "title": "I'm So Paid",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Joel Corry & RAYE & David Guetta",
                    "title": "BED (Toby Romeo Remix)",
                    "number": "w/",
                    "timestamp": "5:27"
                },
                {
                    "artist": "Trinidad Cardona",
                    "title": "Dinero",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift",
                    "title": "22",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hozier",
                    "title": "Take Me To Church (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Raven & Kreyn",
                    "title": "Lift Your Voices",
                    "number": "w/",
                    "timestamp": "6:44"
                },
                {
                    "artist": "Chris Kirkpatrick",
                    "title": "My Shiny Teeth And Me (The Fairly Oddparents OST)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Tungevaag & SICK INDIVIDUALS & Marf",
                "title": "Miss You",
                "number": "06",
                "timestamp": "7:13"
            },
            "played_with": [
                {
                    "artist": "Olivia Rodrigo",
                    "title": "Deja Vu",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mooski",
                    "title": "Track Star",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maggie Rogers",
                    "title": "Light On",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dexy's Midnight Runners",
                    "title": "Come On Eileen",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shouse",
                    "title": "Love Tonight",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Red Hot Chili Peppers",
                "title": "Otherside (Two Friends Remix)",
                "number": "08",
                "timestamp": "10:09"
            },
            "played_with": [
                {
                    "artist": "The Weeknd",
                    "title": "Save Your Tears",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Flux Pavilion",
                    "title": "Got 2 Know",
                    "number": "w/",
                    "timestamp": "11:07"
                },
                {
                    "artist": "Drake",
                    "title": "Over (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Giveon",
                    "title": "Heartbreak Anniversary",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sean Paul",
                    "title": "Temperature (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ellie Goulding",
                    "title": "Still Falling For You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dualities",
                "title": "For Your Love",
                "number": "10",
                "timestamp": "13:00"
            },
            "played_with": [
                {
                    "artist": "The All-American Rejects",
                    "title": "It Ends Tonight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MOTi",
                    "title": "Everything Cool",
                    "number": "w/",
                    "timestamp": "13:35"
                },
                {
                    "artist": "Shania Twain",
                    "title": "Man! I Feel Like A Woman!",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Death Cab For Cutie",
                    "title": "I Will Possess Your Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cheat Codes & Dante Klein",
                    "title": "Let Me Hold You (Turn Me On)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Derulo",
                    "title": "Don't Wanna Go Home",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DJ Press Play & Sunday Scaries & Curry Cartel",
                "title": "Xtasy",
                "number": "12",
                "timestamp": "15:27"
            },
            "played_with": [
                {
                    "artist": "Train",
                    "title": "Drops Of Jupiter",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Regard & Troye Sivan & Tate McRae",
                    "title": "You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ed Sheeran",
                    "title": "Bad Habits (Dominic Strike Remix)",
                    "number": "w/",
                    "timestamp": "16:20"
                }
            ]
        },
        {
            "main": {
                "artist": "Kastra & TRIVD",
                "title": "Savage",
                "number": "13",
                "timestamp": "16:50"
            },
            "played_with": [
                {
                    "artist": "Infant Sorrow",
                    "title": "Inside Of You (Forgetting Sarah Marshall OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BabyJake",
                    "title": "Cigarettes On Patios",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Charlie Puth",
                    "title": "How Long (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": "17:50"
                },
                {
                    "artist": "Jimmy Buffett & Alan Jackson",
                    "title": "It's Five O'Clock Somewhere",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ed Sheeran",
                    "title": "Bad Habits",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jeremy Zucker & Chelsea Cutler",
                    "title": "you were good to me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Adele",
                    "title": "Rolling In The Deep (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Enrique Iglesias",
                    "title": "Tonight (I'm Fucking You)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ALOK & Sevenn",
                    "title": "BYOB",
                    "number": "w/",
                    "timestamp": "20:25"
                },
                {
                    "artist": "Danny Brown",
                    "title": "Grown Up",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Erlandsson",
                "title": "Young Again",
                "number": "16",
                "timestamp": "20:39"
            },
            "played_with": [
                {
                    "artist": "Black Eyed Peas",
                    "title": "Just Can't Get Enough (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Corona",
                    "title": "The Rhythm Of The Night (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "COIN",
                    "title": "Talk Too Much",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Masked Wolf",
                    "title": "Astronaut In The Ocean",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Aileen Quinn, Toni Ann Gisondi & The Cast Of Annie",
                    "title": "It's The Hard Knock Life (Annie OST)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Wildvibes & Martin Miller",
                "title": "Under Tonight",
                "number": "18",
                "timestamp": "22:58"
            },
            "played_with": [
                {
                    "artist": "30 Seconds To Mars",
                    "title": "The Kill (Bury Me)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Williams & Jonas Aden",
                    "title": "I Hope You Know",
                    "number": "w/",
                    "timestamp": "23:52"
                },
                {
                    "artist": "The Beatles",
                    "title": "I Want To Hold Your Hand",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Snakehips & M\u00d8",
                "title": "Don't Leave (Throttle Remix)",
                "number": "19",
                "timestamp": "24:21"
            },
            "played_with": [
                {
                    "artist": "Oasis",
                    "title": "Wonderwall (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sabrina Carpenter",
                    "title": "Skin",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Journey",
                "title": "Don't Stop Believin'",
                "number": "20",
                "timestamp": "25:57"
            },
            "played_with": [
                {
                    "artist": "Sum 41",
                    "title": "In Too Deep (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alicia Keys",
                    "title": "No One",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "VINNE",
                    "title": "Tell Me",
                    "number": "w/",
                    "timestamp": "26:45"
                },
                {
                    "artist": "Billie Eilish",
                    "title": "ocean eyes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wiz Khalifa",
                    "title": "Work Hard Play Hard",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber",
                    "title": "Anyone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Bangles",
                    "title": "Manic Monday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dastic",
                    "title": "Figure It Out",
                    "number": "w/",
                    "timestamp": "29:26"
                }
            ]
        },
        {
            "main": {
                "artist": "Shawn Mendes",
                "title": "If I Can't Have You (Gryffin Remix)",
                "number": "23",
                "timestamp": "29:56"
            },
            "played_with": [
                {
                    "artist": "The Temper Trap",
                    "title": "Love Lost",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Black Eyed Peas",
                    "title": "Let's Get It Started",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Temper Trap",
                    "title": "Sweet Disposition (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SICK INDIVIDUALS",
                    "title": "Dance With Me (Addiel LS & Kristianex Remix)",
                    "number": "w/",
                    "timestamp": "30:50"
                },
                {
                    "artist": "MK",
                    "title": "17",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Jensen & James Arthur",
                    "title": "Nobody",
                    "number": "w/",
                    "timestamp": "31:20"
                },
                {
                    "artist": "Machine Gun Kelly",
                    "title": "Bloody Valentine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kelly Clarkson",
                    "title": "My Life Would Suck Without You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Danny Avila",
                "title": "End Of The Night (Danny Avila Club Mix)",
                "number": "24",
                "timestamp": "32:03"
            },
            "played_with": [
                {
                    "artist": "Beyonc\u00e9",
                    "title": "Halo",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alicia Keys",
                    "title": "Girl On Fire",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Click Five",
                    "title": "Just The Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dua Lipa & Ang\u00e8le",
                    "title": "Fever (RudeLies Remix)",
                    "number": "w/",
                    "timestamp": "33:11"
                },
                {
                    "artist": "A Flock Of Seagulls",
                    "title": "I Ran",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justice vs. Simian",
                    "title": "We Are Your Friends (Switch Disco Edit)",
                    "number": "w/",
                    "timestamp": "34:36"
                },
                {
                    "artist": "Linkin Park",
                    "title": "Shadow Of The Day (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Elliot Yamin",
                    "title": "Wait For You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bowling For Soup",
                "title": "1985",
                "number": "28",
                "timestamp": "36:44"
            },
            "played_with": [
                {
                    "artist": "Spice Girls",
                    "title": "Wannabe (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto",
                    "title": "The Business",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & Ty Dolla $ign",
                    "title": "The Business, Pt. II",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Aloe Blacc",
                    "title": "I Need A Dollar",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Marshmello & Halsey",
                "title": "Be Kind (Surf Mesa Remix)",
                "number": "29",
                "timestamp": "38:05"
            },
            "played_with": [
                {
                    "artist": "Kanye West",
                    "title": "Jail",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sublime",
                    "title": "Santeria",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "SICK INDIVIDUALS",
                "title": "All Of My Heart",
                "number": "30",
                "timestamp": "39:06"
            },
            "played_with": [
                {
                    "artist": "Phillip Phillips",
                    "title": "Home",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wahlstedt",
                    "title": "So Good",
                    "number": "w/",
                    "timestamp": "39:55"
                }
            ]
        },
        {
            "main": {
                "artist": "CARSTN & DAZZ",
                "title": "Worlds Apart (Jeonghyeon Remix)",
                "number": "31",
                "timestamp": "40:26"
            },
            "played_with": [
                {
                    "artist": "Lifehouse",
                    "title": "Hanging By A Moment",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Pierce Fulton",
                    "title": "Kuaga",
                    "number": "w/",
                    "timestamp": "41:19"
                },
                {
                    "artist": "Sigala & James Arthur",
                    "title": "Lasting Lover",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Verve",
                "title": "Bittersweet Symphony",
                "number": "32",
                "timestamp": "41:49"
            },
            "played_with": [
                {
                    "artist": "Illenium & Iann Dior",
                    "title": "First Time",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Coldplay",
                    "title": "Clocks (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "4B & NvrLeft",
                    "title": "Dope",
                    "number": "w/",
                    "timestamp": "42:39"
                },
                {
                    "artist": "UNK",
                    "title": "Walk It Out",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bloodhound Gang",
                    "title": "The Bad Touch",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rick Springfield",
                    "title": "Jessie's Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "twoDB",
                    "title": "Loving You",
                    "number": "w/",
                    "timestamp": "43:46"
                },
                {
                    "artist": "Enya",
                    "title": "Only Time",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lauv & Lany",
                    "title": "Mean It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Hunt",
                    "title": "Break Up In A Small Town",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lucas & Steve",
                    "title": "Say Something (RetroVision Remix)",
                    "number": "w/",
                    "timestamp": "46:40"
                },
                {
                    "artist": "Iyaz",
                    "title": "Replay",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Embody & Iggi Kelly & Louis III",
                "title": "No Time To Stress",
                "number": "36",
                "timestamp": "47:10"
            },
            "played_with": [
                {
                    "artist": "Nelly",
                    "title": "Just A Dream",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Feldt & Sam Fischer",
                    "title": "Pick Me Up",
                    "number": "w/",
                    "timestamp": "47:56"
                },
                {
                    "artist": "Kid Rock",
                    "title": "All Summer Long",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ellie Goulding",
                    "title": "Lights (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kevin Gates",
                    "title": "2 Phones",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alan Walker",
                    "title": "Alone",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lucas Estrada & Blinded Hearts & Henrik Hoven",
                "title": "Don't Know Where To Go (Dualities Remix)",
                "number": "38",
                "timestamp": "49:41"
            },
            "played_with": [
                {
                    "artist": "Tai Verdes",
                    "title": "Stuck In The Middle",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dispatch",
                    "title": "The General",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & Tina Turner",
                    "title": "What's Love Got To Do With It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dimitri Vegas & Like Mike & Martin Garrix",
                "title": "Tremor (Sensation 2014 Anthem)",
                "number": "39",
                "timestamp": "50:53"
            },
            "played_with": [
                {
                    "artist": "Britney Spears",
                    "title": "Gimme More",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Hogland",
                "title": "Telling Me Yes",
                "number": "41",
                "timestamp": "53:31"
            },
            "played_with": [
                {
                    "artist": "Kanye West",
                    "title": "Homecoming",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sean Kingston",
                    "title": "Beautiful Girls (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Life Of Dillon",
                "title": "Overload",
                "number": "42",
                "timestamp": "54:55"
            },
            "played_with": [
                {
                    "artist": "Sultan + Shepard & Showtek",
                    "title": "Way We Used 2",
                    "number": "w/",
                    "timestamp": "55:49"
                },
                {
                    "artist": "Matt Nathanson",
                    "title": "Come On Get Higher",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "50 Cent",
                    "title": "In Da Club (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nappy Roots",
                    "title": "Good Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Selena Gomez",
                    "title": "Lose You To Love Me (Elijah Hill X Drew Wilken Remix)",
                    "number": "w/",
                    "timestamp": "57:12"
                },
                {
                    "artist": "Charli XCX",
                    "title": "Boom Clap",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin & Audrey Mika",
                "title": "Safe With Me",
                "number": "44",
                "timestamp": "57:31"
            },
            "played_with": [
                {
                    "artist": "Anna Kendrick",
                    "title": "Cups (When I'm Gone) (Pitch Perfect OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "3 Doors Down",
                    "title": "When I'm Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "When I'm Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta & Brooks & Loote",
                    "title": "Better When You're Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Goo Goo Dolls",
                    "title": "Here Is Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kelly Clarkson",
                    "title": "Since U Been Gone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD",
                    "title": "Wishing Well",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Kid LAROI & Justin Bieber",
                    "title": "Stay",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Camila Cabello",
                    "title": "Never Be The Same",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tove Lo",
                    "title": "Habits (Stay High) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Smith",
                    "title": "Stay With Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cheat Codes & Bryce Vine",
                    "title": "Stay",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eddie Money",
                    "title": "Take Me Home Tonight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Phillip Phillips",
                    "title": "Home",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Edward Sharpe & The Magnetic Zeros",
                    "title": "HOME",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}