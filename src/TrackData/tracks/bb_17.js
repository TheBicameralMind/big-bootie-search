export const bb17 = {
    "title": "Two Friends - Big Bootie Mix Volume 17 2020-04-09",
    "tracks": [
        {
            "main": {
                "artist": "My Chemical Romance",
                "title": "Welcome To The Black Parade",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Swedish House Mafia ft. John Martin",
                    "title": "Don't You Worry Child (Instrumental)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dua Lipa",
                    "title": "Don't Start Now",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Goo Goo Dolls",
                    "title": "Iris",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars",
                    "title": "Ready Or Not",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Machine Gun Helly & YUNGBLUD & Travis Barker",
                    "title": "I Think I'm Okay",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DallasK",
                "title": "I Know",
                "number": "02",
                "timestamp": "2:18"
            },
            "played_with": [
                {
                    "artist": "Trevor Daniel",
                    "title": "Falling",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Roddy Ricch",
                    "title": "The Box",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jordan Jay ft. Charlie Ray & David Jarvis",
                    "title": "Drive Me Home (Official Animal Sound 2019 Anthem)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lucenzo & Qwote ft. Pitbull & Don Omar",
                    "title": "Danza Kuduro (Throw Your Hands Up)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Halsey",
                "title": "You Should Be Sad (Ti\u00ebsto Remix)",
                "number": "03",
                "timestamp": "3:43"
            },
            "played_with": [
                {
                    "artist": "Daft Punk",
                    "title": "One More Time (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vance Joy",
                    "title": "Riptide (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zook\u00ebper",
                    "title": "House Phone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake ft. Lil Wayne & Tyga",
                    "title": "The Motto",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Nirvana",
                "title": "Smells Like Teen Spirit (DJ Vincenzino Umberto Edit)",
                "number": "04",
                "timestamp": "5:08"
            },
            "played_with": [
                {
                    "artist": "Zedd ft. Foxes",
                    "title": "Clarity (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Arizona Zervas",
                    "title": "Roxanne",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jay Eskar",
                    "title": "Path",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna",
                    "title": "Don't Stop The Music",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "3LAU & Justin Caruso ft. Iselin",
                "title": "Better With You (Kastra & twoDB Remix)",
                "number": "05",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Mac Miller",
                    "title": "Senior Skip Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Calling",
                    "title": "Wherever You Will Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jordan Jay",
                    "title": "Give Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "Lying Is The Most Fun A Girl Can Have Without Taking Her Clothes Off",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Don Diablo ft. Nate Dogg",
                "title": "I Got Love",
                "number": "06",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Boulevard Of Broken Dreams",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lupe Fiasco ft. Guy Sebastian",
                    "title": "Battle Scars",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Jackson 5",
                    "title": "ABC",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix & Julian Jordan",
                "title": "Glitch",
                "number": "07",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Lizzo",
                    "title": "Juice",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Death Cab For Cutie",
                    "title": "I Will Follow You Into The Dark",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Williams",
                    "title": "I Got You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Selena Gomez",
                    "title": "Lose You To Love Me",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RL Grime ft. 24hrs",
                "title": "UCLA",
                "number": "08",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Frank Ocean",
                    "title": "Thinking About You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "First Date (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Freestylers ft. Belle Humble",
                    "title": "Cracks (Flux Pavilion Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Young Jeezy ft. Kanye West",
                    "title": "Put On",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin ft. Maia Wright",
                "title": "Body Back",
                "number": "09",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Tones And I",
                    "title": "Dance Monkey",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hoodie Allen",
                    "title": "No Interruption",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin ft. Maia Wright",
                    "title": "Body Back (Deniz Koyu Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Door Cinema Club",
                    "title": "Undercover Martyn",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Flo Rida ft. Sia",
                "title": "Wild Ones",
                "number": "10",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Sugar Ray",
                    "title": "Every Morning",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "J. Cole",
                    "title": "No Role Modelz",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jack Back ft. David Guetta & Nicky Romero & Sia",
                    "title": "Wild One Two",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "John Mellencamp",
                    "title": "Jack & Diane",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jay Hardway & MOTi ft. Babet",
                    "title": "Wired",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fingere Eleven",
                    "title": "Paralyzer",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers & Illenium ft. Lennon Stella",
                "title": "Takeaway",
                "number": "11",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Marvin Gaye",
                    "title": "Sexual Healing",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z ft. Kanye West & Rihanna",
                    "title": "Run This Town",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. Pusha T",
                    "title": "Runaway",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dan + Shay ft. Justin Bieber",
                "title": "10,000 Hours (Salasnich Remix)",
                "number": "12",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "ZAYN & Zhavia Ward",
                    "title": "A Whole New World (Aladdin OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Elton John",
                    "title": "Rocket Man",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RetroVision",
                    "title": "Stop",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hilary Duff",
                    "title": "Come Clean",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis & Hook N Sling",
                "title": "Love On Me",
                "number": "13",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Gotye ft. Kimbra",
                    "title": "Somebody That I Used To Know (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quinn XCII ft. Chelsea Cutler",
                    "title": "Flare Guns",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Kane Brown",
                    "title": "One Thing Right",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zara Larsson",
                    "title": "All The Time (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Miike Snow",
                    "title": "Animal (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kaskade ft. Mindy Gledhill",
                "title": "Eyes",
                "number": "14",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Post Malone",
                    "title": "Circles",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Howie Day",
                    "title": "Collide",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kid Cudi",
                    "title": "Soundtrack 2 My Life",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Don Diablo",
                "title": "Momentum",
                "number": "15",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Britney Spears",
                    "title": "If U Seek Amy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ron Jones & Butch Hartman",
                    "title": "The Fairly OddParents! Theme Song",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sagan",
                    "title": "Robo",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cobra Starship ft. Leighton Meester",
                    "title": "Good Girls Go Bad",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "Adam's Song",
                "number": "16",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Usher ft. Young Jeezy",
                    "title": "Love In This Club",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Solveig & The Cataracs ft. Kyle",
                    "title": "Hey Now",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182 vs. Sebastian Ingrosso & Tommy Trash vs. Angemi",
                    "title": "Adam's Song Reload (Corrupt Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bishu",
                    "title": "Out Here",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kendrick Lamar",
                    "title": "HUMBLE. (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso vs. OneRepublic",
                "title": "If I Lose Myself",
                "number": "17",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Wiz Khalifa ft. Snoop Dogg & Chris Brown",
                    "title": "Young, Wild and Free",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eddie Money",
                    "title": "Take Me Home Tonight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SIKS",
                    "title": "Maybe You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MKTO",
                    "title": "Classic",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "MEDUZA X Becky Hill X GOODBOYS vs. Calvin Harris X Ellie Goulding",
                "title": "I Need Your Control (Rudeejay & Da Brozz X Raffa Mash-Boot)",
                "number": "18",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Khalid",
                    "title": "Better",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem ft. Nate Dogg",
                    "title": "Till I Collapse",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Marshmello ft. CHVRCHES",
                "title": "Here With Me",
                "number": "19",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Justin Timberlake",
                    "title": "Mirrors",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "Under The Bridge",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends",
                    "title": "Take It Off (Wyle Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Soulja Boy ft. Sammie",
                    "title": "Kiss Me Thru The Phone",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Say Lou Lou",
                "title": "Julian (The Chainsmokers Remix)",
                "number": "20",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Hoodie Allen ft. Jhameel",
                    "title": "No Faith In Brooklyn",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kid Cudi ft. Kanye West",
                    "title": "Erase Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Williams ft. Moa Lisa",
                    "title": "Make You Mine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Christina Milian",
                    "title": "Call Me, Beep Me (Kim Possible Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dirty South & Alesso ft. Ruben Haze",
                "title": "City Of Dreams",
                "number": "21",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Loud Luxury & Bryce Vine",
                    "title": "I'm Not Alright",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Proclaimers",
                    "title": "I'm Gonna Be (500 Miles)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "R3HAB & Mike Williams",
                    "title": "Lullaby",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kesha",
                    "title": "We R Who We R",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Elephante ft. Trouze & Damon Sharpe",
                "title": "Age Of Innocence",
                "number": "22",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Eddy Grant",
                    "title": "Electric Avenue",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dan + Shay ft. Justin Bieber",
                    "title": "10,000 Hours",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Merk & Kremont",
                    "title": "Turn It Around",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lupe Fiasco ft. Matthew Santos",
                    "title": "Superstar",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "J-Kwon",
                    "title": "Tipsy",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Famba ft. Kyra Mastro",
                "title": "Storm",
                "number": "23",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Swedish House Mafia ft. John Martin",
                    "title": "Don't You Worry Child (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna",
                    "title": "Disturbia",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "Save A Little Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5 ft. Christina Aguilera",
                    "title": "Moves Like Jagger (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta ft. Akon",
                "title": "Sexy Bitch (SLTRY Remix)",
                "number": "24",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "David Guetta ft. Akon",
                    "title": "Sexy Bitch",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chance The Rapper ft. Vic Mensa & Twista",
                    "title": "Cocoa Butter Kisses",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gwen Stefani ft. Akon",
                    "title": "The Sweet Escape",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sagan ft. Sam Russell",
                    "title": "Lowly",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Pussycat Dolls ft. Busta Rhymes",
                    "title": "Don't Cha",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Panic! At The Disco",
                "title": "High Hopes (Don Diablo Remix)",
                "number": "25",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Jack Johnson",
                    "title": "Better Together",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cherub",
                    "title": "Doses And Mimosas",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars",
                    "title": "Put Em Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jennifer Lopez ft. Pitbull",
                    "title": "On The Floor",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Michael Calfan",
                "title": "Nobody Does It Better",
                "number": "26",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Usher ft. Alicia Keys",
                    "title": "My Boo",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tove Lo",
                    "title": "Habits (Stay High) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tchami & Malaa",
                    "title": "Kurupt",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "A$AP Rocky ft. Drake & Kendrick Lamar & 2 Chainz",
                    "title": "Fuckin' Problems",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Salasnich & Blanee",
                "title": "Take Me",
                "number": "27",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Supremes",
                    "title": "Stop! In The Name Of Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Lumineers",
                    "title": "Ho Hey",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Robert Falcon vs. Raven & Kreyn",
                    "title": "Sunny",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fitz & The Tantrums",
                    "title": "Out Of My League",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "BEAUZ ft. Momo",
                "title": "Won't Look Back",
                "number": "28",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Lifehouse",
                    "title": "You And Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fergie",
                    "title": "Big Girls Don't Cry (Personal)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dropgun ft. Eddie Jonsson",
                    "title": "A Little More Like You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ant Saunders",
                    "title": "Yellow Hearts",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Madison Mars ft. Klara",
                "title": "I Will Let You Down",
                "number": "29",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Matoma ft. Becky Hill",
                    "title": "False Alarm",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Smallpools",
                    "title": "Dreaming",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta & Martin Garrix & Brooks",
                    "title": "Like I Do",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fifth Harmony & Kid Ink",
                    "title": "Worth It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Matoma ft. Astrid S",
                "title": "Running Out",
                "number": "30",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Gavin Degraw",
                    "title": "Not Over You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD",
                    "title": "Lucid Dreams",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RL Grime",
                    "title": "Pressure",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. Big Sean & JAY Z",
                    "title": "Clique (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Manse & Yaro ft. Philip Strand",
                "title": "Summer Rain",
                "number": "31",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Lauv ft. Anne-Marie",
                    "title": "Fuck I'm Lonely",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ella Henderson",
                    "title": "Ghost",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eric Prydz",
                    "title": "Call On Me (Kepa Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Haddaway",
                    "title": "What Is Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii & Nicky Romero ft. Noonie Bao",
                "title": "I Could Be The One",
                "number": "32",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Colbie Caillat",
                    "title": "Bubbly",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blackbear",
                    "title": "Hot Girl Bummer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii & Nicky Romero ft. Noonie Bao",
                    "title": "I Could Be The One (Audrio Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "CALVO",
                    "title": "Be Alright",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chiddy Bang",
                    "title": "Opposite Of Adults",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Landis",
                "title": "Nobody Like You",
                "number": "33",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Bob Seger",
                    "title": "Old Time Rock & Roll",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shaggy ft. Rayvon",
                    "title": "Angel",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Landis",
                    "title": "Nobody Like You (RetroVision Flip)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Grouplove",
                    "title": "Ways To Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Guns N' Roses",
                "title": "Sweet Child O' Mine",
                "number": "34",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Guns N' Roses",
                    "title": "Sweet Child O' Mine (Jango Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bingo Players",
                    "title": "Cry (Just A Little) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Stronger",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone ft. Quavo",
                    "title": "Congratulations (Dzeko Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "50 Cent ft. Nate Dogg",
                    "title": "21 Questions",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ed Sheeran & Justin Bieber",
                "title": "I Don't Care (Salasnich Remix)",
                "number": "35",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "George Ezra",
                    "title": "Budapest",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Whitney Houston",
                    "title": "How Will I Know (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Brooks ft. Gia Koka",
                    "title": "Say A Little Prayer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Lumineers",
                    "title": "Ophelia",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers & 5 Seconds of Summer",
                "title": "Who Do You Love",
                "number": "36",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "DJ Sammy & Yanou ft. Do",
                    "title": "Heaven",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Waka Flocka Flame ft. Wale & Roscoe Dash",
                    "title": "No Hands",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers & 5 Seconds of Summer",
                    "title": "Who Do You Love (R3HAB Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta & Avicii",
                "title": "Sunshine",
                "number": "37",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Taio Cruz ft. Ludacris",
                    "title": "Break Your Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jesse Frederick",
                    "title": "Everywhere You Look (Full House Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Manse & JRL ft. Jonny Rose",
                    "title": "Outlaws",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lewis Capaldi",
                    "title": "Bruises",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends",
                "title": "ID",
                "number": "38",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Disco Killerz ft. Delaney Jane & Sarah Charness",
                "title": "Beautiful Life",
                "number": "39",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Blackbear",
                    "title": "Me & Ur Ghost",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z ft. Alicia Keys",
                    "title": "Empire State Of Mind (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Feldt ft. Rani",
                    "title": "Post Malone (Joe Stone Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nelly",
                    "title": "Hot In Herre",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Valerie Broussard & Galantis",
                "title": "Roots",
                "number": "40",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "All You Need Is Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Empire Of The Sun",
                    "title": "Walking On A Dream (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "CALVO",
                    "title": "Panda Bounce",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin ft. Maia Wright",
                    "title": "Body Back",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta ft. Usher",
                "title": "Without You",
                "number": "41",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "San Holo",
                    "title": "Light",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chelsea Cutler",
                    "title": "Your Shirt",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shwayze & Cisco Adler",
                    "title": "Corona & Lime",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & Chelsea Cutler",
                    "title": "Not Ok (Frank Walker Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BTS ft. Halsey",
                    "title": "Boy With Luv",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Diplo & Jonas Brothers",
                "title": "Lonely",
                "number": "42",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Wiz Khalifa ft. Charlie Puth",
                    "title": "See You Again (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bill Withers",
                    "title": "Lean On Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Anne-Marie",
                    "title": "FRIENDS",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justice vs. Simian",
                    "title": "We Are Your Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna ft. JAY Z",
                    "title": "Umbrella",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Randy Newman",
                    "title": "You've Got A Friend In Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vitamin C",
                    "title": "Graduation (Friends Forever)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Foster The People",
                    "title": "Best Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber & BloodPop",
                    "title": "Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Biz Markie",
                    "title": "Just A Friend",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Queen",
                    "title": "You're My Best Friend",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. JUSCOVA & James Delaney",
                    "title": "With My Homies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madeon",
                    "title": "All My Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Spice Girls",
                    "title": "Wannabe (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Hans Zimmer",
                "title": "Time (Inception OST)",
                "number": "43",
                "timestamp": "59:56"
            },
            "played_with": []
        }
    ]
}