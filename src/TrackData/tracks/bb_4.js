export const bb4 = {
    "title": "Two Friends - Big Bootie Mix Vol. 4 2013-08-15",
    "tracks": [
        {
            "main": {
                "artist": "Ying Yang Twins ft. Mike Jones & Mr. Collipark",
                "title": "Badd",
                "number": "01",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "EDX ft. John Williams",
                "title": "Give It Up For Love (Mysto & Pizzi Remix)",
                "number": "02",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dirty South & Michael Brun",
                "title": "Rift",
                "number": "03",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Muse",
                    "title": "Starlight (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Milk N Cooks",
                "title": "Ghosts",
                "number": "04",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Florence + The Machine",
                    "title": "Shake It Out (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Neon Trees",
                "title": "Animal",
                "number": "05",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tmare",
                "title": "Coloring Book",
                "number": "06",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "JAY-Z ft. Mr. Hudson",
                "title": "Forever Young",
                "number": "07",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Project 46",
                "title": "Ekho",
                "number": "08",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Pendulum",
                    "title": "The Island Pt. 1 (Dawn) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Pendulum",
                "title": "The Island Pt. 1 (Dawn)",
                "number": "09",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kansas",
                "title": "Dust In The Wind",
                "number": "10",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "M83",
                "title": "Midnight City (Eric Prydz Private Remix)",
                "number": "11",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "OneRepublic",
                    "title": "If I Lose Myself (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zara Larsson",
                "title": "Uncover (Richello Remix)",
                "number": "12",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Imagine Dragons",
                    "title": "It's Time",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Z3ro",
                "title": "Hunting Down Myself (Tommy Noble Remix)",
                "number": "13",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Wolfgang Gartner",
                    "title": "Wolfgang's 5th Symphony",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Ellie Goulding",
                    "title": "I Need Your Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Wolfgang Gartner",
                "title": "Redline (Aylen & Kastra Remix)",
                "number": "14",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Of Monsters & Men",
                "title": "Little Talks",
                "number": "15",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "deadmau5 ft. Rob Swire",
                "title": "Ghosts 'N' Stuff",
                "number": "16",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Felguk",
                "title": "Blow Out (Lazy Rich Impossible Remix)",
                "number": "17",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Icona Pop ft. Charli XCX",
                    "title": "I Love It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Qulinez",
                "title": "Troll",
                "number": "18",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Youngblood Hawke",
                "title": "We Come Running (Acappella)",
                "number": "19",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Audien",
                "title": "Wayfarer",
                "number": "20",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Fray",
                    "title": "How To Save A Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Tommy Trash & A-Trak",
                "title": "Tuna Melt",
                "number": "21",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Atlas Genius",
                    "title": "Trojan",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Porter Robinson",
                "title": "Say My Name",
                "number": "22",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "I Miss You",
                "number": "23",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Sweet Dreams (Cazzette Meet At Night Mix)",
                "number": "24",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Porter Robinson",
                "title": "Say My Name (Aylen Trap Bootleg)",
                "number": "25",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Yung Joc",
                "title": "It's Goin' Down",
                "number": "26",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "RL Grime",
                "title": "Flood",
                "number": "27",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Weezer",
                "title": "Island In The Sun",
                "number": "28",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "MitiS",
                "title": "Endeavors",
                "number": "29",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fort Minor ft. Holly Brook & Jonah Matranga",
                "title": "Where'd You Go (Acappella)",
                "number": "30",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Mean",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vida",
                "title": "Keys",
                "number": "31",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ke$ha",
                "title": "Die Young (Acappella)",
                "number": "32",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dirty South & Alesso ft. Ruben Haze",
                "title": "City Of Dreams",
                "number": "33",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Project 46",
                "title": "Faces",
                "number": "34",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Police",
                "title": "Message In A Bottle (Acappella)",
                "number": "35",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Magnus & Timon",
                "title": "Triangle",
                "number": "36",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Avicii ft. Salem Al Fakir",
                    "title": "Silhouettes (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Andreas Moe",
                "title": "Fade Into Darkness (Instrumental Mix)",
                "number": "37",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Asaf Avidan",
                "title": "One Day (Reckoning Song) (Wankelmut Remix)",
                "number": "38",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Asaf Avidan",
                    "title": "One Day (Reckoning Song) (Wankelmut Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vengaboys",
                "title": "Boom, Boom, Boom, Boom!!",
                "number": "39",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii ft. Andreas Moe",
                "title": "Fade Into Darkness (Dave Martin Remix)",
                "number": "40",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Deniz Koyu vs. Wynter Gordon vs. Sultan & Shepard Ft. Quila",
                "title": "Follow The Walls (Dj Billy C Mashup)",
                "number": "41",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Backstreet Boys",
                "title": "I Want It That Way (Acappella)",
                "number": "42",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hardwell ft. Amba Shepherd",
                "title": "Apollo",
                "number": "43",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Old Crow Medicine Show",
                "title": "Wagon Wheel",
                "number": "44",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Vitodito & Oza",
                "title": "Kawaii (Dirty Club Mix)",
                "number": "45",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Michael Jackson",
                "title": "Billie Jean (Acappella)",
                "number": "46",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sebastian Ingrosso & Alesso",
                "title": "Calling",
                "number": "47",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Celine Dion",
                "title": "My Heart Will Go On (Titanic OST) (Acappella)",
                "number": "48",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Lupe Fiasco ft. Guy Sebastian",
                "title": "Battle Scars",
                "number": "49",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Jackson 5",
                "title": "ABC",
                "number": "50",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd ft. Foxes",
                "title": "Clarity",
                "number": "51",
                "timestamp": ""
            },
            "played_with": []
        }
    ]
}