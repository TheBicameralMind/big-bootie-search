export const bb13 = {
    "title": "Two Friends - Big Bootie Mix Volume 13 2018-04-04",
    "tracks": [
        {
            "main": {
                "artist": "Eminem",
                "title": "Lose Yourself",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Nicky Romero",
                    "title": "Toulouse",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd & Maren Morris & Grey",
                    "title": "The Middle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Williams & Brooks",
                    "title": "Jetlag",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Logic ft. Alessia Cara & Khalid",
                    "title": "1-800-273-8255",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Clean Bandit ft. Zara Larsson",
                "title": "Symphony (Ryos Remix)",
                "number": "02",
                "timestamp": "2:13"
            },
            "played_with": [
                {
                    "artist": "Bruno Mars",
                    "title": "Treasure",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Starkillers & Alex Kenji ft. Nadia Ali",
                    "title": "Pressure (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Justin Bieber",
                    "title": "Let Me Love You (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Axwell \u039b Ingrosso ft. Kristoffer Fogelmark",
                "title": "More Than You Know",
                "number": "03",
                "timestamp": "3:51"
            },
            "played_with": [
                {
                    "artist": "Nine Days",
                    "title": "Absolutely (Story Of A Girl)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jesse McCartney",
                    "title": "Beautiful Soul",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "3OH!3 ft. Kesha",
                    "title": "My First Kiss",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala ft. Ella Eyre",
                "title": "Came Here For Love",
                "number": "04",
                "timestamp": "5:09"
            },
            "played_with": [
                {
                    "artist": "Sam Hunt",
                    "title": "Body Like A Back Road",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Killers",
                    "title": "All These Things That I've Done",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cyndi Lauper",
                    "title": "Girls Just Want To Have Fun",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DNCE",
                    "title": "Cake By The Ocean",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Breathe Carolina & Dropgun ft. Kaleena Zanders",
                "title": "Rhythm Is A Dancer",
                "number": "05",
                "timestamp": "7:14"
            },
            "played_with": [
                {
                    "artist": "Blink-182",
                    "title": "Aliens Exist",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Camila Cabello ft. Young Thug",
                    "title": "Havana (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Steve Aoki ft. Wynter Gordon",
                    "title": "Ladi Dadi (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Lady Antebellum",
                "title": "Something Better",
                "number": "06",
                "timestamp": "8:38"
            },
            "played_with": [
                {
                    "artist": "Gryffin & Illenium ft. Daya",
                    "title": "Feel Good (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Aerosmith",
                    "title": "I Don't Want To Miss A Thing",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Solveig ft. ALMA",
                    "title": "All Stars",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Solveig & GTA",
                "title": "Intoxicated",
                "number": "07",
                "timestamp": "9:58"
            },
            "played_with": [
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "Tell Me Baby",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kings Of Leon",
                    "title": "Use Somebody (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "LMFAO",
                    "title": "Sexy And I Know It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Axwell \u039b Ingrosso ft. Vargas & Lagola",
                "title": "On My Way",
                "number": "08",
                "timestamp": "11:22"
            },
            "played_with": [
                {
                    "artist": "Eminem ft. Ed Sheeran",
                    "title": "River",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Halsey",
                    "title": "Bad At Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Brooks & GRX",
                    "title": "Boomerang",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift",
                    "title": "Look What You Made Me Do",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Porter Robinson",
                "title": "Sad Machine (Darren Styles & Gammer Remix)",
                "number": "09",
                "timestamp": "13:00"
            },
            "played_with": [
                {
                    "artist": "Marshmello",
                    "title": "Alone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson",
                    "title": "Sad Machine (Crystalize Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z",
                    "title": "Dirt Off Your Shoulder (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd ft. Hayley Williams",
                "title": "Stay The Night",
                "number": "10",
                "timestamp": "14:00"
            },
            "played_with": [
                {
                    "artist": "Tom Petty and The Heartbreakers",
                    "title": "Don't Do Me Like That",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "P!nk",
                    "title": "Raise Your Glass",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd & Alessia Cara",
                    "title": "Stay (Chris Robleda Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kaskade ft. Ilsey",
                    "title": "Disarm You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris & Alesso ft. Hurts",
                "title": "Under Control (Instrumental Mix)",
                "number": "11",
                "timestamp": "15:26"
            },
            "played_with": [
                {
                    "artist": "Frankie Goes To Hollywood",
                    "title": "Relax (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift",
                    "title": "\u2026Ready For It?",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5",
                    "title": "One More Night",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "JAUZ x Ghastly",
                "title": "Ghosts N' Sharks",
                "number": "12",
                "timestamp": "16:48"
            },
            "played_with": [
                {
                    "artist": "G-Eazy & Halsey",
                    "title": "Him & I",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Charli XCX ft. Lil Yachty",
                    "title": "After The Afterparty",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Clarx & Uplink",
                    "title": "Titans",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Florence + The Machine",
                    "title": "Spectrum (Say My Name) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Eximinds & Vigel",
                "title": "Handprint",
                "number": "13",
                "timestamp": "18:15"
            },
            "played_with": [
                {
                    "artist": "Twenty One Pilots",
                    "title": "Ride",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vertical Horizon",
                    "title": "Everything You Want",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Showtek ft. We Are Loud & Sonny Wilson",
                    "title": "Booyah",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien",
                "title": "Resolve",
                "number": "14",
                "timestamp": "19:35"
            },
            "played_with": [
                {
                    "artist": "Bruce Springsteen",
                    "title": "Born To Run",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Natasha Bedingfield",
                    "title": "Unwritten",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd ft. Matthew Koma",
                    "title": "Spectrum (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DJ Snake ft. Bipolar Sunshine",
                "title": "Middle",
                "number": "15",
                "timestamp": "20:50"
            },
            "played_with": [
                {
                    "artist": "Wiz Khalifa",
                    "title": "No Sleep",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone ft. 21 Savage",
                    "title": "Rockstar (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Bipolar Sunshine",
                    "title": "Middle (Mark Anthony Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Derulo ft. 2 Chainz",
                    "title": "Talk Dirty (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso ft. Roy English",
                "title": "Cool",
                "number": "16",
                "timestamp": "22:12"
            },
            "played_with": [
                {
                    "artist": "Tommy Tutone",
                    "title": "867-5309/Jenny",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "James Arthur",
                    "title": "Say You Won't Let Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso ft. Roy English",
                    "title": "Cool (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Axwell \u039b Ingrosso ft. Kristoffer Fogelmark",
                    "title": "More Than You Know (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Swedish House Mafia ft. John Martin",
                "title": "Don't You Worry Child (Instrumental)",
                "number": "17",
                "timestamp": "23:50"
            },
            "played_with": [
                {
                    "artist": "Clean Bandit ft. Zara Larsson",
                    "title": "Symphony (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marvin Gaye & Tammi Terrel",
                    "title": "Ain't No Mountain High Enough (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Kevin Writer",
                    "title": "While We're Dreaming",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Aloe Blacc",
                "title": "Wake Me Up",
                "number": "18",
                "timestamp": "25:05"
            },
            "played_with": [
                {
                    "artist": "Of Monsters & Men",
                    "title": "Little Talks (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ram Jam",
                    "title": "Black Betty",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Selena Gomez",
                    "title": "Wolves (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "MGMT",
                "title": "Kids (Two Friends Remix)",
                "number": "19",
                "timestamp": "26:20"
            },
            "played_with": [
                {
                    "artist": "Yellowcard",
                    "title": "Ocean Avenue (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Old Crow Medicine Show",
                    "title": "Wagon Wheel",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dillon Francis & DJ Snake",
                "title": "Get Low",
                "number": "20",
                "timestamp": "27:48"
            },
            "played_with": [
                {
                    "artist": "50 Cent",
                    "title": "In Da Club (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Otto Knows",
                "title": "Million Voices",
                "number": "21",
                "timestamp": "29:00"
            },
            "played_with": [
                {
                    "artist": "Randy Newman",
                    "title": "You've Got A Friend In Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. John Newman",
                    "title": "Blame (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber & BloodPop",
                    "title": "Friends",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Axwell \u039b Ingrosso ft. Trevor Guthrie",
                "title": "Dreamer",
                "number": "22",
                "timestamp": "30:33"
            },
            "played_with": [
                {
                    "artist": "Britney Spears",
                    "title": "Circus",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5 ft. SZA",
                    "title": "What Lovers Do",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ivan Gough & Feenixpawl ft. Georgi Kay",
                    "title": "In My Mind (Axwell Mix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. XYL\u00d8",
                    "title": "Setting Fires",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Manse ft. Chris Jones",
                "title": "Rising Sun",
                "number": "23",
                "timestamp": "31:57"
            },
            "played_with": [
                {
                    "artist": "Neon Trees",
                    "title": "Animal",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Smith",
                    "title": "Too Good At Goodbyes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Black Eyed Peas",
                    "title": "The Time (Dirty Bit) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Dua Lipa",
                    "title": "Scared To Be Lonely (Beau Collins Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Diplo ft. M\u00d8",
                "title": "Get It Right",
                "number": "24",
                "timestamp": "33:25"
            },
            "played_with": [
                {
                    "artist": "Post Malone",
                    "title": "I Fall Apart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Imagine Dragons",
                    "title": "Thunder",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Power (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Afrojack ft. Stephen Wrabel",
                "title": "Ten Feet Tall",
                "number": "25",
                "timestamp": "34:50"
            },
            "played_with": [
                {
                    "artist": "Michael Bubl\u00e9",
                    "title": "Haven't Met You Yet",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Commodores",
                    "title": "Brick House",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RetroVision X Raven & Kreyn",
                    "title": "Nobody Else",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Kelis",
                    "title": "Bounce (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Toni Braxton",
                "title": "Coping (Tom Swoon Remix)",
                "number": "26",
                "timestamp": "36:00"
            },
            "played_with": [
                {
                    "artist": "Rihanna",
                    "title": "Where Have You Been (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake Bell",
                    "title": "I Found A Way (Drake & Josh Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Derulo",
                    "title": "In My Head",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd & Maren Morris & Grey",
                "title": "The Middle",
                "number": "27",
                "timestamp": "37:32"
            },
            "played_with": [
                {
                    "artist": "Gin Blossoms",
                    "title": "Follow You Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fall Out Boy",
                    "title": "Centuries",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd & Maren Morris & Grey",
                    "title": "The Middle (Andrew Marks Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Matroda",
                    "title": "Chronic",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Usher ft. Ludacris & Lil Jon",
                    "title": "Yeah! (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Solveig ft. Dragonette",
                "title": "Hello",
                "number": "28",
                "timestamp": "39:19"
            },
            "played_with": [
                {
                    "artist": "Fitz & The Tantrums",
                    "title": "Handclap",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Billy Joel",
                    "title": "Uptown Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Dicky ft. Rich Homie Quan & Fetty Wap",
                    "title": "Save Dat Money",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Crankdat",
                "title": "Dollars",
                "number": "29",
                "timestamp": "40:42"
            },
            "played_with": [
                {
                    "artist": "Grouplove",
                    "title": "Tongue Tied",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Weeknd",
                    "title": "The Hills (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd ft. Foxes",
                "title": "Clarity (Vicetone Remix)",
                "number": "30",
                "timestamp": "41:42"
            },
            "played_with": [
                {
                    "artist": "Tom Petty and The Heartbreakers",
                    "title": "I Won't Back Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Julia Michaels",
                    "title": "Issues",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien",
                    "title": "Message",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snakehips & M\u00d8",
                    "title": "Don't Leave",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "3LAU ft. Bright Lights",
                "title": "How You Love Me (Instrumental Mix)",
                "number": "31",
                "timestamp": "43:10"
            },
            "played_with": [
                {
                    "artist": "The Chainsmokers & Coldplay",
                    "title": "Something Just Like This (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cheat Codes & Danny Quest",
                    "title": "NSFW",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Pump",
                    "title": "Gucci Gang",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Post Malone ft. Quavo",
                "title": "Congratulations (TELYKast x BKAYE Remix)",
                "number": "32",
                "timestamp": "44:33"
            },
            "played_with": [
                {
                    "artist": "Khalid",
                    "title": "Young Dumb & Broke",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jibbs",
                    "title": "Chain Hang Low",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Raven & Kreyn & Carnivore",
                "title": "Delight",
                "number": "33",
                "timestamp": "45:47"
            },
            "played_with": [
                {
                    "artist": "Niall Horan",
                    "title": "Slow Hands",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Clean Bandit ft. Sean Paul & Anne-Marie",
                    "title": "Rockabye (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Syn Cole",
                "title": "Bright Lights (Steerner Remix)",
                "number": "34",
                "timestamp": "46:56"
            },
            "played_with": [
                {
                    "artist": "Kelly Clarkson",
                    "title": "Stronger (What Doesn't Kill You)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Pitbull ft. Kesha",
                    "title": "Timber",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & Don Diablo ft. Thomas Troelsen",
                    "title": "Chemicals (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Axwell \u039b Ingrosso ft. Vargas & Lagola",
                "title": "Sun Is Shining",
                "number": "35",
                "timestamp": "48:15"
            },
            "played_with": [
                {
                    "artist": "Shawn Mendes",
                    "title": "There's Nothing Holdin' Me Back (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso ft. nICo & Vinz",
                    "title": "I Wanna Know (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Major Lazer ft. Ellie Goulding & Tarrus Riley",
                    "title": "Powerful",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Kevin Writer",
                "title": "Just A Kid (SuperJet Remix)",
                "number": "36",
                "timestamp": "49:30"
            },
            "played_with": [
                {
                    "artist": "Switchfoot",
                    "title": "Meant To Live",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "Mockingbird",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "This Is Gospel",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Nicky Romero & Vicetone ft. When We Are Wild",
                "title": "Let Me Feel",
                "number": "37",
                "timestamp": "50:56"
            },
            "played_with": [
                {
                    "artist": "B\u00f8rns",
                    "title": "Electric Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Beatles",
                    "title": "Here Comes The Sun",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Dan Tyminski",
                    "title": "Hey Brother (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Ruby Prophet",
                "title": "Circles",
                "number": "38",
                "timestamp": "52:15"
            },
            "played_with": [
                {
                    "artist": "Macklemore ft. Skylar Grey",
                    "title": "Glorious",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Dicky ft. Chris Brown",
                    "title": "Freaky Friday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Flo Rida ft. Sia",
                    "title": "Wild Ones (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Hardwell ft. Matthew Koma",
                "title": "Dare You",
                "number": "39",
                "timestamp": "53:38"
            },
            "played_with": [
                {
                    "artist": "OMI",
                    "title": "Cheerleader",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello ft. Khalid",
                    "title": "Silence",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sonny Bass",
                    "title": "No Stress",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake",
                    "title": "Hotline Bling (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix",
                "title": "Oops",
                "number": "40",
                "timestamp": "55:25"
            },
            "played_with": [
                {
                    "artist": "Charlie Puth",
                    "title": "Attention (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Rita Ora",
                    "title": "Lonely Together",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Rita Ora",
                "title": "Lonely Together",
                "number": "41",
                "timestamp": "56:45"
            },
            "played_with": [
                {
                    "artist": "5 Seconds of Summer",
                    "title": "She Looks So Perfect",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers & Coldplay",
                "title": "Something Just Like This",
                "number": "42",
                "timestamp": "57:38"
            },
            "played_with": [
                {
                    "artist": "Nico & Vinz",
                    "title": "That's How You Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Diddy Dirty Money ft. Skylar Grey",
                    "title": "Coming Home (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avril Lavigne",
                    "title": "My Happy Ending",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Kevin Writer",
                    "title": "Just A Kid",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. T-Pain",
                    "title": "Good Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chumbawamba",
                    "title": "Tubthumping (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Weezer",
                    "title": "Say It Ain't So",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}