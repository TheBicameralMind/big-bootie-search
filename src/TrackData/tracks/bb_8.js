export const bb8 = {
    "title": "Two Friends - Big Bootie Mix Vol. 8 2015-09-28",
    "tracks": [
        {
            "main": {
                "artist": "Michael Calfan",
                "title": "Resurrection (Axwell Re-Cut Club Version)",
                "number": "01",
                "timestamp": "0:01"
            },
            "played_with": [
                {
                    "artist": "Major Lazer & DJ Snake ft. M\u00d8",
                    "title": "Lean On (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eiffel 65",
                    "title": "Blue (Da Ba Dee) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Modern Machines",
                    "title": "Feel It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Adele",
                    "title": "Someone Like You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "deadmau5",
                "title": "Some Chords (Andrei Stephen Remix)",
                "number": "02",
                "timestamp": "1:33"
            },
            "played_with": [
                {
                    "artist": "Ellie Goulding",
                    "title": "Lights (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kid Cudi ft. MGMT & Ratatat",
                    "title": "Pursuit Of Happiness (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Valentino Khan",
                    "title": "Deep Down Low",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Paris & Simo",
                    "title": "Zombie (3LAU TomorrowWorld Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Milky Chance",
                    "title": "Flashed Junk Mind (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "'N Sync",
                    "title": "Bye Bye Bye (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Deepside Deejays",
                    "title": "Never Be Alone (DJ Viduta Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BORGEOUS & Shaun Frank ft. Delaney Jane",
                    "title": "This Could Be Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Milky Chance",
                "title": "Flashed Junk Mind (Two Friends Remix)",
                "number": "03",
                "timestamp": "4:10"
            },
            "played_with": [
                {
                    "artist": "Clean Bandit ft. Jess Glynne",
                    "title": "Real Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BORGEOUS & Shaun Frank ft. Delaney Jane",
                    "title": "This Could Be Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "501",
                "title": "Kill Your Boss (Barely Alive Remix)",
                "number": "04",
                "timestamp": "5:41"
            },
            "played_with": [
                {
                    "artist": "Big Sean ft. E40",
                    "title": "IDFWU (I Don't Fuck With You) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift ft. Kendrick Lamar",
                    "title": "Bad Blood (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Nora En Pure",
                "title": "Saltwater (2015 Rework)",
                "number": "05",
                "timestamp": "6:38"
            },
            "played_with": [
                {
                    "artist": "Tove Lo",
                    "title": "Talking Body (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gostan",
                    "title": "Klanga (Pep & Rash Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "LORDE",
                    "title": "Team (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Julian Jordan",
                "title": "Blinded By The Light",
                "number": "06",
                "timestamp": "9:13"
            },
            "played_with": [
                {
                    "artist": "Alesso ft. Roy English",
                    "title": "Cool (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jet",
                    "title": "Are You Gonna Be My Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Guru Josh Project",
                    "title": "Infinity (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Andy Grammer",
                    "title": "Honey I'm Good",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Major Lazer & DJ Snake ft. M\u00d8",
                    "title": "Lean On (Jonas Aden Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Florian Picasso ft. Nolan Sipe",
                    "title": "Want It Back (Origami)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "On My Mind",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Coyote Kisses",
                "title": "Illusion",
                "number": "07",
                "timestamp": "13:24"
            },
            "played_with": [
                {
                    "artist": "Drake ft. Majid Jordan",
                    "title": "Hold On, We're Going Home (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cali Swag District",
                    "title": "Teach Me How To Dougie (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snoop Dogg ft. Pharrell Williams",
                    "title": "Drop It Like It's Hot (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Pelari & Pablo Oliveros",
                "title": "Blackout",
                "number": "08",
                "timestamp": "14:47"
            },
            "played_with": [
                {
                    "artist": "Wiz Khalifa ft. Charlie Puth",
                    "title": "See You Again (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Benny Benassi pres. The Biz",
                    "title": "Satisfaction (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Matisse & Sadko",
                    "title": "Dragon",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Steve Angello ft. Mako",
                    "title": "Children Of The Wild (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Michael Brun & Dirty Twist",
                    "title": "WOO",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Paris Blohm & Steerner ft. Paul Aiden",
                    "title": "Fight Forever",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Arston",
                "title": "Light",
                "number": "09",
                "timestamp": "17:55"
            },
            "played_with": [
                {
                    "artist": "Coleman Hell",
                    "title": "2 Heads",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Afrojack ft. Mike Taylor",
                    "title": "SummerThing! (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DubVision & Michael Brun ft. Tom Cane",
                    "title": "Sun In Your Eyes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien ft. Michael S.",
                    "title": "Leaving You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Teqq",
                "title": "ID",
                "number": "10",
                "timestamp": "20:21"
            },
            "played_with": [
                {
                    "artist": "Bryan Adams",
                    "title": "Summer Of 69",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo ft. Conrad Sewell",
                    "title": "Firestone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jus Jack",
                "title": "Stars",
                "number": "11",
                "timestamp": "22:06"
            },
            "played_with": [
                {
                    "artist": "Tom Petty",
                    "title": "American Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jimmy Eat World",
                    "title": "The Middle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Weezer",
                    "title": "If You're Wondering If I Want You To",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DubVision",
                    "title": "Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Walk The Moon",
                    "title": "Shut Up And Dance (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd ft. Jon Bellion",
                "title": "Beautiful Now",
                "number": "12",
                "timestamp": "26:13"
            },
            "played_with": [
                {
                    "artist": "Spin Doctors",
                    "title": "Two Princes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien ft. Lady Antebellum",
                    "title": "Something Better",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lush & Simon",
                "title": "Stellar",
                "number": "13",
                "timestamp": "27:35"
            },
            "played_with": [
                {
                    "artist": "Lilly Wood & The Prick & Robin Schulz",
                    "title": "Prayer In C (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Amy Winehouse",
                    "title": "Rehab (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien ft. Michael S.",
                    "title": "Leaving You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Axwell \u039b Ingrosso ft. Vargas & Lagola",
                    "title": "On My Way (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Basshunter",
                    "title": "DotA",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fetty Wap",
                    "title": "Trap Queen (Crankdat Future Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Natalie La Rose ft. Jeremih",
                    "title": "Somebody (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "NGHTMRE",
                "title": "Street",
                "number": "14",
                "timestamp": "30:47"
            },
            "played_with": [
                {
                    "artist": "Echosmith",
                    "title": "Cool Kids (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Darude",
                "title": "Sandstorm (Didrick Remix)",
                "number": "15",
                "timestamp": "32:00"
            },
            "played_with": [
                {
                    "artist": "Echosmith",
                    "title": "Cool Kids (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kaskade ft. Rebecca & Fiona",
                    "title": "Turn It Down (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dropgun",
                "title": "Ninja",
                "number": "16",
                "timestamp": "33:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bastille",
                "title": "Flaws (Acappella)",
                "number": "17",
                "timestamp": "33:33"
            },
            "played_with": [
                {
                    "artist": "Bastille",
                    "title": "Pompeii (Acappella)",
                    "number": "w/",
                    "timestamp": "34:01"
                },
                {
                    "artist": "DJ Antoine ft. The Beat Shakers",
                    "title": "Ma Cherie (Acappella)",
                    "number": "w/",
                    "timestamp": "34:15"
                }
            ]
        },
        {
            "main": {
                "artist": "Aylen",
                "title": "Life Alert",
                "number": "18",
                "timestamp": "34:58"
            },
            "played_with": [
                {
                    "artist": "Ellie Goulding",
                    "title": "Love Me Like You Do (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Usher",
                    "title": "Confessions Part II",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vida",
                "title": "Arctic Lights",
                "number": "19",
                "timestamp": "36:34"
            },
            "played_with": [
                {
                    "artist": "Justin Bieber",
                    "title": "What Do You Mean? (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Omarion ft. Chris Brown & Jhen\u00e9 Aiko",
                    "title": "Post To Be (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Osen",
                "title": "ID",
                "number": "20",
                "timestamp": "38:00"
            },
            "played_with": [
                {
                    "artist": "Robin Schulz ft. Jasmine Thompson",
                    "title": "Sun Goes Down (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna ft. Calvin Harris",
                    "title": "We Found Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Big Sean vs. Party Favor",
                    "title": "I Don't Give A Bap U (TWRK Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Backstreet Boys",
                    "title": "Everybody (Oski & Apashe & Lennon Bootleg)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Great Good Fine Ok",
                    "title": "Let You Go (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Riggi & Piros",
                    "title": "Keep Rockin",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Oliver Heldens & Becky Hill",
                    "title": "Gecko (Overdrive) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Pierce Fulton",
                    "title": "In Reality",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bassnectar",
                "title": "Bass Head",
                "number": "21",
                "timestamp": "43:26"
            },
            "played_with": [
                {
                    "artist": "Oliver Heldens & Becky Hill",
                    "title": "Gecko (Overdrive) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ludacris & Shawnna",
                    "title": "Gettin' Some",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Popeska",
                "title": "Kraken",
                "number": "22",
                "timestamp": "45:07"
            },
            "played_with": [
                {
                    "artist": "Third Eye Blind",
                    "title": "Never Let You Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sum 41",
                    "title": "In Too Deep (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jack U\u0308 ft. Kiesza",
                    "title": "Take \u00dc There (Tchami Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kayliox",
                "title": "Nova",
                "number": "23",
                "timestamp": "46:36"
            },
            "played_with": [
                {
                    "artist": "Kings Of Leon",
                    "title": "Sex On Fire (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "3LAU & Nom De Strip ft. Estelle",
                    "title": "The Night (Hunter Siegel Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DubVision",
                "title": "Backlash (Martin Garrix Edit)",
                "number": "24",
                "timestamp": "47:57"
            },
            "played_with": [
                {
                    "artist": "Becky G",
                    "title": "Shower (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chumbawamba",
                    "title": "Tubthumping (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justice vs. Simian",
                    "title": "We Are Your Friends (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "King Arthur ft. Michael Meaco",
                    "title": "Belong To The Rhythm (Don Diablo Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Demi Lovato",
                    "title": "Cool For The Summer (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dillon Francis ft. Totally Enormous Extinct Dinosaurs",
                    "title": "Without You (Henry Fong Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Nicky Romero vs. Volt & State ft. Danny Shah",
                "title": "Warriors (Syn Cole Remix)",
                "number": "25",
                "timestamp": "51:56"
            },
            "played_with": [
                {
                    "artist": "AlunaGeorge",
                    "title": "You Know You Like It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Pitbull ft. Ne-Yo & Afrojack & Nayer",
                    "title": "Give Me Everything (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Michael Calfan",
                "title": "Treasured Soul",
                "number": "26",
                "timestamp": "53:16"
            },
            "played_with": [
                {
                    "artist": "Calvin Harris & Disciples ft. Ina Wroldsen",
                    "title": "How Deep Is Your Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "ODESZA ft. Shy Girls",
                "title": "All We Need (Dzeko & Torres Remix)",
                "number": "27",
                "timestamp": "54:46"
            },
            "played_with": [
                {
                    "artist": "OneRepublic",
                    "title": "I Lived (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "JAMES",
                "title": "Coming Home",
                "number": "28",
                "timestamp": "56:17"
            },
            "played_with": [
                {
                    "artist": "Jason Derulo",
                    "title": "Want To Want Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo ft. Conrad Sewell",
                    "title": "Firestone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd ft. Jon Bellion",
                    "title": "Beautiful Now (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Becky G",
                    "title": "Shower (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Demi Lovato",
                    "title": "Cool For The Summer (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}