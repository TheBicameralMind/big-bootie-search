export const bb16 = {
    "title": "Two Friends - Big Bootie Mix Volume 16 2019-10-11",
    "tracks": [
        {
            "main": {
                "artist": "Gene Wilder",
                "title": "Pure Imagination (Willy Wonka & The Chocolate Factory OST) (Two Friends Intro Edit)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Ellie Goulding",
                    "title": "Sixteen (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Toto",
                    "title": "Africa (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Aloe Blacc",
                    "title": "SOS",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "Mama Look At Me Now",
                "number": "02",
                "timestamp": "1:33"
            },
            "played_with": [
                {
                    "artist": "Billie Eilish",
                    "title": "bad guy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bryce Vine ft. YG",
                    "title": "La La Land",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Billie Eilish & Justin Bieber",
                    "title": "bad guy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "Mama Look At Me Now (Galantis & Deniz Koyu Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DubVision",
                    "title": "Magnum",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Script",
                    "title": "Breakeven",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Deorro",
                "title": "Five Hours",
                "number": "03",
                "timestamp": "3:05"
            },
            "played_with": [
                {
                    "artist": "Ashley O",
                    "title": "On A Roll",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ivan Gough & Feenixpawl ft. Georgi Kay",
                    "title": "In My Mind (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien",
                    "title": "Wayfarer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bowling For Soup",
                    "title": "1985",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "MEDUZA ft. GOODBOYS",
                "title": "Piece Of Your Heart (ALOK Remix)",
                "number": "04",
                "timestamp": "4:31"
            },
            "played_with": [
                {
                    "artist": "Queen & David Bowie",
                    "title": "Under Pressure",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shakira ft. Wyclef Jean",
                    "title": "Hips Don't Lie",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MEDUZA ft. GOODBOYS",
                    "title": "Piece Of Your Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snoop Dogg ft. Pharrell Williams",
                    "title": "Drop It Like It's Hot (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Marshmello ft. Bastille",
                "title": "Happier (Salasnich Remix)",
                "number": "05",
                "timestamp": "5:56"
            },
            "played_with": [
                {
                    "artist": "Galantis & Throttle",
                    "title": "Tell Me You Love Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Portugal. The Man",
                    "title": "Feel It Still",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "Hey Look Ma, I Made It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Feldt ft. Jake Reese",
                    "title": "Blackbird (CALVO Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone ft. Young Thug",
                    "title": "Goodbyes",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "BEAUZ ft. Maggie Szabo",
                "title": "Sayonara",
                "number": "06",
                "timestamp": "7:20"
            },
            "played_with": [
                {
                    "artist": "Fall Out Boy",
                    "title": "This Ain't A Scene, It's An Arms Race",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sammy Adams",
                    "title": "All Night Longer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Him & Jordan Jay",
                    "title": "By Your Side",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lukas Graham",
                    "title": "Love Someone",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dante Klein & Raven & Kreyn",
                "title": "Escape",
                "number": "07",
                "timestamp": "8:45"
            },
            "played_with": [
                {
                    "artist": "Social House ft. Lil Yachty",
                    "title": "Magic In The Hamptons",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jamie Lynn Spears",
                    "title": "Follow Me (Zoey 101 Theme)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "Cutting Shapes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "The Party Song",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis ft. OneRepublic",
                "title": "Bones (Galantis & shnd\u014d VIP Mix)",
                "number": "08",
                "timestamp": "10:09"
            },
            "played_with": [
                {
                    "artist": "Hilary Duff",
                    "title": "So Yesterday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Offspring",
                    "title": "You're Gonna Go Far, Kid",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Perry",
                    "title": "Runaway",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jimmy Cliff & Lebo M.",
                    "title": "Hakuna Matata (The Lion King OST)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Maison & Dragen ft. Michael Rice",
                "title": "I Won't Stumble Back",
                "number": "09",
                "timestamp": "11:24"
            },
            "played_with": [
                {
                    "artist": "Rihanna ft. Kanye West & Paul McCartney",
                    "title": "FourFiveSeconds",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ABBA",
                    "title": "Mamma Mia",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Illenium ft. Jon Bellion",
                    "title": "Good Things Fall Apart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo ft. Alex Clare",
                    "title": "Heaven To Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Dicky",
                    "title": "Earth",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alan Walker ft. AU/RA & Tomine Harket",
                "title": "Darkside",
                "number": "10",
                "timestamp": "12:48"
            },
            "played_with": [
                {
                    "artist": "Passenger",
                    "title": "Let Her Go (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Journey",
                    "title": "Any Way You Want It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Grandtheft & Keys N Krates",
                    "title": "Keep It 100",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Savage",
                    "title": "Swing (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jordan Jay",
                "title": "Spinning Lights",
                "number": "11",
                "timestamp": "14:04"
            },
            "played_with": [
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "Californication (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake",
                    "title": "Marvin's Room",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Yves V & HIDDN",
                    "title": "Magnolia",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kansas",
                    "title": "Carry On Wayward Son",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kisma ft. Shira",
                "title": "Shine Over Me",
                "number": "12",
                "timestamp": "15:27"
            },
            "played_with": [
                {
                    "artist": "The Rembrandts",
                    "title": "I'll Be There For You (Friends Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone",
                    "title": "Wow",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hailee Steinfeld",
                    "title": "Love Myself",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson ft. Bright Lights",
                    "title": "Language",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber",
                    "title": "What Do You Mean? (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Chris Martin",
                "title": "Heaven",
                "number": "13",
                "timestamp": "16:50"
            },
            "played_with": [
                {
                    "artist": "Snow Patrol",
                    "title": "Chasing Cars (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justice",
                    "title": "D.A.N.C.E. (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "American Hi-Fi",
                    "title": "Flavor Of The Weak",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso ft. Roy English",
                    "title": "Cool",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sigala & Ella Eyre & Meghan Trainor ft. French Montana",
                    "title": "Just Got Paid",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Loud Luxury & Bryce Vine",
                "title": "I'm Not Alright",
                "number": "14",
                "timestamp": "18:15"
            },
            "played_with": [
                {
                    "artist": "Martin Solveig ft. Dragonette",
                    "title": "Hello (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hot Chelle Rae",
                    "title": "Tonight Tonight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvo & TAI",
                    "title": "Sky Racer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Green Day",
                    "title": "21 Guns",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sagan",
                "title": "Dance With Me",
                "number": "15",
                "timestamp": "19:40"
            },
            "played_with": [
                {
                    "artist": "Gym Class Heroes",
                    "title": "Cupid's Chokehold",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quinn XCII",
                    "title": "Straightjacket",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BRKLYN & Zack Martino",
                    "title": "Good Vibe",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem ft. Rihanna",
                    "title": "The Monster (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Clean Bandit ft. Jess Glynne",
                "title": "Rather Be",
                "number": "16",
                "timestamp": "21:02"
            },
            "played_with": [
                {
                    "artist": "Kygo & Rita Ora",
                    "title": "Carry On",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lizzo",
                    "title": "Truth Hurts",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Nas X ft. Billy Ray Cyrus",
                    "title": "Old Town Road",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "SLANDER & NGHTMRE",
                "title": "Warning",
                "number": "17",
                "timestamp": "22:15"
            },
            "played_with": [
                {
                    "artist": "NF",
                    "title": "Let You Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ariana Grande ft. Mac Miller",
                    "title": "The Way",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kendrick Lamar",
                    "title": "Backseat Freestyle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dropgun ft. Nevve",
                "title": "Drought",
                "number": "18",
                "timestamp": "23:25"
            },
            "played_with": [
                {
                    "artist": "5 Seconds of Summer",
                    "title": "Easier",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Boston",
                    "title": "More Than A Feeling",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo & MARNIK",
                    "title": "Children Of A Miracle (Don Diablo VIP Mix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Pinkfong",
                    "title": "Baby Shark",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jonas Blue ft. Theresa Rex",
                "title": "What I Like About You (Owen Norton Remix)",
                "number": "19",
                "timestamp": "24:48"
            },
            "played_with": [
                {
                    "artist": "One Direction",
                    "title": "Best Song Ever",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Simple Plan",
                    "title": "Welcome To My Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers & Tritonal ft. Emily Warren",
                    "title": "Until You Were Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Heartless",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo ft. Jackie Young",
                "title": "All My Friends",
                "number": "20",
                "timestamp": "26:10"
            },
            "played_with": [
                {
                    "artist": "Florida Georgia Line",
                    "title": "Cruise",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mario",
                    "title": "Let Me Love You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "T-Pain",
                    "title": "I'm N Luv (With A Stripper)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sigrid",
                    "title": "Don't Kill My Vibe",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The White Stripes vs. Gregori Klosman",
                "title": "Seven Nation Funk It (Alex Addea Mashup)",
                "number": "21",
                "timestamp": "27:46"
            },
            "played_with": [
                {
                    "artist": "Nirvana",
                    "title": "Smells Like Teen Spirit (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The White Stripes",
                    "title": "Seven Nation Army (Joe Maz Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gwen Stefani",
                    "title": "Hollaback Girl (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "50 Cent",
                    "title": "Disco Inferno (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Mama",
                    "title": "Lipgloss",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "All The Small Things",
                "number": "22",
                "timestamp": "29:05"
            },
            "played_with": [
                {
                    "artist": "Amy Winehouse",
                    "title": "Rehab (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Nas X ft. Billy Ray Cyrus",
                    "title": "Old Town Road",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MEDUZA ft. GOODBOYS",
                    "title": "Piece Of Your Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182 vs. Bassjackers",
                    "title": "All The Small Things Like That (Simon Dc Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "All The Small Things (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sam Feldt & Yves V ft. ROZES",
                "title": "One Day",
                "number": "23",
                "timestamp": "30:35"
            },
            "played_with": [
                {
                    "artist": "Rise Against",
                    "title": "Savior",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Los Del Rio",
                    "title": "Macarena",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Throttle",
                    "title": "Like This",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso & Hailee Steinfeld ft. Florida Georgia Line & Watt",
                    "title": "Let Me Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Levels (Skrillex Remix)",
                "number": "24",
                "timestamp": "32:00"
            },
            "played_with": [
                {
                    "artist": "Sam Smith & Normani",
                    "title": "Dancing With A Stranger",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Papa Roach",
                    "title": "Last Resort",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Whethan & Louis The Child",
                    "title": "Chop It Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Silento",
                    "title": "Watch Me (Whip/Nae Nae) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. Big Sean & Pusha T & 2 Chainz",
                    "title": "Mercy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ti\u00ebsto ft. Stargate & Aloe Blacc",
                "title": "Carry You Home",
                "number": "25",
                "timestamp": "33:00"
            },
            "played_with": [
                {
                    "artist": "Lizzo ft. Ariana Grande",
                    "title": "Good As Hell (Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers & Illenium ft. Lennon Stella",
                    "title": "Takeaway",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dzeko vs. Riggi & Piros ft. Veronica",
                    "title": "Heaven",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello ft. CHVRCHES",
                    "title": "Here With Me",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bingo Players",
                "title": "Rattle",
                "number": "26",
                "timestamp": "34:25"
            },
            "played_with": [
                {
                    "artist": "Eminem",
                    "title": "Crack A Bottle",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quad City DJ's",
                    "title": "Space Jam",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ishimaru & Harkoz",
                    "title": "Rock N Swing",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tyga",
                    "title": "Rack City",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Salasnich & KPLR",
                "title": "Your Eyes",
                "number": "27",
                "timestamp": "35:35"
            },
            "played_with": [
                {
                    "artist": "Etta James",
                    "title": "Something's Got A Hold On Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MGMT",
                    "title": "Electric Feel (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shane 54 & Cubicore ft. Eric Lumiere",
                    "title": "Out Of Time (Sagan Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Destiny's Child",
                    "title": "Soldier",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "Firebird",
                "number": "28",
                "timestamp": "36:56"
            },
            "played_with": [
                {
                    "artist": "The Vamps & Matoma",
                    "title": "All Night (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Survivor",
                    "title": "Eye Of The Tiger (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso vs. OneRepublic",
                    "title": "If I Lose Myself",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Ayah Marar",
                    "title": "Thinking About You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Owl City",
                "title": "Fireflies",
                "number": "29",
                "timestamp": "38:22"
            },
            "played_with": [
                {
                    "artist": "Nickelback",
                    "title": "Photograph",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift",
                    "title": "You Need To Calm Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin ft. Katie Pearlman",
                    "title": "Nobody Compares To You (Codeko Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Owl City",
                    "title": "Fireflies",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Disco Fries ft. Ollie Green",
                "title": "Moving Mountains (GATT\u00dcSO Remix)",
                "number": "30",
                "timestamp": "39:37"
            },
            "played_with": [
                {
                    "artist": "Ed Sheeran ft. Khalid",
                    "title": "Beautiful People",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Lonely Island ft. Michael Bolton",
                    "title": "Jack Sparrow",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sultan + Shepard ft. Nadia Ali & IRO",
                    "title": "Almost Home",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shakira ft. Freshlyground",
                    "title": "Waka Waka (This Time For Africa)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Oliver Heldens",
                "title": "Gecko",
                "number": "31",
                "timestamp": "41:08"
            },
            "played_with": [
                {
                    "artist": "Lil Kim ft. 50 Cent",
                    "title": "Magic Stick",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Notorious B.I.G. ft. Mase & P. Diddy",
                    "title": "Mo Money Mo Problems",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Diplo & Sleepy Tom",
                "title": "Be Right There",
                "number": "32",
                "timestamp": "41:50"
            },
            "played_with": [
                {
                    "artist": "David Guetta ft. Anne-Marie",
                    "title": "Don't Leave Me Alone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Green Day",
                    "title": "American Idiot",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers & Bebe Rexha",
                    "title": "Call You Mine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alex Newell & DJ Cassidy & Jess Glynne ft. Nile Rodgers",
                    "title": "Kill The Lights",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin ft. Zohara",
                "title": "Remember (Madison Mars Remix)",
                "number": "33",
                "timestamp": "43:14"
            },
            "played_with": [
                {
                    "artist": "KT Tunstall",
                    "title": "Suddenly I See",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shawn Mendes",
                    "title": "If I Can't Have You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers & Coldplay",
                    "title": "Something Just Like This (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "James Bay",
                    "title": "Let It Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ookay",
                "title": "Thief (Flux Pavilion Remix)",
                "number": "34",
                "timestamp": "44:40"
            },
            "played_with": [
                {
                    "artist": "DJ Khaled ft. Ludacris & Rick Ross & T-Pain & Snoop Dogg",
                    "title": "All I Do Is Win (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Soulja Boy Tell 'Em",
                    "title": "Crank That (Soulja Boy) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Don Diablo ft. Jessie J",
                "title": "Brave (Don Diablo VIP Mix)",
                "number": "35",
                "timestamp": "45:21"
            },
            "played_with": [
                {
                    "artist": "Jimmy Eat World",
                    "title": "Sweetness (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kesha",
                    "title": "Your Love Is My Drug",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien ft. Parson James",
                    "title": "Insomnia",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ed Sheeran & Justin Bieber",
                    "title": "I Don't Care",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mr. Belt & Wezol & Jack Wins",
                "title": "One Thing",
                "number": "36",
                "timestamp": "46:46"
            },
            "played_with": [
                {
                    "artist": "Flo Rida ft. T-Pain",
                    "title": "Low",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gorillaz ft. Del The Funky Homosapien",
                    "title": "Clint Eastwood (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chris Brown ft. T-Pain",
                    "title": "Kiss Kiss",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sigala ft. John Newman & Nile Rodgers",
                    "title": "Give Me Your Love",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kelly Clarkson",
                "title": "Love So Soft (Cash Cash Remix)",
                "number": "37",
                "timestamp": "48:10"
            },
            "played_with": [
                {
                    "artist": "Raven-Symon\u00e9",
                    "title": "That's So Raven (Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The White Stripes",
                    "title": "Seven Nation Army (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo & Khrebto",
                    "title": "Got The Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Feldt ft. Rani",
                    "title": "Post Malone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lewis Capaldi",
                    "title": "Someone You Loved",
                    "number": "w/",
                    "timestamp": "49:32"
                },
                {
                    "artist": "Ava Max",
                    "title": "Sweet But Psycho",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Britney Spears",
                    "title": "Lucky",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. JUSCOVA",
                "title": "Good For You",
                "number": "39",
                "timestamp": "50:40"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "P!nk ft. Cash Cash",
                "title": "Can We Pretend",
                "number": "40",
                "timestamp": "51:55"
            },
            "played_with": [
                {
                    "artist": "James Blunt",
                    "title": "You're Beautiful",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The All-American Rejects",
                    "title": "Move Along",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Starland Vocal Band",
                    "title": "Afternoon Delight",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Salasnich ft. Rhett Fisher",
                "title": "Talking To Myself",
                "number": "41",
                "timestamp": "53:00"
            },
            "played_with": [
                {
                    "artist": "Billy Joel",
                    "title": "We Didn't Start The Fire",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Dani Poppitt",
                    "title": "Dollar Menu",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Dani Poppitt",
                    "title": "Dollar Menu (GATT\u00dcSO Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cher",
                    "title": "Believe",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Aloe Blacc",
                "title": "SOS",
                "number": "42",
                "timestamp": "54:23"
            },
            "played_with": [
                {
                    "artist": "Paramore",
                    "title": "Misery Business",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Nas X",
                    "title": "Panini",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd & Alessia Cara",
                    "title": "Stay",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dua Lipa",
                    "title": "IDGAF",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Illenium ft. Bahari",
                "title": "Crashing (Country Club Martini Crew Remix)",
                "number": "43",
                "timestamp": "55:33"
            },
            "played_with": [
                {
                    "artist": "Sam Smith",
                    "title": "I'm Not The Only One",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sum 41",
                    "title": "Fat Lip",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "CALVO & DAZZ ft. K.O.",
                    "title": "Wind Me Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "FLETCHER",
                    "title": "Undrunk",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin & SLANDER ft. Calle Lehman",
                "title": "All You Need To Know",
                "number": "44",
                "timestamp": "56:57"
            },
            "played_with": [
                {
                    "artist": "Rihanna ft. Mikky Ekko",
                    "title": "Stay (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Smith",
                    "title": "Stay With Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Khalid",
                    "title": "Talk",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wynter Gordon",
                    "title": "Dirty Talk (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "Runaway (U & I)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sebastian Ingrosso & Alesso ft. Ryan Tedder",
                    "title": "Calling (Lose My Mind)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madeon",
                    "title": "All My Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snakehips ft. Tinashe & Chance The Rapper",
                    "title": "All My Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Billy Joel",
                    "title": "Piano Man",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ingrid Michaelson",
                    "title": "You & I",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alesso vs. OneRepublic",
                    "title": "If I Lose Myself (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}