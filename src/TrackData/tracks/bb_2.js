export const bb2 = {
    "title": "Two Friends - Big Bootie Mix Volume 2 2012-11-03",
    "tracks": [
        {
            "main": {
                "artist": "Porter Robinson",
                "title": "Unison",
                "number": "01",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Two Door Cinema Club",
                "title": "What You Know",
                "number": "02",
                "timestamp": "0:08"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Christopher S. ft. Fannie Luescher",
                "title": "Free (let Yourself Go)",
                "number": "03",
                "timestamp": "2:13"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Wolfgang Gartner",
                "title": "Space Junk",
                "number": "04",
                "timestamp": "2:52"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Wolfgang Gartner",
                "title": "Space Junk (Go Buck! Buck'd Out Bootleg)",
                "number": "05",
                "timestamp": "4:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dyro",
                "title": "Paradox",
                "number": "06",
                "timestamp": "5:29"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Linkin Park",
                "title": "What I've Done (Acappella)",
                "number": "07",
                "timestamp": "5:35"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Two Million",
                "number": "08",
                "timestamp": "6:51"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "James Egbert",
                "title": "Back To New",
                "number": "09",
                "timestamp": "7:13"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Savoy",
                "title": "Free Agent",
                "number": "10",
                "timestamp": "7:54"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "The Rain",
                "number": "11",
                "timestamp": "8:58"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Randy Newman",
                "title": "You've Got A Friend In Me",
                "number": "12",
                "timestamp": "9:58"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Martin Solveig ft. Dragonette",
                "title": "Hello (Acappella)",
                "number": "13",
                "timestamp": "10:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Martin Solveig ft. Dragonette",
                "title": "Hello (Drughead Remix)",
                "number": "14",
                "timestamp": "10:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bingo Players",
                "title": "Rattle",
                "number": "15",
                "timestamp": "11:34"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bel Suono & DJ Magic Finger",
                "title": "Te Quiero",
                "number": "16",
                "timestamp": "12:51"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "One Direction",
                "title": "What Makes You Beautiful",
                "number": "17",
                "timestamp": "12:51"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sebastian Ingrosso & Alesso",
                "title": "Calling",
                "number": "18",
                "timestamp": "13:43"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Matisyahu",
                "title": "Jerusalem",
                "number": "19",
                "timestamp": "14:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kaskade ft. Rebecca & Fiona",
                "title": "Turn It Down (Acappella)",
                "number": "20",
                "timestamp": "14:56"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kaskade ft. Rebecca & Fiona",
                "title": "Turn It Down (Thrusher Remix)",
                "number": "21",
                "timestamp": "15:17"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kaskade ft. Rebecca & Fiona",
                "title": "Turn It Down (Nause Remix)",
                "number": "22",
                "timestamp": "15:31"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kaskade ft. Rebecca & Fiona",
                "title": "Turn It Down (Le Castle Vania Remix)",
                "number": "23",
                "timestamp": "17:01"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Nause",
                "title": "Mellow",
                "number": "24",
                "timestamp": "18:27"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "John Legend",
                "title": "Ordinary People (Acappella)",
                "number": "25",
                "timestamp": "18:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Diplo ft. Gent & Jawns",
                "title": "Butter's Theme",
                "number": "26",
                "timestamp": "21:50"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Eminem",
                "title": "Lose Yourself (Acappella)",
                "number": "27",
                "timestamp": "22:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Lucky Date",
                "title": "I Want You",
                "number": "28",
                "timestamp": "23:56"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Beach Boys",
                "title": "Wouldn't It Be Nice",
                "number": "29",
                "timestamp": "24:24"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tim Mason",
                "title": "Anima",
                "number": "30",
                "timestamp": "26:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "First Date (Acappella)",
                "number": "31",
                "timestamp": "26:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd",
                "title": "Autonomy",
                "number": "32",
                "timestamp": "28:49"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Maroon 5",
                "title": "Payphone",
                "number": "33",
                "timestamp": "29:16"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tom Hangs ft. Shermanology",
                "title": "Blessed (Avicii Edit)",
                "number": "34",
                "timestamp": "30:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Robbie Rivera & Tommy Lee & Bassjackers vs. Red Hot Chili Peppers",
                "title": "Ding Dong vs. Otherside (Hardwell Mashup)",
                "number": "35",
                "timestamp": "31:02"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "W&W & Jochen Miller",
                "title": "Summer",
                "number": "36",
                "timestamp": "32:54"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fun.",
                "title": "Some Nights",
                "number": "37",
                "timestamp": "33:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Marco V",
                "title": "Analogital (Bassjackers Remix)",
                "number": "38",
                "timestamp": "34:42"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Black Eyed Peas",
                "title": "Just Can't Get Enough (Acappella)",
                "number": "39",
                "timestamp": "35:03"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Erick Morillo & Eddie Thoneick ft. Shawnee Taylor",
                "title": "Live Your Life",
                "number": "40",
                "timestamp": "36:17"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Afrojack",
                "title": "Bangduck (MOGUAI Remix)",
                "number": "41",
                "timestamp": "38:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "R. Kelly",
                "title": "Ignition",
                "number": "42",
                "timestamp": "38:52"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Chris Brown & Lil Wayne",
                "title": "I Can Only Imagine (R3hab Remix)",
                "number": "43",
                "timestamp": "41:17"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Maison & Dragen",
                "title": "Rio De Janeiro",
                "number": "44",
                "timestamp": "42:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "All The Small Things (Acappella)",
                "number": "45",
                "timestamp": "42:35"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tjernberg & Wess",
                "title": "Evolve",
                "number": "46",
                "timestamp": "45:41"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Pitbull ft. Ne-Yo & Afrojack & Nayer",
                "title": "Give Me Everything (Acappella)",
                "number": "47",
                "timestamp": "46:06"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Markus Gardeweg",
                "title": "Comex",
                "number": "48",
                "timestamp": "48:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Usher",
                "title": "Without You (Acappella)",
                "number": "49",
                "timestamp": "48:23"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bjornberg",
                "title": "Libella",
                "number": "50",
                "timestamp": "50:18"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Aqua",
                "title": "Barbie Girl",
                "number": "51",
                "timestamp": "50:49"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "P. Diddy",
                "title": "Missing You",
                "number": "52",
                "timestamp": "52:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ne-Yo",
                "title": "Let's Go",
                "number": "53",
                "timestamp": "52:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "ID (Jakob Liedholm Summerburst Edit)",
                "number": "54",
                "timestamp": "52:50"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hard Rock Sofa & Swanky Tunes",
                "title": "Apogee",
                "number": "55",
                "timestamp": "52:50"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Axwell, Sebastian Ingrosso, Steve Angello & Laidback Luke ft. Deborah Cox",
                "title": "Leave The World Behind (Acappella)",
                "number": "56",
                "timestamp": "54:58"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd",
                "title": "Slam The Door",
                "number": "57",
                "timestamp": "57:37"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "You Used To Hold Me (Acappella)",
                "number": "58",
                "timestamp": "57:43"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ti\u00ebsto & Allure",
                "title": "Pair Of Dice",
                "number": "59",
                "timestamp": "58:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Wynter Gordon",
                "title": "Til Death",
                "number": "60",
                "timestamp": "58:57"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Chris Crawford",
                "title": "Helios",
                "number": "61",
                "timestamp": "1:00:45"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Above & Beyond ft. Richard Bedford",
                "title": "Sun & Moon (Acappella)",
                "number": "62",
                "timestamp": "1:00:57"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fareoh & Archie",
                "title": "Feathers",
                "number": "63",
                "timestamp": "1:02:30"
            },
            "played_with": []
        }
    ]
}