export const bb5 = {
    "title": "Two Friends - Big Bootie Mix Volume 5 2014-02-18",
    "tracks": [
        {
            "main": {
                "artist": "The White Stripes vs. Gregori Klosman",
                "title": "Seven Nation Funk It (Alex Addea Mashup)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Third Eye Blind",
                "title": "Semi-Charmed Life",
                "number": "02",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Merk & Kremont",
                "title": "Gear",
                "number": "03",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Mike Posner",
                "title": "Cooler Than Me",
                "number": "04",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Walk The Moon",
                "title": "Anna Sun",
                "number": "05",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Cedric Gervais",
                "title": "Flip",
                "number": "06",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bastille",
                "title": "Pompeii (Audien Instrumental)",
                "number": "07",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Goo Goo Dolls",
                "title": "Name (Acappella)",
                "number": "08",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ben E King",
                "title": "Stand By Me (Acappella)",
                "number": "09",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Snow Patrol",
                "title": "Chasing Cars (Acappella)",
                "number": "10",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Overwerk",
                "title": "Daybreak (Gopro Hero3 Edit)",
                "number": "11",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "U2",
                "title": "Stuck In A Moment",
                "number": "12",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Global Deejays",
                "title": "Kids",
                "number": "13",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bastille",
                "title": "Pompeii (Acappella)",
                "number": "14",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Benny Benassi ft. Gary Go",
                "title": "Cinema (Acappella)",
                "number": "15",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Armin van Buuren ft. Trevor Guthrie",
                "title": "This Is What It Feels Like (Extended Mix)",
                "number": "16",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Paris & Simo",
                "title": "Nova",
                "number": "17",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Don Diablo & Matt Nash ft. Noonie Bao",
                "title": "Starlight (Could You Be Mine) (Otto Knows Remix)",
                "number": "18",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Cappa Regime",
                "title": "So Retro",
                "number": "19",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "AWOLNATION",
                "title": "Sail",
                "number": "20",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Lush & Simon",
                "title": "City Of Lights",
                "number": "21",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Oasis",
                "title": "Wonderwall (Acappella)",
                "number": "22",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bob Marley & The Wailers",
                "title": "Is This Love (Acappella)",
                "number": "23",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Electric Joy Ride",
                "title": "365",
                "number": "24",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Audien ft. Michael S.",
                "title": "Leaving You (Acappella)",
                "number": "25",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Killers",
                "title": "When You Were Young (Acappella)",
                "number": "26",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Fatrat",
                "title": "Dancing Naked",
                "number": "27",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Daniel Portman",
                "title": "Mantenido",
                "number": "28",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Taylor Swift",
                "title": "22",
                "number": "29",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Daleri",
                "title": "Hysteria",
                "number": "30",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Usher ft. Ludacris & Lil Jon",
                "title": "Yeah! (Acappella)",
                "number": "31",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Victor Niglio ft. Mr. Man",
                "title": "Jiggy",
                "number": "32",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bubba Sparxxx ft. Ying Yang Twins",
                "title": "Ms. New Booty",
                "number": "33",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Speed (Burn & Lotus F1 Team Mix)",
                "number": "34",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Queen & David Bowie",
                "title": "Under Pressure",
                "number": "35",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Adrian Lux",
                "title": "Teenage Crime (Acappella)",
                "number": "36",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Voodoo & Serano",
                "title": "Overload (Offbeat Remix)",
                "number": "37",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Syn Cole",
                "title": "April",
                "number": "38",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Zedd ft. Hayley Williams",
                "title": "Stay The Night (Acappella)",
                "number": "39",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Mako",
                "title": "Our Story (Acappella)",
                "number": "40",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Presets",
                "title": "Fall (Hook N Sling Remix)",
                "number": "41",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Gareth Emery ft. Lucy Saunders",
                "title": "Sanctuary",
                "number": "42",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Audien",
                "title": "Iris",
                "number": "43",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Script",
                "title": "For The First Time",
                "number": "44",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Coldplay",
                "title": "Every Teardrop Is A Waterfall (Acappella)",
                "number": "45",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dzeko & Torres vs. Sarah McLeod",
                "title": "Hurricane (Club Mix)",
                "number": "46",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Cheap Trick",
                "title": "I Want You To Want Me",
                "number": "47",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fareoh & Archie",
                "title": "2012 (Twenty Twelve)",
                "number": "48",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Two Friends ft. Jeff Sontag",
                "title": "Sedated (Acappella)",
                "number": "49",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Nause",
                "title": "Mellow",
                "number": "50",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avril Lavigne",
                "title": "Complicated",
                "number": "51",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Madeon",
                "title": "Icarus",
                "number": "52",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "OneRepublic",
                "title": "Stop & Stare (Acappella)",
                "number": "53",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sebastien Benett",
                "title": "Ghost Rider",
                "number": "54",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Young The Giant",
                "title": "My Body",
                "number": "55",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Deorro",
                "title": "Bootie In Your Face",
                "number": "56",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Beatles",
                "title": "Come Together",
                "number": "57",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dada Life",
                "title": "Everything Is Free (Deficio Remix)",
                "number": "58",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Eminem",
                "title": "We Made You",
                "number": "59",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Botnek",
                "title": "Through The Night",
                "number": "60",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sebastian Ingrosso & Tommy Trash ft. John Martin",
                "title": "Reload (Acappella)",
                "number": "61",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Guns N' Roses",
                "title": "Sweet Child O' Mine",
                "number": "62",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Wolfgang Gartner",
                "title": "There And Back (Tommy Noble Full Power Bootleg Remix)",
                "number": "63",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Taped Rai",
                "title": "Just One Last Time (DigitalFX Edit)",
                "number": "64",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Skrillex",
                "title": "With You, Friends",
                "number": "65",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii ft. Aloe Blacc",
                "title": "Wake Me Up (Acappella)",
                "number": "66",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Klingande",
                "title": "Jubel",
                "number": "67",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Martin Solveig & The Cataracs ft. Kyle",
                "title": "Hey Now (Acappella)",
                "number": "68",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "MitiS",
                "title": "Born",
                "number": "69",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Two Door Cinema Club",
                "title": "What You Know",
                "number": "70",
                "timestamp": ""
            },
            "played_with": []
        }
    ]
}