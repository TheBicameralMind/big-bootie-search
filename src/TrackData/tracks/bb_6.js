export const bb6 = {
    "title": "Two Friends - Big Bootie Mix Volume 6 2014-08-18",
    "tracks": [
        {
            "main": {
                "artist": "Bastille",
                "title": "Pompeii (Kat Krazy Remix)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Yellowcard",
                    "title": "Ocean Avenue (Acappella)",
                    "number": "w/",
                    "timestamp": "0:29"
                },
                {
                    "artist": "Tritonal ft. Phoebe Ryan",
                    "title": "Now Or Never (Chris Barnhart Remix)",
                    "number": "w/",
                    "timestamp": "1:12"
                },
                {
                    "artist": "Sean Kingston",
                    "title": "Beautiful Girls (Acappella)",
                    "number": "w/",
                    "timestamp": "1:57"
                }
            ]
        },
        {
            "main": {
                "artist": "Popeska ft. Luciana",
                "title": "The New Kings (Vicetone Remix)",
                "number": "02",
                "timestamp": "2:45"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "American Authors",
                "title": "Best Day Of My Life (Gazzo Remix)",
                "number": "03",
                "timestamp": "3:34"
            },
            "played_with": [
                {
                    "artist": "Marvin Gaye & Tammi Terrel",
                    "title": "Ain't No Mountain High Enough (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Max Elto",
                    "title": "Shadow Of The Sun (Mako Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Teknartist",
                "title": "Agartha",
                "number": "04",
                "timestamp": "5:04"
            },
            "played_with": [
                {
                    "artist": "Avicii ft. Dan Tyminski",
                    "title": "Hey Brother (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Laidback Luke ft. Jonathan Mendelsohn",
                    "title": "Timebomb (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Chuckie & Promise Land vs. Laidback Luke vs. Mercer",
                "title": "Breaking Up Timebombs (Kastra Bootleg)",
                "number": "05",
                "timestamp": "6:19"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Julian Calor",
                "title": "Typhoon",
                "number": "06",
                "timestamp": "7:19"
            },
            "played_with": [
                {
                    "artist": "Disclosure ft. Sam Smith",
                    "title": "Latch (Acappella)",
                    "number": "w/",
                    "timestamp": "7:56"
                }
            ]
        },
        {
            "main": {
                "artist": "Capital Cities",
                "title": "Safe And Sound (Cash Cash Instrumental Remix)",
                "number": "07",
                "timestamp": "9:27"
            },
            "played_with": [
                {
                    "artist": "Anna Kendrick",
                    "title": "Cups (When I'm Gone)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Jeff Sontag",
                "title": "Sedated",
                "number": "08",
                "timestamp": "12:11"
            },
            "played_with": [
                {
                    "artist": "Chromeo",
                    "title": "Jealous (I Ain't With It) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ehrencrona",
                "title": "This Is So Good",
                "number": "09",
                "timestamp": "14:00"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Love Story",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Wolfgang Gartner",
                "title": "Space Junk (Overwerk Remix)",
                "number": "10",
                "timestamp": "16:08"
            },
            "played_with": [
                {
                    "artist": "Daddy's Groove",
                    "title": "Stellar (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. Bon Iver",
                    "title": "Lost In The World (Share Bootleg)",
                    "number": "w/",
                    "timestamp": "18:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Katy Perry",
                "title": "Unconditionally (Syn Cole Remix)",
                "number": "11",
                "timestamp": "19:41"
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Good Riddance",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Elton John",
                    "title": "Crocodile Rock",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Example",
                    "title": "We'll Be Coming Back (Lush & Simon Bootleg)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Foxes",
                "title": "Holding OnTo Heaven (The Chainsmokers Instrumental Remix)",
                "number": "12",
                "timestamp": "22:41"
            },
            "played_with": [
                {
                    "artist": "Of Monsters & Men",
                    "title": "Little Talks (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "John De Sohn ft. Andreas Moe",
                "title": "Long Time (Oliver Twizt Instrumental)",
                "number": "13",
                "timestamp": "23:56"
            },
            "played_with": [
                {
                    "artist": "The All-American Rejects",
                    "title": "Gives You Hell",
                    "number": "w/",
                    "timestamp": "24:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Starkillers & Alex Kenji ft. Nadia Ali",
                "title": "Pressure (Alesso Instrumental Remix)",
                "number": "14",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Survivor",
                    "title": "Eye Of The Tiger (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Foster The People",
                    "title": "Helena Beat",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Otto Knows",
                "title": "Million Voices (TORN Remix)",
                "number": "15",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Milk N Cookies",
                "title": "Bring Da Funk Back",
                "number": "16",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Weezer",
                    "title": "Island In The Sun",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Bulla & Swede Dreams",
                "title": "Galactica",
                "number": "17",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Kelly Clarkson",
                    "title": "Since U Been Gone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "James Blunt",
                "title": "Bonfire Heart (eSQUIRE vs. Offbeat Remix)",
                "number": "18",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Blink-182",
                    "title": "Adam's Song (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Baauer & RL Grime",
                "title": "Infinite Daps",
                "number": "19",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "T.I.",
                "title": "Bring Em Out",
                "number": "20",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Eminem",
                    "title": "The Real Slim Shady (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bruno Mars",
                "title": "Treasure (Audien Remix)",
                "number": "21",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Duke Dumont ft. A*M*E",
                "title": "Need U (100%)",
                "number": "22",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Vicetone",
                "title": "Harmony",
                "number": "23",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Angus & Julia Stone",
                "title": "Big Jet Plane",
                "number": "24",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Steerner",
                "title": "Friends",
                "number": "25",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "OneRepublic",
                    "title": "Counting Stars (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta ft. Sia",
                "title": "Titanium (Instrumental)",
                "number": "26",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The All-American Rejects",
                    "title": "Move Along",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta ft. Sia",
                "title": "Titanium (dBerrie Remix)",
                "number": "27",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Nico & Vinz",
                    "title": "Am I Wrong (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kaskade ft. McKay Stevens",
                "title": "Atmosphere (Hook N Sling Remix)",
                "number": "28",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Levels",
                "number": "29",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Jeff Sontag",
                    "title": "Sedated (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Madeon",
                "title": "Cut The Kid",
                "number": "30",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "American Authors",
                    "title": "Best Day Of My Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien",
                "title": "Hindsight",
                "number": "31",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "Mr. Brightside (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Henry Fong & Mike Hawkins & Toby Green",
                "title": "Hot Steppa",
                "number": "32",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Pharrell Williams",
                    "title": "Happy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Klingande",
                "title": "Punga",
                "number": "33",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Kaskade ft. McKay Stevens",
                    "title": "Atmosphere (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Blackmill",
                "title": "Friend",
                "number": "34",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Calvin Harris",
                    "title": "Feel So Close (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cash Cash ft. Bebe Rexha",
                    "title": "Take Me Home (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Posner",
                    "title": "Please Don't Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}