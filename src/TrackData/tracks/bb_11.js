export const bb11 = {
    "title": "Two Friends - Big Bootie Mix Episode 11 2017-04-11",
    "tracks": [
        {
            "main": {
                "artist": "The Scrantones",
                "title": "The Office Theme Song",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Bastille",
                    "title": "Good Grief (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": "0:25"
                },
                {
                    "artist": "The Fray",
                    "title": "Over My Head (Cable Car)",
                    "number": "w/",
                    "timestamp": "0:40"
                },
                {
                    "artist": "Carly Rae Jepsen",
                    "title": "Call Me Maybe (Acappella)",
                    "number": "w/",
                    "timestamp": "1:26"
                },
                {
                    "artist": "Gnash ft. Olivia O'brien",
                    "title": "I Hate U, I Love U",
                    "number": "w/",
                    "timestamp": "2:16"
                },
                {
                    "artist": "Barenaked Ladies",
                    "title": "One Week",
                    "number": "w/",
                    "timestamp": "2:54"
                },
                {
                    "artist": "Antoine Delvig & Paul Vinx",
                    "title": "Blondies",
                    "number": "w/",
                    "timestamp": "3:09"
                },
                {
                    "artist": "JAY-Z",
                    "title": "Encore",
                    "number": "w/",
                    "timestamp": "3:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Jonas Aden & Brooks",
                "title": "Take Me Away",
                "number": "02",
                "timestamp": "3:40"
            },
            "played_with": [
                {
                    "artist": "The Chainsmokers ft. Emily Warren",
                    "title": "Paris (Acappella)",
                    "number": "w/",
                    "timestamp": "3:43"
                },
                {
                    "artist": "Avril Lavigne",
                    "title": "Sk8er Boi",
                    "number": "w/",
                    "timestamp": "4:14"
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Deb\u2019s Daughter",
                "title": "Crazy Love (MOTi Remix)",
                "number": "03",
                "timestamp": "5:00"
            },
            "played_with": [
                {
                    "artist": "Misterwives",
                    "title": "Reflections (Acappella)",
                    "number": "w/",
                    "timestamp": "5:00"
                },
                {
                    "artist": "Neiked ft. Dyo",
                    "title": "Sexual",
                    "number": "w/",
                    "timestamp": "5:38"
                }
            ]
        },
        {
            "main": {
                "artist": "Dzeko & Torres ft. Delaney Jane",
                "title": "L'amour Toujours (Ti\u00ebsto Edit)",
                "number": "04",
                "timestamp": "6:23"
            },
            "played_with": [
                {
                    "artist": "Good Charlotte",
                    "title": "Girls & Boys",
                    "number": "w/",
                    "timestamp": "6:30"
                },
                {
                    "artist": "DJ Snake ft. Justin Bieber",
                    "title": "Let Me Love You (Acappella)",
                    "number": "w/",
                    "timestamp": "7:00"
                },
                {
                    "artist": "Charli XCX",
                    "title": "Break The Rules",
                    "number": "w/",
                    "timestamp": "7:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Backstreet Boys",
                "title": "Everybody (Oski & Apashe & Lennon Bootleg)",
                "number": "05",
                "timestamp": "7:45"
            },
            "played_with": [
                {
                    "artist": "Ludacris",
                    "title": "Pimpin' All Over The World",
                    "number": "w/",
                    "timestamp": "8:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Thomas Hayes ft. Joni Fatora",
                "title": "Neon (Alluvion) (Ryos Remix)",
                "number": "06",
                "timestamp": "9:06"
            },
            "played_with": [
                {
                    "artist": "3 Doors Down",
                    "title": "Here Without You (Acappella)",
                    "number": "w/",
                    "timestamp": "9:11"
                },
                {
                    "artist": "Men At Work",
                    "title": "Down Under",
                    "number": "w/",
                    "timestamp": "9:40"
                },
                {
                    "artist": "Kanye West ft. T-Pain",
                    "title": "Good Life",
                    "number": "w/",
                    "timestamp": "10:24"
                }
            ]
        },
        {
            "main": {
                "artist": "The Killers",
                "title": "Mr. Brightside (Two Friends Remix)",
                "number": "07",
                "timestamp": "10:40"
            },
            "played_with": [
                {
                    "artist": "Avicii ft. Aloe Blacc",
                    "title": "Wake Me Up (Acappella)",
                    "number": "w/",
                    "timestamp": "10:47"
                },
                {
                    "artist": "Marshmello",
                    "title": "Alone (Acappella)",
                    "number": "w/",
                    "timestamp": "11:12"
                },
                {
                    "artist": "Sebastian Ingrosso & Tommy Trash ft. John Martin",
                    "title": "Reload (Acappella)",
                    "number": "w/",
                    "timestamp": "11:37"
                }
            ]
        },
        {
            "main": {
                "artist": "Mike Williams",
                "title": "Bambini",
                "number": "08",
                "timestamp": "12:30"
            },
            "played_with": [
                {
                    "artist": "Katy Perry ft. Juicy J",
                    "title": "Dark Horse (Acappella)",
                    "number": "w/",
                    "timestamp": "12:38"
                },
                {
                    "artist": "Whethan ft. Flux Pavilion & MAX",
                    "title": "Savage",
                    "number": "w/",
                    "timestamp": "13:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "No Money",
                "number": "09",
                "timestamp": "13:53"
            },
            "played_with": [
                {
                    "artist": "Zedd & Alessia Cara",
                    "title": "Stay (Acappella)",
                    "number": "w/",
                    "timestamp": "14:00"
                },
                {
                    "artist": "Ti\u00ebsto ft. Michel Zitron",
                    "title": "Red Lights (Acappella)",
                    "number": "w/",
                    "timestamp": "15:02"
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo ft. Y LUV",
                "title": "Never Touch The Ground",
                "number": "10",
                "timestamp": "15:17"
            },
            "played_with": [
                {
                    "artist": "R. Kelly",
                    "title": "I Believe I Can Fly",
                    "number": "w/",
                    "timestamp": "15:21"
                },
                {
                    "artist": "Nero",
                    "title": "Promises (Acappella)",
                    "number": "w/",
                    "timestamp": "16:05"
                },
                {
                    "artist": "Zombie Nation",
                    "title": "Kernkraft 400 (VAVO 2016 Bootleg)",
                    "number": "w/",
                    "timestamp": "16:20"
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "Feel So Close (Instrumental)",
                "number": "11",
                "timestamp": "16:50"
            },
            "played_with": [
                {
                    "artist": "Semisonic",
                    "title": "Closing Time",
                    "number": "w/",
                    "timestamp": "16:58"
                },
                {
                    "artist": "Calvin Harris",
                    "title": "Feel So Close (Big Z Remix)",
                    "number": "w/",
                    "timestamp": "17:28"
                },
                {
                    "artist": "Calvin Harris",
                    "title": "Feel So Close (Acappella)",
                    "number": "w/",
                    "timestamp": "17:28"
                },
                {
                    "artist": "Bingo Players",
                    "title": "Cry (Just A Little) (A-Trak & DiscoTech Remix)",
                    "number": "w/",
                    "timestamp": "17:42"
                },
                {
                    "artist": "Daft Punk",
                    "title": "Around The World (Acappella)",
                    "number": "w/",
                    "timestamp": "17:58"
                },
                {
                    "artist": "My Chemical Romance",
                    "title": "Welcome To The Black Parade",
                    "number": "w/",
                    "timestamp": "18:20"
                },
                {
                    "artist": "Don Diablo & Khrebto",
                    "title": "Got The Love (Vanze Bootleg)",
                    "number": "w/",
                    "timestamp": "18:58"
                },
                {
                    "artist": "Calvin Harris",
                    "title": "My Way (Acappella)",
                    "number": "w/",
                    "timestamp": "19:20"
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala ft. Bryn Christopher",
                "title": "Sweet Lovin'",
                "number": "12",
                "timestamp": "19:35"
            },
            "played_with": [
                {
                    "artist": "Vanessa Carlton",
                    "title": "A Thousand Miles",
                    "number": "w/",
                    "timestamp": "19:43"
                },
                {
                    "artist": "2Pac ft. Dr. Dre",
                    "title": "California Love (Acappella)",
                    "number": "w/",
                    "timestamp": "20:29"
                },
                {
                    "artist": "Calvin Harris ft. Florence Welch",
                    "title": "Sweet Nothing (Acappella)",
                    "number": "w/",
                    "timestamp": "20:53"
                }
            ]
        },
        {
            "main": {
                "artist": "Quintino & Hardwell vs. DJ Kool",
                "title": "Let Me Clear My Scorpion (Henry Fong Mashup)",
                "number": "13",
                "timestamp": "21:23"
            },
            "played_with": [
                {
                    "artist": "Kill FM",
                    "title": "Fresh",
                    "number": "w/",
                    "timestamp": "22:17"
                },
                {
                    "artist": "Eminem ft. Nate Dogg",
                    "title": "Shake That (Acappella)",
                    "number": "w/",
                    "timestamp": "22:32"
                }
            ]
        },
        {
            "main": {
                "artist": "Madison Mars",
                "title": "Milky Way",
                "number": "14",
                "timestamp": "22:47"
            },
            "played_with": [
                {
                    "artist": "La Roux",
                    "title": "Bulletproof (Acappella)",
                    "number": "w/",
                    "timestamp": "22:47"
                },
                {
                    "artist": "Dr. Dre ft. Snoop Dogg & Kurupt & Nate Dogg",
                    "title": "The Next Episode (Acappella)",
                    "number": "w/",
                    "timestamp": "23:18"
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. RAS",
                "title": "The Nights",
                "number": "15",
                "timestamp": "24:03"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "New Romantics",
                    "number": "w/",
                    "timestamp": "24:03"
                },
                {
                    "artist": "Whitney Houston",
                    "title": "I Wanna Dance With Somebody (Acappella)",
                    "number": "w/",
                    "timestamp": "24:35"
                },
                {
                    "artist": "Alan Walker ft. Iselin Solheim",
                    "title": "Faded (Acappella)",
                    "number": "w/",
                    "timestamp": "25:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Deniz Koyu ft. Wynter Gordon",
                "title": "Follow You",
                "number": "16",
                "timestamp": "25:20"
            },
            "played_with": [
                {
                    "artist": "Bastille",
                    "title": "Pompeii (Acappella)",
                    "number": "w/",
                    "timestamp": "25:49"
                },
                {
                    "artist": "Kelly Clarkson",
                    "title": "Behind These Hazel Eyes",
                    "number": "w/",
                    "timestamp": "26:19"
                }
            ]
        },
        {
            "main": {
                "artist": "Porter Robinson",
                "title": "Unison (Crankdat Re-Crank)",
                "number": "17",
                "timestamp": "27:04"
            },
            "played_with": [
                {
                    "artist": "Linkin Park",
                    "title": "Numb (Acappella)",
                    "number": "w/",
                    "timestamp": "27:17"
                },
                {
                    "artist": "Avicii ft. Simon Aldred",
                    "title": "Waiting For Love (Acappella)",
                    "number": "w/",
                    "timestamp": "27:40"
                },
                {
                    "artist": "Slips & Slurs",
                    "title": "Divided (VIP)",
                    "number": "w/",
                    "timestamp": "28:04"
                },
                {
                    "artist": "Eminem",
                    "title": "Lose Yourself (Acappella)",
                    "number": "w/",
                    "timestamp": "28:04"
                }
            ]
        },
        {
            "main": {
                "artist": "Arston ft. Johnny Kelvin",
                "title": "Beautiful Asian",
                "number": "18",
                "timestamp": "28:26"
            },
            "played_with": [
                {
                    "artist": "Porter Robinson",
                    "title": "Sad Machine",
                    "number": "w/",
                    "timestamp": "28:34"
                },
                {
                    "artist": "Imagine Dragons",
                    "title": "Demons (Acappella)",
                    "number": "w/",
                    "timestamp": "29:04"
                },
                {
                    "artist": "AlunaGeorge",
                    "title": "You Know You Like It (Acappella)",
                    "number": "w/",
                    "timestamp": "29:19"
                },
                {
                    "artist": "DJ Snake & AlunaGeorge",
                    "title": "You Know You Like It (Tchami Remix)",
                    "number": "w/",
                    "timestamp": "29:34"
                }
            ]
        },
        {
            "main": {
                "artist": "Murtagh & Aiobahn",
                "title": "Follow Me",
                "number": "19",
                "timestamp": "29:59"
            },
            "played_with": [
                {
                    "artist": "Nico & Vinz",
                    "title": "Am I Wrong (Acappella)",
                    "number": "w/",
                    "timestamp": "30:07"
                },
                {
                    "artist": "The Outfield",
                    "title": "Your Love",
                    "number": "w/",
                    "timestamp": "30:53"
                },
                {
                    "artist": "Calvin Harris ft. Ellie Goulding",
                    "title": "I Need Your Love (Acappella)",
                    "number": "w/",
                    "timestamp": "31:35"
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris ft. Ellie Goulding",
                "title": "I Need Your Love",
                "number": "20",
                "timestamp": "31:52"
            },
            "played_with": [
                {
                    "artist": "X Ambassadors",
                    "title": "Unsteady",
                    "number": "w/",
                    "timestamp": "31:59"
                },
                {
                    "artist": "Britney Spears",
                    "title": "Hold It Against Me",
                    "number": "w/",
                    "timestamp": "32:13"
                },
                {
                    "artist": "Calvin Harris ft. Ellie Goulding",
                    "title": "I Need Your Love (Xristo Remix)",
                    "number": "w/",
                    "timestamp": "32:31"
                },
                {
                    "artist": "Kungs vs. Cookin' On 3 Burners ft. Kylie Auldist",
                    "title": "This Girl (JAKKO & Marck! Bootleg)",
                    "number": "w/",
                    "timestamp": "32:45"
                },
                {
                    "artist": "The Game ft. 50 Cent",
                    "title": "This Is How We Do",
                    "number": "w/",
                    "timestamp": "33:01"
                }
            ]
        },
        {
            "main": {
                "artist": "Sebastian Ingrosso & Tommy Trash ft. John Martin",
                "title": "Reload",
                "number": "21",
                "timestamp": "33:16"
            },
            "played_with": [
                {
                    "artist": "Alessia Cara",
                    "title": "Scars To Your Beautiful (Acappella)",
                    "number": "w/",
                    "timestamp": "33:24"
                },
                {
                    "artist": "Oliver Heldens & Shaun Frank ft. Delaney Jane",
                    "title": "Shades Of Grey (Acappella)",
                    "number": "w/",
                    "timestamp": "34:08"
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo",
                "title": "Feel The Love",
                "number": "22",
                "timestamp": "34:54"
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Cosmos & Creature",
                    "title": "Out Of Love",
                    "number": "w/",
                    "timestamp": "34:58"
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "I Write Sins Not Tragedies",
                    "number": "w/",
                    "timestamp": "35:30"
                },
                {
                    "artist": "Two Friends ft. Cosmos & Creature",
                    "title": "Out Of Love (Culture Code Remix)",
                    "number": "w/",
                    "timestamp": "36:06"
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Cosmos & Creature",
                "title": "Out Of Love",
                "number": "23",
                "timestamp": "36:37"
            },
            "played_with": [
                {
                    "artist": "Jason Mraz",
                    "title": "I'm Yours",
                    "number": "w/",
                    "timestamp": "36:37"
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "I Write Sins Not Tragedies",
                    "number": "w/",
                    "timestamp": "37:00"
                }
            ]
        },
        {
            "main": {
                "artist": "Valentino Khan",
                "title": "Deep Down Low (Dapp Retwerk)",
                "number": "24",
                "timestamp": "37:24"
            },
            "played_with": [
                {
                    "artist": "JAY-Z",
                    "title": "99 Problems (Acappella)",
                    "number": "w/",
                    "timestamp": "38:00"
                }
            ]
        },
        {
            "main": {
                "artist": "Bali Bandits",
                "title": "Kaboom",
                "number": "25",
                "timestamp": "38:37"
            },
            "played_with": [
                {
                    "artist": "Montell Jordan",
                    "title": "This Is How We Do It (Acappella)",
                    "number": "w/",
                    "timestamp": "38:37"
                }
            ]
        },
        {
            "main": {
                "artist": "DJ Snake & Yellow Claw",
                "title": "Ocho Cinco (Henry Fong Remix)",
                "number": "26",
                "timestamp": "39:58"
            },
            "played_with": [
                {
                    "artist": "Jon Bellion ft. Travis Mendes",
                    "title": "All Time Low",
                    "number": "w/",
                    "timestamp": "39:59"
                }
            ]
        },
        {
            "main": {
                "artist": "Class & Clowns",
                "title": "ID 40",
                "number": "27",
                "timestamp": "41:12"
            },
            "played_with": [
                {
                    "artist": "JAY-Z ft. Mr. Hudson",
                    "title": "Forever Young (Acappella)",
                    "number": "w/",
                    "timestamp": "41:13"
                },
                {
                    "artist": "Sigala ft. Bryn Christopher",
                    "title": "Sweet Lovin' (Acappella)",
                    "number": "w/",
                    "timestamp": "41:27"
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Emily Warren",
                "title": "Paris (Pegboard Nerds Remix)",
                "number": "28",
                "timestamp": "42:27"
            },
            "played_with": [
                {
                    "artist": "Fall Out Boy",
                    "title": "Sugar, We're Goin' Down",
                    "number": "w/",
                    "timestamp": "42:37"
                },
                {
                    "artist": "Lil Jon & The Eastside Boyz & Ying Yang Twins",
                    "title": "Get Low (Acappella)",
                    "number": "w/",
                    "timestamp": "43:35"
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo",
                "title": "Nothing To Lose",
                "number": "29",
                "timestamp": "43:54"
            },
            "played_with": [
                {
                    "artist": "Kanye West ft. Rihanna",
                    "title": "All Of The Lights (Acappella)",
                    "number": "w/",
                    "timestamp": "43:59"
                },
                {
                    "artist": "Two Friends ft. MAX",
                    "title": "Pacific Coast Highway (Acappella)",
                    "number": "w/",
                    "timestamp": "44:14"
                },
                {
                    "artist": "Timbaland ft. OneRepublic",
                    "title": "Apologize (Acappella)",
                    "number": "w/",
                    "timestamp": "44:28"
                }
            ]
        },
        {
            "main": {
                "artist": "DubVision",
                "title": "Sweet Harmony",
                "number": "30",
                "timestamp": "45:23"
            },
            "played_with": [
                {
                    "artist": "The Cure",
                    "title": "Just Like Heaven",
                    "number": "w/",
                    "timestamp": "45:37"
                },
                {
                    "artist": "Melanie Martinez",
                    "title": "Pity Party",
                    "number": "w/",
                    "timestamp": "46:01"
                },
                {
                    "artist": "Don Diablo",
                    "title": "Back In Time",
                    "number": "w/",
                    "timestamp": "46:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Jack U\u0308 ft. Kiesza",
                "title": "Take \u00dc There",
                "number": "31",
                "timestamp": "47:00"
            },
            "played_with": [
                {
                    "artist": "Fountains Of Wayne",
                    "title": "Stacy's Mom",
                    "number": "w/",
                    "timestamp": "47:00"
                },
                {
                    "artist": "MIMS",
                    "title": "This Is Why I'm Hot (Acappella)",
                    "number": "w/",
                    "timestamp": "47:54"
                }
            ]
        },
        {
            "main": {
                "artist": "Justin Caruso ft. Victoria Zaro",
                "title": "Talk About Me",
                "number": "32",
                "timestamp": "48:06"
            },
            "played_with": [
                {
                    "artist": "Ed Sheeran",
                    "title": "Shape Of You (Acappella)",
                    "number": "w/",
                    "timestamp": "48:15"
                },
                {
                    "artist": "The Chainsmokers ft. ROZES",
                    "title": "Roses",
                    "number": "w/",
                    "timestamp": "49:04"
                },
                {
                    "artist": "Hailee Steinfeld & Grey ft. Zedd",
                    "title": "Starving (Acappella)",
                    "number": "w/",
                    "timestamp": "49:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Tim Berg",
                "title": "Bromance (Avicii Arena Mix)",
                "number": "33",
                "timestamp": "49:47"
            },
            "played_with": [
                {
                    "artist": "Hailee Steinfeld & Grey ft. Zedd",
                    "title": "Starving (Acappella)",
                    "number": "w/",
                    "timestamp": "49:55"
                },
                {
                    "artist": "The Chainsmokers ft. ROZES",
                    "title": "Roses (Acappella)",
                    "number": "w/",
                    "timestamp": "50:11"
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix & Dua Lipa",
                "title": "Scared To Be Lonely",
                "number": "34",
                "timestamp": "51:11"
            },
            "played_with": [
                {
                    "artist": "Mumford & Sons",
                    "title": "The Cave",
                    "number": "w/",
                    "timestamp": "51:24"
                },
                {
                    "artist": "Adele",
                    "title": "Set Fire To The Rain (Acappella)",
                    "number": "w/",
                    "timestamp": "51:59"
                },
                {
                    "artist": "Justin Bieber",
                    "title": "Love Yourself (Acappella)",
                    "number": "w/",
                    "timestamp": "52:13"
                }
            ]
        },
        {
            "main": {
                "artist": "Jay Hardway",
                "title": "Bootcamp",
                "number": "35",
                "timestamp": "52:27"
            },
            "played_with": [
                {
                    "artist": "Twenty One Pilots",
                    "title": "Heathens (Acappella)",
                    "number": "w/",
                    "timestamp": "52:31"
                },
                {
                    "artist": "The White Stripes",
                    "title": "Seven Nation Army (Acappella)",
                    "number": "w/",
                    "timestamp": "53:01"
                },
                {
                    "artist": "Dropgun",
                    "title": "Ninja",
                    "number": "w/",
                    "timestamp": "53:29"
                }
            ]
        },
        {
            "main": {
                "artist": "Mako",
                "title": "Let Go Of The Wheel (WE ARE FURY Remix)",
                "number": "36",
                "timestamp": "54:01"
            },
            "played_with": [
                {
                    "artist": "Florence + The Machine",
                    "title": "Dog Days",
                    "number": "w/",
                    "timestamp": "54:04"
                },
                {
                    "artist": "Kiiara",
                    "title": "Gold",
                    "number": "w/",
                    "timestamp": "54:39"
                }
            ]
        },
        {
            "main": {
                "artist": "Tove Lo",
                "title": "Cool Girl (Two Friends Remix)",
                "number": "37",
                "timestamp": "55:13"
            },
            "played_with": [
                {
                    "artist": "Cheat Codes & Kris Kross Amsterdam",
                    "title": "SEX (Acappella)",
                    "number": "w/",
                    "timestamp": "55:23"
                },
                {
                    "artist": "The Fray",
                    "title": "You Found Me",
                    "number": "w/",
                    "timestamp": "56:01"
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix & Mesto",
                "title": "WIEE",
                "number": "38",
                "timestamp": "56:38"
            },
            "played_with": [
                {
                    "artist": "Kygo & Selena Gomez",
                    "title": "It Ain't Me (Acappella)",
                    "number": "w/",
                    "timestamp": "56:53"
                },
                {
                    "artist": "Maroon 5 ft. Future",
                    "title": "Cold (Acappella)",
                    "number": "w/",
                    "timestamp": "57:13"
                },
                {
                    "artist": "Clarx & Uplink",
                    "title": "Titans",
                    "number": "w/",
                    "timestamp": "57:29"
                }
            ]
        },
        {
            "main": {
                "artist": "Alan Walker ft. Iselin Solheim",
                "title": "Faded (Instrumental Version)",
                "number": "39",
                "timestamp": "58:01"
            },
            "played_with": [
                {
                    "artist": "Coldplay",
                    "title": "The Scientist (Acappella)",
                    "number": "w/",
                    "timestamp": "58:11"
                },
                {
                    "artist": "Rent",
                    "title": "Seasons Of Love",
                    "number": "w/",
                    "timestamp": "58:32"
                },
                {
                    "artist": "Flume ft. Tove Lo",
                    "title": "Say It (Acappella)",
                    "number": "w/",
                    "timestamp": "58:56"
                },
                {
                    "artist": "Afroman",
                    "title": "Because I Got High (Acappella)",
                    "number": "w/",
                    "timestamp": "59:17"
                },
                {
                    "artist": "Steve Miller Band",
                    "title": "The Joker",
                    "number": "w/",
                    "timestamp": "59:38"
                },
                {
                    "artist": "Coldplay",
                    "title": "Paradise (Acappella)",
                    "number": "w/",
                    "timestamp": "59:50"
                },
                {
                    "artist": "Louis The Child & Icona Pop",
                    "title": "Weekend",
                    "number": "w/",
                    "timestamp": "1:00:01"
                }
            ]
        }
    ]
}