export const bb7 = {
    "title": "Two Friends - Big Bootie Mix Volume 7 2015-04-07",
    "tracks": [
        {
            "main": {
                "artist": "Martin Garrix & MOTi ft. Jenny Wahlstr\u00f6m",
                "title": "Virus (How About Now)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Ed Sheeran",
                    "title": "Don't (Acappella)",
                    "number": "w/",
                    "timestamp": "0:20"
                },
                {
                    "artist": "Coldplay",
                    "title": "Viva La Vida (Acappella)",
                    "number": "w/",
                    "timestamp": "0:58"
                },
                {
                    "artist": "Ed Sheeran",
                    "title": "Don't (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": "1:00"
                }
            ]
        },
        {
            "main": {
                "artist": "Pep & Rash",
                "title": "Fatality (Quintino Edit)",
                "number": "02",
                "timestamp": "1:37"
            },
            "played_with": [
                {
                    "artist": "Coldplay",
                    "title": "Yellow (Acappella)",
                    "number": "w/",
                    "timestamp": "1:37"
                }
            ]
        },
        {
            "main": {
                "artist": "Pierce Fulton",
                "title": "Kuaga",
                "number": "03",
                "timestamp": "3:08"
            },
            "played_with": [
                {
                    "artist": "3LAU ft. Bright Lights",
                    "title": "How You Love Me (Acappella)",
                    "number": "w/",
                    "timestamp": "3:10"
                },
                {
                    "artist": "Galantis",
                    "title": "Runaway (U & I) (Acappella)",
                    "number": "w/",
                    "timestamp": "3:54"
                }
            ]
        },
        {
            "main": {
                "artist": "RL Grime",
                "title": "Core",
                "number": "04",
                "timestamp": "5:39"
            },
            "played_with": [
                {
                    "artist": "Mark Ronson ft. Bruno Mars",
                    "title": "Uptown Funk (Acappella)",
                    "number": "w/",
                    "timestamp": "6:05"
                },
                {
                    "artist": "Eminem",
                    "title": "Superman (Acappella)",
                    "number": "w/",
                    "timestamp": "6:17"
                }
            ]
        },
        {
            "main": {
                "artist": "Deorro ft. DyCy",
                "title": "Five Hours (Don't Hold Me Back) (Empia Remix)",
                "number": "05",
                "timestamp": "7:09"
            },
            "played_with": [
                {
                    "artist": "Clean Bandit ft. Jess Glynne",
                    "title": "Rather Be (Acappella)",
                    "number": "w/",
                    "timestamp": "7:35"
                },
                {
                    "artist": "The Chainsmokers ft. SirenXX",
                    "title": "KANYE (Acappella)",
                    "number": "w/",
                    "timestamp": "8:28"
                }
            ]
        },
        {
            "main": {
                "artist": "Clean Bandit ft. Jess Glynne",
                "title": "Rather Be (Instrumental Mix)",
                "number": "06",
                "timestamp": "9:21"
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Breach The Summit",
                    "title": "Long Way Home (Acappella)",
                    "number": "w/",
                    "timestamp": "9:34"
                },
                {
                    "artist": "Two Friends ft. Jeff Sontag & I Am Lightyear",
                    "title": "Brighter (Acappella)",
                    "number": "w/",
                    "timestamp": "9:51"
                },
                {
                    "artist": "Milky Chance",
                    "title": "Stolen Dance (Acappella)",
                    "number": "w/",
                    "timestamp": "10:06"
                }
            ]
        },
        {
            "main": {
                "artist": "Hardwell ft. Amba Shepherd",
                "title": "Apollo",
                "number": "07",
                "timestamp": "11:25"
            },
            "played_with": [
                {
                    "artist": "Naughty Boy ft. Sam Smith",
                    "title": "La La La (Acappella)",
                    "number": "w/",
                    "timestamp": "11:32"
                },
                {
                    "artist": "Cazzette ft. The High",
                    "title": "Sleepless (Acappella)",
                    "number": "w/",
                    "timestamp": "12:16"
                },
                {
                    "artist": "Vice ft. Mike Taylor",
                    "title": "World Is Our Playground",
                    "number": "w/",
                    "timestamp": "12:28"
                }
            ]
        },
        {
            "main": {
                "artist": "Naughty Boy ft. Sam Smith",
                "title": "La La La (James Egbert Remix)",
                "number": "08",
                "timestamp": "13:19"
            },
            "played_with": [
                {
                    "artist": "Meghan Trainor",
                    "title": "All About That Bass (Acappella)",
                    "number": "w/",
                    "timestamp": "13:36"
                },
                {
                    "artist": "BORGEOUS ft. Julia Michaels",
                    "title": "Invincible (Acappella)",
                    "number": "w/",
                    "timestamp": "15:09"
                },
                {
                    "artist": "Porter Robinson ft. Bright Lights",
                    "title": "Language",
                    "number": "w/",
                    "timestamp": "16:10"
                }
            ]
        },
        {
            "main": {
                "artist": "Knife Party vs. What So Not",
                "title": "Touched Mode (Henry Fong Mashup)",
                "number": "09",
                "timestamp": "16:55"
            },
            "played_with": [
                {
                    "artist": "The Notorious B.I.G. ft. Eminem",
                    "title": "Dead Wrong (Acappella)",
                    "number": "w/",
                    "timestamp": "17:37"
                },
                {
                    "artist": "Earth, Wind & Fire",
                    "title": "September (Acappella)",
                    "number": "w/",
                    "timestamp": "18:27"
                },
                {
                    "artist": "ODESZA ft. Zyra",
                    "title": "Say My Name (Mazde Remix)",
                    "number": "w/",
                    "timestamp": "19:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Parade Of Lights",
                "title": "We're The Kids Now (Cash Cash & Gazzo Remix)",
                "number": "10",
                "timestamp": "19:54"
            },
            "played_with": [
                {
                    "artist": "Rick Springfield",
                    "title": "Jessie's Girl",
                    "number": "w/",
                    "timestamp": "19:55"
                },
                {
                    "artist": "Maroon 5",
                    "title": "Daylight",
                    "number": "w/",
                    "timestamp": "20:24"
                },
                {
                    "artist": "Urban Cone",
                    "title": "Freak (Thand Remix)",
                    "number": "w/",
                    "timestamp": "20:55"
                }
            ]
        },
        {
            "main": {
                "artist": "Murtagh",
                "title": "Alpha",
                "number": "11",
                "timestamp": "21:44"
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Wake Me Up When September Ends (Acappella)",
                    "number": "w/",
                    "timestamp": "22:02"
                },
                {
                    "artist": "Bon Jovi",
                    "title": "Livin' On A Prayer (Acappella)",
                    "number": "w/",
                    "timestamp": "22:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Dirty South ft. Sam Martin",
                "title": "Unbreakable (Elephante Remix)",
                "number": "12",
                "timestamp": "23:17"
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "Human (Acappella)",
                    "number": "w/",
                    "timestamp": "23:20"
                },
                {
                    "artist": "Michael Calfan",
                    "title": "Prelude",
                    "number": "w/",
                    "timestamp": "24:49"
                },
                {
                    "artist": "The Fray",
                    "title": "How To Save A Life (Acappella)",
                    "number": "w/",
                    "timestamp": "25:21"
                }
            ]
        },
        {
            "main": {
                "artist": "Syn Cole ft. Madame Buttons",
                "title": "Miami 82 (Avicii Edit)",
                "number": "13",
                "timestamp": "26:06"
            },
            "played_with": [
                {
                    "artist": "Alesso ft. Tove Lo",
                    "title": "Heroes (We Could Be) (Acappella)",
                    "number": "w/",
                    "timestamp": "26:14"
                },
                {
                    "artist": "Adele",
                    "title": "Set Fire To The Rain (Acappella)",
                    "number": "w/",
                    "timestamp": "26:42"
                }
            ]
        },
        {
            "main": {
                "artist": "Arizon",
                "title": "All I Have",
                "number": "14",
                "timestamp": "27:43"
            },
            "played_with": [
                {
                    "artist": "Lana Del Rey",
                    "title": "Summertime Sadness (Acappella)",
                    "number": "w/",
                    "timestamp": "27:45"
                },
                {
                    "artist": "The Killers",
                    "title": "Spaceman (Acappella)",
                    "number": "w/",
                    "timestamp": "28:15"
                },
                {
                    "artist": "Tim Berg ft. Amanda Wilson",
                    "title": "Seek Bromance (X5IGHT Remix)",
                    "number": "w/",
                    "timestamp": "28:45"
                }
            ]
        },
        {
            "main": {
                "artist": "Katy Perry",
                "title": "The One Who Got Away (Tommie Sunshine & Disco Fries Club Mix)",
                "number": "15",
                "timestamp": "29:34"
            },
            "played_with": [
                {
                    "artist": "Lady Antebellum",
                    "title": "Need You Now",
                    "number": "w/",
                    "timestamp": "30:19"
                }
            ]
        },
        {
            "main": {
                "artist": "Julian Calor ft. Rienk Speelman",
                "title": "Evolve",
                "number": "16",
                "timestamp": "31:21"
            },
            "played_with": [
                {
                    "artist": "Adele",
                    "title": "Rolling In The Deep (Acappella)",
                    "number": "w/",
                    "timestamp": "31:37"
                },
                {
                    "artist": "Otto Knows",
                    "title": "Million Voices (Acappella)",
                    "number": "w/",
                    "timestamp": "32:07"
                },
                {
                    "artist": "Maroon 5",
                    "title": "This Love (Acappella)",
                    "number": "w/",
                    "timestamp": "32:08"
                },
                {
                    "artist": "Calvin Harris ft. Ayah Marar",
                    "title": "Thinking About You (Acappella)",
                    "number": "w/",
                    "timestamp": "32:52"
                }
            ]
        },
        {
            "main": {
                "artist": "Vicetone",
                "title": "United We Dance",
                "number": "17",
                "timestamp": "33:22"
            },
            "played_with": [
                {
                    "artist": "Tritonal & Paris Blohm ft. Sterling Fox",
                    "title": "Colors (Acappella)",
                    "number": "w/",
                    "timestamp": "33:29"
                },
                {
                    "artist": "Afrojack ft. Eva Simons",
                    "title": "Take Over Control (Acappella)",
                    "number": "w/",
                    "timestamp": "33:58"
                }
            ]
        },
        {
            "main": {
                "artist": "Dimitri Vangelis & Wyman X Steve Angello",
                "title": "Payback",
                "number": "18",
                "timestamp": "35:00"
            },
            "played_with": [
                {
                    "artist": "Kiesza",
                    "title": "Hideaway (Acappella)",
                    "number": "w/",
                    "timestamp": "35:14"
                },
                {
                    "artist": "Dimitri Vangelis & Wyman X Steve Angello",
                    "title": "Payback (Havok Roth & Enzo Picardi & Party Thieves Remix)",
                    "number": "w/",
                    "timestamp": "36:45"
                }
            ]
        },
        {
            "main": {
                "artist": "Plissken",
                "title": "Storm",
                "number": "19",
                "timestamp": "38:07"
            },
            "played_with": [
                {
                    "artist": "Vice ft. Mike Taylor",
                    "title": "World Is Our Playground (Acappella)",
                    "number": "w/",
                    "timestamp": "38:22"
                },
                {
                    "artist": "Above & Beyond ft. Richard Bedford",
                    "title": "Sun & Moon (Acappella)",
                    "number": "w/",
                    "timestamp": "38:51"
                },
                {
                    "artist": "BORGEOUS ft. Julia Michaels",
                    "title": "Invincible (Markus Cole Remix)",
                    "number": "w/",
                    "timestamp": "39:39"
                }
            ]
        },
        {
            "main": {
                "artist": "Icona Pop",
                "title": "Just Another Night (DubVision Remix)",
                "number": "20",
                "timestamp": "40:38"
            },
            "played_with": [
                {
                    "artist": "Sara Bareilles",
                    "title": "Love Song",
                    "number": "w/",
                    "timestamp": "40:45"
                },
                {
                    "artist": "Arty ft. Chris James",
                    "title": "Together We Are (Audien Remix)",
                    "number": "w/",
                    "timestamp": "41:46"
                }
            ]
        },
        {
            "main": {
                "artist": "Jay Cosmic & Husman",
                "title": "Universe",
                "number": "21",
                "timestamp": "42:45"
            },
            "played_with": [
                {
                    "artist": "Faul & Wad Ad vs. Pnau",
                    "title": "Changes (Acappella)",
                    "number": "w/",
                    "timestamp": "43:00"
                },
                {
                    "artist": "Axwell, Sebastian Ingrosso, Steve Angello & Laidback Luke ft. Deborah Cox",
                    "title": "Leave The World Behind (Acappella)",
                    "number": "w/",
                    "timestamp": "43:14"
                }
            ]
        },
        {
            "main": {
                "artist": "RL Grime & What So Not",
                "title": "Tell Me",
                "number": "22",
                "timestamp": "44:00"
            },
            "played_with": [
                {
                    "artist": "Coldplay",
                    "title": "Viva La Vida (Acappella)",
                    "number": "w/",
                    "timestamp": "44:13"
                },
                {
                    "artist": "Eminem ft. Nate Dogg",
                    "title": "Shake That (Acappella)",
                    "number": "w/",
                    "timestamp": "44:53"
                }
            ]
        },
        {
            "main": {
                "artist": "Itro & Tobu",
                "title": "Cloud 9",
                "number": "23",
                "timestamp": "45:22"
            },
            "played_with": [
                {
                    "artist": "Counting Crows",
                    "title": "Accidentally In Love",
                    "number": "w/",
                    "timestamp": "45:22"
                },
                {
                    "artist": "Taylor Swift",
                    "title": "We Are Never Getting Back Together",
                    "number": "w/",
                    "timestamp": "45:52"
                },
                {
                    "artist": "Puppet ft. The Eden Project",
                    "title": "Scribble (Murtagh Remix)",
                    "number": "w/",
                    "timestamp": "46:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "Feel So Close (Instrumental)",
                "number": "24",
                "timestamp": "46:37"
            },
            "played_with": [
                {
                    "artist": "Smash Mouth",
                    "title": "All Star (Acappella)",
                    "number": "w/",
                    "timestamp": "46:37"
                },
                {
                    "artist": "Calvin Harris vs. Daft Punk vs. Wolfgang Gartner",
                    "title": "Feel So Close vs. Around World vs. Shrunken Heads (Fareoh Bootleg)",
                    "number": "w/",
                    "timestamp": "47:07"
                },
                {
                    "artist": "Cash Cash ft. Bebe Rexha",
                    "title": "Take Me Home (Acappella)",
                    "number": "w/",
                    "timestamp": "47:14"
                }
            ]
        },
        {
            "main": {
                "artist": "Oovee",
                "title": "Lanikai",
                "number": "25",
                "timestamp": "48:38"
            },
            "played_with": [
                {
                    "artist": "Carl Douglas",
                    "title": "Kung Fu Fighting (Acappella)",
                    "number": "w/",
                    "timestamp": "48:40"
                },
                {
                    "artist": "Alex Clare",
                    "title": "Too Close (Acappella)",
                    "number": "w/",
                    "timestamp": "49:22"
                },
                {
                    "artist": "Arty",
                    "title": "Flashback",
                    "number": "w/",
                    "timestamp": "49:23"
                }
            ]
        },
        {
            "main": {
                "artist": "Vicetone",
                "title": "Lowdown",
                "number": "26",
                "timestamp": "50:37"
            },
            "played_with": [
                {
                    "artist": "Justice vs. Simian",
                    "title": "We Are Your Friends (Acappella)",
                    "number": "w/",
                    "timestamp": "50:52"
                }
            ]
        },
        {
            "main": {
                "artist": "ODESZA ft. Zyra",
                "title": "Say My Name",
                "number": "27",
                "timestamp": "52:22"
            },
            "played_with": [
                {
                    "artist": "Pierce Fulton",
                    "title": "Kuaga (Acappella)",
                    "number": "w/",
                    "timestamp": "52:22"
                },
                {
                    "artist": "ODESZA ft. Zyra",
                    "title": "Say My Name",
                    "number": "w/",
                    "timestamp": "52:39"
                },
                {
                    "artist": "RAC ft. Matthew Koma",
                    "title": "Cheap Sunglasses",
                    "number": "w/",
                    "timestamp": "52:39"
                },
                {
                    "artist": "Kanye West ft. Lupe Fiasco",
                    "title": "Touch The Sky (Acappella)",
                    "number": "w/",
                    "timestamp": "53:13"
                },
                {
                    "artist": "Ivan Gough & Feenixpawl ft. Georgi Kay",
                    "title": "In My Mind (Acappella)",
                    "number": "w/",
                    "timestamp": "53:13"
                },
                {
                    "artist": "The Temper Trap",
                    "title": "Sweet Disposition (Acappella)",
                    "number": "w/",
                    "timestamp": "55:34"
                }
            ]
        }
    ]
}