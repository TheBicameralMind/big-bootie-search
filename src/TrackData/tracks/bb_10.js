export const bb10 = {
    "title": "Two Friends - Big Bootie Mix Volume 10 2016-10-06",
    "tracks": [
        {
            "main": {
                "artist": "Citizen Cope",
                "title": "Let The Drummer Kick",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Birdy",
                    "title": "Keeping Your Head Up (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shaun Frank & KSHMR ft. Delaney Jane",
                    "title": "Heaven (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Fray",
                    "title": "How To Save A Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Otto Knows",
                    "title": "Million Voices (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Birdy",
                    "title": "Keeping Your Head Up (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alice Deejay",
                "title": "Better Off Alone (Instrumental Mix)",
                "number": "02",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "OutKast",
                    "title": "Ms. Jackson (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alice Deejay vs. Galantis vs. SCNDL",
                    "title": "Better Off Alone vs. Runaway (U&I) vs. Otis (WeDamnz Reboot)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alice Deejay",
                    "title": "Better Off Alone (ZAXX Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd ft. Selena Gomez",
                    "title": "I Want You To Know (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "I Miss You (Two Friends & Shoolz VIP Remix)",
                "number": "03",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Alice Deejay",
                    "title": "Better Off Alone (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Matchbox Twenty",
                    "title": "How Far We've Come",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Yeah Yeah Yeahs",
                "title": "Heads Will Roll (Steve Reece 2K15 Reboot)",
                "number": "04",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Martin Solveig",
                    "title": "The Night Out (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Spice Girls",
                    "title": "Wannabe (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Askery & Lash ft. Laurell",
                "title": "Float Alone",
                "number": "05",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Kungs vs. Cookin' On 3 Burners ft. Kylie Auldist",
                    "title": "This Girl (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Halsey",
                    "title": "New Americana",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Loge21",
                    "title": "Drop That",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "D12",
                    "title": "My Band",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Norman Greenbaum",
                "title": "Spirit In The Sky",
                "number": "06",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Calvin Harris ft. Rihanna",
                    "title": "This Is What You Came For (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars",
                    "title": "Future Is Now",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alien Ant Farm",
                    "title": "Smooth Criminal",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Modern Machines",
                "title": "Waiting For You",
                "number": "07",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Miike Snow",
                    "title": "Genghis Khan",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hot Chocolate",
                    "title": "You Sexy Thing (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MERCER & SayMyName",
                    "title": "Wanted",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dirty Audio",
                "title": "Gorilla Glue",
                "number": "08",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Rick Ross",
                    "title": "Hustlin' (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ludacris",
                    "title": "How Low (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "Without Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DJ MERCER",
                "title": "Opium",
                "number": "09",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Justin Timberlake",
                    "title": "Can't Stop The Feeling (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mako",
                "title": "Way Back Home (Two Friends Remix)",
                "number": "10",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Halsey",
                    "title": "Colors",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mako",
                    "title": "Way Back Home (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ryos ft. KARRA",
                "title": "Where We Are",
                "number": "11",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "MGMT",
                    "title": "Time To Pretend",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Selena Gomez",
                    "title": "Kill Em With Kindness",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "C&C Music Factory",
                    "title": "Gonna Make You Sweat (Everybody Dance Now)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Starkillers & Alex Kenji ft. Nadia Ali",
                "title": "Pressure (Alesso Remix)",
                "number": "12",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "DJ Snake & AlunaGeorge",
                    "title": "You Know You Like It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dimitri Vegas & Like Mike vs. Sander van Doorn",
                "title": "Project T (Martin Garrix Remix)",
                "number": "13",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Coldplay",
                    "title": "Adventure Of A Lifetime (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kaskade ft. Tamra Keenan",
                    "title": "Angel On My Shoulder (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lucas & Steve vs. Pep & Rash",
                    "title": "Enigma",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Weeknd",
                    "title": "Can't Feel My Face (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Fedde Le Grand & Nicky Romero",
                "title": "Sparks (Vicetone Remix)",
                "number": "14",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Flo Rida ft. Sage The Gemini",
                    "title": "GDFR",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lou Bega",
                    "title": "Mambo No. 5 (A Little Bit Of...) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kap Slap ft. M. Bronx",
                    "title": "Felt This Good",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DallasK",
                "title": "Orion",
                "number": "15",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Robyn",
                    "title": "Dancing On My Own (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Florian Picasso",
                    "title": "Final Call",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Boxinbox & Lionsize",
                "title": "Ragga Chinese",
                "number": "16",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "50 Cent ft. Olivia",
                    "title": "Candy Shop (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Merk & Kremont",
                "title": "41 Days",
                "number": "17",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Alessia Cara",
                    "title": "Here",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "Peanut Butter Jelly",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Julian Jordan",
                "title": "Rebound",
                "number": "18",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "TLC",
                    "title": "No Scrubs (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo ft. Parson James",
                    "title": "Stole The Show (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Shaun Frank & KSHMR ft. Delaney Jane",
                "title": "Heaven (Two Friends Remix)",
                "number": "19",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Major Lazer ft. Justin Bieber & M\u00d8",
                    "title": "Cold Water (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ZAYN",
                    "title": "Pillowtalk (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Curbi",
                "title": "Triple Six",
                "number": "20",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Two Door Cinema Club",
                    "title": "What You Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Skrillex & JAUZ ft. Fatman Scoop",
                    "title": "Squad Out",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vigel",
                "title": "SQRT",
                "number": "21",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "B.o.B ft. Hayley Williams",
                    "title": "Airplanes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends & Exit Friendzone ft. Natalola",
                    "title": "Overdose (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ODESZA ft. Shy Girls",
                    "title": "All We Need (Dzeko & Torres Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jay Hardway",
                "title": "Stardust",
                "number": "22",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Sean Paul",
                    "title": "Get Busy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tritonal ft. Phoebe Ryan",
                    "title": "Now Or Never (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends & Exit Friendzone ft. Natalola",
                    "title": "Overdose",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers & Tritonal ft. Emily Warren",
                "title": "Until You Were Gone (Justin Caruso Remix)",
                "number": "23",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Halsey",
                    "title": "Gasoline",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Akon",
                    "title": "Sexy Bitch (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris & Disciples ft. Ina Wroldsen",
                "title": "How Deep Is Your Love (eSQUIRE Take Me To Church Bootleg Remix)",
                "number": "24",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Sheppard",
                    "title": "Geronimo",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DubVision",
                "title": "Primer",
                "number": "25",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Police",
                    "title": "Every Breath You Take",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Empire Of The Sun",
                    "title": "We Are The People (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "R3HAB & Quintino",
                    "title": "Freak (VIP Mix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "3LAU & Kill FM",
                "title": "Paralyzed",
                "number": "26",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Mike Posner",
                    "title": "Please Don't Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Smash Mouth",
                    "title": "All Star (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Chico Rose & Joey Dale",
                "title": "Everest",
                "number": "27",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "By The Way (Waiting For) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Killers",
                    "title": "Mr. Brightside (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Florian Picasso",
                "title": "Kirigami",
                "number": "28",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Fifth Harmony ft. Ty Dolla $ign",
                    "title": "Work From Home",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd ft. Foxes",
                    "title": "Clarity (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kryder",
                "title": "Crocodile Tears",
                "number": "29",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Holiday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bombs Away",
                    "title": "Ghetto Blaster",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bro Safari",
                "title": "Snap (Dirty Audio Remix)",
                "number": "30",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Waka Flocka Flame ft. Wale & Roscoe Dash",
                    "title": "No Hands (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Datsik & Bear Grillz",
                "title": "Fuck Off",
                "number": "31",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Rick Astley",
                    "title": "Never Gonna Give You Up (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SNBRN ft. Kerli",
                    "title": "Raindrops (Prince Fox Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Tokyo Machine",
                "title": "PARTY",
                "number": "32",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Wanted",
                    "title": "Glad You Came (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Deorro",
                    "title": "Five Hours",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Posner",
                    "title": "I Took A Pill In Ibiza",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "LORDE",
                "title": "Team (Elephante Remix)",
                "number": "33",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Coldplay",
                    "title": "Paradise (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Empire Of The Sun",
                    "title": "Alive (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "TastyTreat",
                "title": "Ascension",
                "number": "34",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "I Knew You Were Trouble (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Wayne",
                    "title": "A Milli (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ookay",
                    "title": "Thief",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Soulja Boy Tell 'Em",
                    "title": "Crank That (Soulja Boy) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Breathe Carolina & Shanahan ft. HALIENE",
                "title": "Stars & Moon (APEK Remix)",
                "number": "35",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Weeknd",
                    "title": "In The Night (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Otto Knows ft. Avicii",
                    "title": "Back Where I Belong (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "No Money (Two Friends Remix)",
                "number": "36",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Jackson 5",
                    "title": "I Want You Back",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem ft. Rihanna",
                    "title": "Love The Way You Lie",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Krewella",
                    "title": "Alive (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Say Lou Lou",
                    "title": "Julian (The Chainsmokers Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Coldplay",
                    "title": "Viva La Vida (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Halsey",
                "title": "Colors (Audien Remix)",
                "number": "37",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Steve Aoki & Moxie",
                    "title": "I Love It When You Cry (Moxoki) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MAGIC!",
                    "title": "Rude (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "Runaway (U & I)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends & INSTRUM",
                "title": "Trap King (Fetty Wap ft. Adriana Gomez Cover)",
                "number": "38",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Flume ft. Kai",
                    "title": "Never Be Like You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Daya",
                "title": "Don't Let Me Down (Illenium Remix)",
                "number": "39",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Louis The Child ft. K.Flay",
                    "title": "It's Strange",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Breezee",
                "title": "Found Home",
                "number": "40",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Owl City",
                    "title": "Fireflies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alessia Cara",
                    "title": "Wild Things",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake & AlunaGeorge",
                    "title": "You Know You Like It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake ft. Kanye West & Lil Wayne & Eminem",
                    "title": "Forever (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "G-Eazy & Bebe Rexha",
                    "title": "Me, Myself & I (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "Feeling This",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}