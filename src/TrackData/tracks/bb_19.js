export const bb19 = {
    "title": "Two Friends - Big Bootie Mix 019 2021-05-03",
    "tracks": [
        {
            "main": {
                "artist": "Robert B. Sherman & Richard M. Sherman",
                "title": "It's A Small World (After All)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "OutKast",
                    "title": "Roses",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix ft. Bonn",
                    "title": "High On Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "OutKast",
                    "title": "Roses",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SAINt JHN",
                    "title": "Roses (Imanbek Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. ROZES",
                    "title": "Roses (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Benny Blanco & Juice WRLD ft. Brendon Urie",
                    "title": "Roses",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bastille",
                    "title": "Pompeii (SLANDER Heaven Trap Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Machine Gun Kelly & Blackbear",
                    "title": "My Ex's Best Friend",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris ft. John Newman",
                "title": "Blame (Instrumental Mix)",
                "number": "02",
                "timestamp": "2:19"
            },
            "played_with": [
                {
                    "artist": "Eurythmics",
                    "title": "Sweet Dreams (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Conan Gray",
                    "title": "Maniac",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "twoDB ft. Manjo",
                    "title": "Going Down (In My Head) (Over Easy Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "G-Eazy & Bebe Rexha",
                    "title": "Me, Myself & I",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Chess & Fynn ft. Chad Kowal",
                "title": "Edge Of You",
                "number": "03",
                "timestamp": "3:35"
            },
            "played_with": [
                {
                    "artist": "Shwayze & Cisco Adler",
                    "title": "Buzzin'",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jennifer Lopez",
                    "title": "Waiting For Tonight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Yvng Jalape\u00f1o & Allen Mock",
                    "title": "Activate",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "O.T. Genasis",
                    "title": "CoCo (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alex Ross",
                "title": "Close Enough",
                "number": "04",
                "timestamp": "4:55"
            },
            "played_with": [
                {
                    "artist": "Nea",
                    "title": "Some Say",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Travis Scott ft. Kendrick Lamar",
                    "title": "Goosebumps (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Williams & Justin Mylo ft. Sara Sangfelt",
                    "title": "Face Up To The Sun",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. RAS",
                    "title": "The Nights",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Loud Luxury ft. Morgan St. Jean",
                "title": "Aftertaste",
                "number": "05",
                "timestamp": "6:19"
            },
            "played_with": [
                {
                    "artist": "Duke Dumont ft. Jay Norton",
                    "title": "Ocean Drive (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Macklemore & Ryan Lewis ft. Wanz",
                    "title": "Thrift Shop",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto",
                    "title": "The Business (BLVD. Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mac Miller",
                    "title": "The Spins",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Marshmello ft. YUNGBLUD & Blackbear",
                "title": "Tongue Tied",
                "number": "06",
                "timestamp": "7:51"
            },
            "played_with": [
                {
                    "artist": "Miley Cyrus ft. Mike Will Made It",
                    "title": "We Can't Stop",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wiz Khalifa",
                    "title": "Black And Yellow",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone",
                    "title": "Candy Paint",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Jesso & Seeb",
                    "title": "Bigger Than (MVRE Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Good Morning",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Chester Young & Jasted",
                "title": "Sorry",
                "number": "07",
                "timestamp": "8:58"
            },
            "played_with": [
                {
                    "artist": "Leona Lewis",
                    "title": "Bleeding Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "BTS",
                    "title": "Dynamite",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wheatus",
                    "title": "Teenage Dirtbag",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends",
                "title": "Hell (Salasnich Remix)",
                "number": "08",
                "timestamp": "10:22"
            },
            "played_with": [
                {
                    "artist": "Fun.",
                    "title": "Some Nights",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madeon ft. Passion Pit",
                    "title": "Pay No Mind (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "FITZ",
                    "title": "Head Up High (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Michelle Branch",
                    "title": "Everywhere",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ludvigsson & Jorm ft. Vide",
                "title": "Heading Home",
                "number": "09",
                "timestamp": "11:46"
            },
            "played_with": [
                {
                    "artist": "The Band Perry",
                    "title": "If I Die Young",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The All-American Rejects",
                    "title": "Gives You Hell",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ALOK & Sevenn",
                    "title": "The Wall",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake",
                    "title": "0 To 100 (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien & ARTY ft. Ellee Duke",
                "title": "Craving",
                "number": "10",
                "timestamp": "13:13"
            },
            "played_with": [
                {
                    "artist": "Jonas Blue ft. DAKOTA",
                    "title": "Fast Car",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "PUBLIC",
                    "title": "Make You Mine (Put Your Hand in Mine)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Danny Avila ft. Robin Valo & PollyAnna",
                    "title": "Hometown Heroes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gravity & Ed Sheeran",
                    "title": "The A Team",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "CALVO",
                "title": "Broke In Amsterdam (VIP Mix)",
                "number": "11",
                "timestamp": "14:33"
            },
            "played_with": [
                {
                    "artist": "Corinne Bailey Rae",
                    "title": "Put Your Records On",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Bipolar Sunshine",
                    "title": "Middle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Chris Martin",
                    "title": "Heaven",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Going Deeper ft. Sam Tinnesz",
                "title": "Head Up",
                "number": "12",
                "timestamp": "16:16"
            },
            "played_with": [
                {
                    "artist": "Jeremy Zucker",
                    "title": "All The Kids Are Depressed",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rednex",
                    "title": "Cotton Eye Joe (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Keane",
                    "title": "Somewhere Only We Know",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin & Chris Lane",
                "title": "Hold You Tonight",
                "number": "13",
                "timestamp": "17:02"
            },
            "played_with": [
                {
                    "artist": "Drake",
                    "title": "Toosie Slide",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "AFI",
                    "title": "Miss Murder",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chamillionaire ft. Krayzie Bone",
                    "title": "Ridin",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SLUMBERJACK",
                    "title": "RA",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Childish Gambino",
                    "title": "Bonfire",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Icona Pop",
                "title": "All Night",
                "number": "14",
                "timestamp": "18:18"
            },
            "played_with": [
                {
                    "artist": "Tate McRae",
                    "title": "You Broke Me First",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sheryl Crow",
                    "title": "Soak Up The Sun",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo ft. KiFi",
                    "title": "The Same Way",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lauv & Conan Gray",
                    "title": "Fake",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Wildvibes & Patrick Key ft. Lasso The Sun",
                "title": "Meant For You",
                "number": "15",
                "timestamp": "19:44"
            },
            "played_with": [
                {
                    "artist": "Foster The People",
                    "title": "Pumped Up Kicks (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quinn XCII ft. Chelsea Cutler",
                    "title": "Stay Next To Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Niiko x SWAE",
                    "title": "Can't Feel My Face",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cardi B",
                    "title": "Up",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien X MAX",
                "title": "One More Weekend",
                "number": "16",
                "timestamp": "20:52"
            },
            "played_with": [
                {
                    "artist": "Dua Lipa ft. DaBaby",
                    "title": "Levitating",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jimmy Eat World",
                    "title": "The Middle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mac Miller",
                    "title": "Knock Knock",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd ft. Matthew Koma & Miriam Bryant",
                "title": "Find You",
                "number": "17",
                "timestamp": "22:02"
            },
            "played_with": [
                {
                    "artist": "Edward Sharpe & The Magnetic Zeros",
                    "title": "HOME",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Britney Spears",
                    "title": "Baby One More Time",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd ft. Matthew Koma & Miriam Bryant",
                    "title": "Find You (KDrew Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson ft. Urban Cone",
                    "title": "Lionhearted (ARTY Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Weeknd",
                    "title": "Blinding Lights",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Rudeejay & Da Brozz & Chico Rose ft. Robin S",
                "title": "Show Me Love",
                "number": "18",
                "timestamp": "23:32"
            },
            "played_with": [
                {
                    "artist": "Mumford & Sons",
                    "title": "Little Lion Man",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Phoebe Ryan",
                    "title": "Mine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jordan Jay & Jeonghyeon ft. George Redwood",
                    "title": "Love You Right",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MOGUAI ft. Cheat Codes",
                    "title": "Hold On",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Embody",
                "title": "Losing Sleep",
                "number": "19",
                "timestamp": "24:57"
            },
            "played_with": [
                {
                    "artist": "Strumbellas",
                    "title": "Spirits",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "'N Sync",
                    "title": "Bye Bye Bye",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Kid LAROI",
                    "title": "Without You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin & Seven Lions ft. Noah Kahan",
                    "title": "Need Your Love (yetep Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Flo Rida",
                    "title": "Whistle",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "ALOK",
                "title": "Alive (It Feels Like)",
                "number": "20",
                "timestamp": "26:05"
            },
            "played_with": [
                {
                    "artist": "Queen",
                    "title": "I Want To Break Free",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Loona",
                    "title": "Vamos A La Playa",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis & Hook N Sling",
                    "title": "Love On Me (CID Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "jxdn",
                    "title": "Angels & Demons",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mr. Probz",
                "title": "Waves (Robin Schulz Remix)",
                "number": "21",
                "timestamp": "27:29"
            },
            "played_with": [
                {
                    "artist": "Joel Corry X MNEK",
                    "title": "Head & Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Passion Pit",
                    "title": "Sleepyhead",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mesto & Justin Mylo ft. Stef Classens",
                    "title": "When We're Gone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix ft. Macklemore & Patrick Stump of Fall Out Boy",
                    "title": "Summer Days",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Baaku",
                "title": "Bears",
                "number": "22",
                "timestamp": "28:55"
            },
            "played_with": [
                {
                    "artist": "The Fray",
                    "title": "Never Say Never",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Adele",
                    "title": "Hello (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Kid LAROI",
                    "title": "So Done",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sebastian Wibe ft. Jack Dawson",
                "title": "Lies",
                "number": "23",
                "timestamp": "30:00"
            },
            "played_with": [
                {
                    "artist": "Frank Ocean",
                    "title": "Chanel",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bishop Briggs",
                    "title": "River",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Dua Lipa",
                    "title": "Scared To Be Lonely (Brooks Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Nicki Minaj",
                    "title": "Turn Me On (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Robin Schulz ft. James Blunt",
                "title": "OK",
                "number": "24",
                "timestamp": "31:20"
            },
            "played_with": [
                {
                    "artist": "Regard",
                    "title": "Ride It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Olivia Rodrigo",
                    "title": "Drivers License",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Over Easy & Wyle ft. Linney",
                    "title": "Sleeping",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Deorro ft. Chris Brown",
                    "title": "Five More Hours",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kygo & Imagine Dragons",
                "title": "Born To Be Yours",
                "number": "25",
                "timestamp": "32:39"
            },
            "played_with": [
                {
                    "artist": "Quinn XCII & Marc E. Bassy",
                    "title": "Coffee",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Josie Dunne",
                    "title": "Last Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Demi Lovato",
                    "title": "Ok Not To Be Ok (LZRD & TWINSICK Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wes Walker & Dyl",
                    "title": "Jordan Belfort",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ryos",
                "title": "Hey Child",
                "number": "26",
                "timestamp": "33:50"
            },
            "played_with": [
                {
                    "artist": "Cutting Crew",
                    "title": "(I Just) Died In Your Arms",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Toploader",
                    "title": "Dancing In The Moonlight",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Mylo ft. SMBDY",
                    "title": "Still Around",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Macklemore ft. Kesha",
                    "title": "Good Old Days",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Faustix",
                "title": "Need You",
                "number": "27",
                "timestamp": "35:09"
            },
            "played_with": [
                {
                    "artist": "Ti\u00ebsto",
                    "title": "The Business",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Milky Chance",
                    "title": "Stolen Dance (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & Ty Dolla $ign",
                    "title": "The Business, Pt. II",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien & Axis",
                    "title": "Dreams",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zendaya & Zac Efron",
                    "title": "Rewrite The Stars",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lorelei Marcell",
                "title": "Eyes Closed (Embody Remix)",
                "number": "28",
                "timestamp": "36:32"
            },
            "played_with": [
                {
                    "artist": "Phil Collins",
                    "title": "In The Air Tonight (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Billie Eilish",
                    "title": "Therefore I Am",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ke$ha x A$AP Ferg",
                    "title": "Tik Tok Work (Nitti Gritti Bootleg)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake ft. Rick Ross",
                    "title": "Money In The Grave",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso X Charlotte Lawrence",
                "title": "The End",
                "number": "29",
                "timestamp": "37:49"
            },
            "played_with": [
                {
                    "artist": "iLoveMakonnen ft. Drake",
                    "title": "Tuesday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JP Saxe ft. Julia Michaels",
                    "title": "If The World Was Ending",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Embody",
                    "title": "I Miss You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snoop Dogg & Akon",
                    "title": "I Wanna Love You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Midnight Kids",
                    "title": "Monsters",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Boys Like Girls",
                    "title": "Lovedrunk",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kapera",
                "title": "My Love",
                "number": "30",
                "timestamp": "39:09"
            },
            "played_with": [
                {
                    "artist": "Rihanna",
                    "title": "Only Girl (In The World)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "One Direction",
                    "title": "Story Of My Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gotez",
                    "title": "Talking Head",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DVBBS ft. Blackbear & 24kGoldn",
                    "title": "Tinted Eyes",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dualities & Cally Rhodes ft. Cally Rhodes",
                "title": "Sad Song",
                "number": "31",
                "timestamp": "40:29"
            },
            "played_with": [
                {
                    "artist": "Destiny's Child",
                    "title": "Independent Woman",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "D12",
                    "title": "My Band",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SNBRN ft. Kaleena Zanders",
                    "title": "California (Chris Lake & Matroda Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "G-Eazy ft. A$AP Rocky & Cardi B",
                    "title": "No Limit",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Point North",
                "title": "This Will Be My Year",
                "number": "32",
                "timestamp": "41:45"
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Point North",
                    "title": "This Will Be My Year",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD & The Kid LAROI",
                    "title": "Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cash Cash ft. Bebe Rexha",
                    "title": "Take Me Home (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lookas & Crankdat",
                    "title": "Game Over",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kendrick Lamar",
                    "title": "Swimming Pools (Drank)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "CJ",
                    "title": "Whoopty",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "TMW",
                "title": "All Of My Love (Riggi & Piros Remix)",
                "number": "33",
                "timestamp": "43:54"
            },
            "played_with": [
                {
                    "artist": "Zara Larsson",
                    "title": "Ruin My Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Salem Ilese",
                    "title": "Mad At Disney",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fleetwood Mac",
                    "title": "Dreams (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ \u00d6tzi",
                    "title": "Hey Baby",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dario Rodriguez ft. Colorway",
                "title": "In Your Eyes",
                "number": "34",
                "timestamp": "45:14"
            },
            "played_with": [
                {
                    "artist": "Gavin Degraw",
                    "title": "She Sets The City On Fire",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Glass Animals",
                    "title": "Heat Waves",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Stadiumx",
                    "title": "It's Not Right But It's Okay",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MEDUZA ft. Dermot Kennedy",
                    "title": "Paradise",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala ft. Kodaline",
                "title": "All For Love",
                "number": "35",
                "timestamp": "46:30"
            },
            "played_with": [
                {
                    "artist": "Ali Gatie",
                    "title": "What If I Told You That I Love You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lustra",
                    "title": "Scotty Doesn't Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cash Cash ft. Georgia Ku",
                    "title": "Love You Now (LZRD & TWINSICK Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mura Masa ft. Bonzai",
                    "title": "What If I Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends",
                "title": "ID",
                "number": "36",
                "timestamp": "47:39"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "GATT\u00dcSO & Laidback Luke ft. Sarah Reeves",
                "title": "Heart On My Sleeve",
                "number": "37",
                "timestamp": "49:15"
            },
            "played_with": [
                {
                    "artist": "Marshmello & Halsey",
                    "title": "Be Kind",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bon Jovi",
                    "title": "It's My Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Triple M & BRAUNFUFEL & Danny Leax",
                    "title": "Treasured Memories",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MAX ft. Quinn XCII",
                    "title": "Love Me Less",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ava Max",
                "title": "Who's Laughing Now (COASTR. Remix)",
                "number": "38",
                "timestamp": "50:38"
            },
            "played_with": [
                {
                    "artist": "Luke Christopher",
                    "title": "Lot To Learn",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kelly Clarkson",
                    "title": "A Moment Like This",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Wayne ft. Fat Joe",
                    "title": "Make it Rain",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sagan ft. Sam Russell",
                "title": "Lowly (BEAUZ Remix)",
                "number": "39",
                "timestamp": "51:56"
            },
            "played_with": [
                {
                    "artist": "Roddy Ricch ft. Mustard",
                    "title": "High Fashion",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nathan Evans",
                    "title": "Wellerman",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "CALVO & Gigi",
                    "title": "One Thing",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Becky G",
                    "title": "Shower (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso",
                "title": "Destinations",
                "number": "40",
                "timestamp": "53:28"
            },
            "played_with": [
                {
                    "artist": "Bob Dylan",
                    "title": "Blowin In The Wind",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Feist",
                    "title": "1234",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Riggi & Piros ft. Avrii Castle",
                    "title": "Find A Way",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna",
                    "title": "Diamonds (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin ft. Elley Duh\u00e9",
                "title": "Tie Me Down",
                "number": "41",
                "timestamp": "54:47"
            },
            "played_with": [
                {
                    "artist": "Petit Biscuit",
                    "title": "Sunset Lover",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gabby Barrett",
                    "title": "I Hope",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber ft. Chance The Rapper",
                    "title": "Holy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The NGHBRS",
                    "title": "Almost There (PHZES Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lupe Fiasco",
                    "title": "The Show Goes On",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Josie Dunne",
                "title": "Last Day",
                "number": "42",
                "timestamp": "56:04"
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Josie Dunne",
                    "title": "Last Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bazzi ft. Camila Cabello",
                    "title": "Beautiful",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "James Blunt",
                    "title": "You're Beautiful",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin ft. Katie Pearlman",
                    "title": "Nobody Compares To You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bryce Vine",
                    "title": "Drew Barrymore",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Harry Styles",
                    "title": "Watermelon Sugar",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello ft. CHVRCHES",
                    "title": "Here With Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Ayah Marar",
                    "title": "Thinking About You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ed Sheeran",
                    "title": "Shape Of You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Harry Styles",
                    "title": "Adore You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quinn XCII ft. Chelsea Cutler",
                    "title": "Stay Next To Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Illenium ft. X Ambassadors",
                    "title": "In Your Arms",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cutting Crew",
                    "title": "(I Just) Died In Your Arms",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Justin Bieber",
                    "title": "Let Me Love You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & Imagine Dragons",
                    "title": "Born To Be Yours",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ODESZA ft. Zyra",
                    "title": "Say My Name",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chris Brown",
                    "title": "With You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fort Minor ft. Holly Brook & Jonah Matranga",
                    "title": "Where'd You Go (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake ft. Kanye West & Lil Wayne & Eminem",
                    "title": "Forever (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "First Date (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Illenium ft. Jon Bellion",
                    "title": "Good Things Fall Apart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD & The Kid LAROI",
                    "title": "Reminds Me Of You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shawn Mendes",
                    "title": "If I Can't Have You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "3 Doors Down",
                    "title": "Here Without You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}