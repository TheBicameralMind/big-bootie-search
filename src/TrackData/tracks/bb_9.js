export const bb9 = {
    "title": "Two Friends - Big Bootie Mix Volume 9 2016-04-20",
    "tracks": [
        {
            "main": {
                "artist": "Coldplay",
                "title": "Adventure Of A Lifetime (Audien Remix)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Train",
                    "title": "Drops Of Jupiter",
                    "number": "w/",
                    "timestamp": "0:15"
                },
                {
                    "artist": "Capital Cities",
                    "title": "Safe And Sound (Acappella)",
                    "number": "w/",
                    "timestamp": "0:46"
                },
                {
                    "artist": "Martin Garrix ft. Justin Mylo & Mesto",
                    "title": "Bouncybob",
                    "number": "w/",
                    "timestamp": "1:00"
                }
            ]
        },
        {
            "main": {
                "artist": "ASTRVL",
                "title": "Sleepless",
                "number": "02",
                "timestamp": "1:30"
            },
            "played_with": [
                {
                    "artist": "Ti\u00ebsto ft. Matthew Koma",
                    "title": "Wasted (Acappella)",
                    "number": "w/",
                    "timestamp": "1:32"
                },
                {
                    "artist": "ZHU",
                    "title": "Faded (Acappella)",
                    "number": "w/",
                    "timestamp": "2:00"
                },
                {
                    "artist": "Skrillex",
                    "title": "With You, Friends",
                    "number": "w/",
                    "timestamp": "2:30"
                },
                {
                    "artist": "OneRepublic",
                    "title": "If I Lose Myself (Acappella)",
                    "number": "w/",
                    "timestamp": "2:45"
                },
                {
                    "artist": "AronChupa",
                    "title": "I'm An Albatraoz",
                    "number": "w/",
                    "timestamp": "3:00"
                },
                {
                    "artist": "Daya",
                    "title": "Hide Away",
                    "number": "w/",
                    "timestamp": "3:15"
                },
                {
                    "artist": "Porter Robinson & Mat Zo",
                    "title": "Easy",
                    "number": "w/",
                    "timestamp": "3:45"
                }
            ]
        },
        {
            "main": {
                "artist": "Curbi",
                "title": "51",
                "number": "03",
                "timestamp": "4:23"
            },
            "played_with": [
                {
                    "artist": "Madonna",
                    "title": "Music (Acappella)",
                    "number": "w/",
                    "timestamp": "4:30"
                },
                {
                    "artist": "Gym Class Heroes",
                    "title": "Ass Back Home (Acappella)",
                    "number": "w/",
                    "timestamp": "5:00"
                },
                {
                    "artist": "Ghastly",
                    "title": "Get On This",
                    "number": "w/",
                    "timestamp": "5:30"
                },
                {
                    "artist": "Kanye West ft. Lupe Fiasco",
                    "title": "Touch The Sky (Acappella)",
                    "number": "w/",
                    "timestamp": "5:45"
                }
            ]
        },
        {
            "main": {
                "artist": "KAAZE",
                "title": "Tell Me",
                "number": "04",
                "timestamp": "6:00"
            },
            "played_with": [
                {
                    "artist": "3OH!3",
                    "title": "Don't Trust Me",
                    "number": "w/",
                    "timestamp": "6:15"
                },
                {
                    "artist": "Otto Knows ft. Lindsey Stirling & Alex Aris",
                    "title": "Dying For You (The Him Remix)",
                    "number": "w/",
                    "timestamp": "7:00"
                },
                {
                    "artist": "Bastille",
                    "title": "Pompeii (Acappella)",
                    "number": "w/",
                    "timestamp": "7:16"
                }
            ]
        },
        {
            "main": {
                "artist": "Major Lazer ft. Elliphant & Jovi Rockwell",
                "title": "Too Original (Topi & WY Remix)",
                "number": "05",
                "timestamp": "7:31"
            },
            "played_with": [
                {
                    "artist": "Terror Squad ft. Fat Joe & Remy",
                    "title": "Lean Back (Acappella)",
                    "number": "w/",
                    "timestamp": "7:58"
                }
            ]
        },
        {
            "main": {
                "artist": "Vicetone",
                "title": "Pitch Black",
                "number": "06",
                "timestamp": "8:44"
            },
            "played_with": [
                {
                    "artist": "Mike Posner",
                    "title": "I Took A Pill In Ibiza (SeeB Remix)",
                    "number": "w/",
                    "timestamp": "8:58"
                },
                {
                    "artist": "BORGEOUS & Shaun Frank ft. Delaney Jane",
                    "title": "This Could Be Love (Ryos Remix)",
                    "number": "w/",
                    "timestamp": "9:42"
                },
                {
                    "artist": "Martin Solveig",
                    "title": "The Night Out (Acappella)",
                    "number": "w/",
                    "timestamp": "9:58"
                }
            ]
        },
        {
            "main": {
                "artist": "Blink-182",
                "title": "I Miss You (Two Friends Remix)",
                "number": "07",
                "timestamp": "10:21"
            },
            "played_with": [
                {
                    "artist": "Mako",
                    "title": "Smoke Filled Room",
                    "number": "w/",
                    "timestamp": "10:29"
                }
            ]
        },
        {
            "main": {
                "artist": "Oliver Heldens & Throttle",
                "title": "Waiting",
                "number": "08",
                "timestamp": "12:01"
            },
            "played_with": [
                {
                    "artist": "Matthew Koma",
                    "title": "So Fuckin' Romantic",
                    "number": "w/",
                    "timestamp": "12:05"
                },
                {
                    "artist": "Cee-Lo Green",
                    "title": "Fuck You",
                    "number": "w/",
                    "timestamp": "12:35"
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo ft. SUGARWHISKEY",
                "title": "What You Waiting For",
                "number": "09",
                "timestamp": "13:21"
            },
            "played_with": [
                {
                    "artist": "Sander van Doorn & Martin Garrix & DVBBS ft. Aleesia",
                    "title": "Gold Skies (Acappella)",
                    "number": "w/",
                    "timestamp": "13:29"
                },
                {
                    "artist": "Jess Glynne",
                    "title": "Hold My Hand (Acappella)",
                    "number": "w/",
                    "timestamp": "13:58"
                },
                {
                    "artist": "The Offspring",
                    "title": "Self Esteem",
                    "number": "w/",
                    "timestamp": "15:06"
                },
                {
                    "artist": "Blink-182",
                    "title": "First Date (Acappella)",
                    "number": "w/",
                    "timestamp": "15:29"
                },
                {
                    "artist": "Kaskade & CID",
                    "title": "Us",
                    "number": "w/",
                    "timestamp": "15:59"
                }
            ]
        },
        {
            "main": {
                "artist": "Blur",
                "title": "Song 2 (San Holo Remix)",
                "number": "10",
                "timestamp": "16:29"
            },
            "played_with": [
                {
                    "artist": "Selena Gomez",
                    "title": "Hands To Myself",
                    "number": "w/",
                    "timestamp": "16:41"
                },
                {
                    "artist": "Blonde ft. Alex Newell",
                    "title": "All Cried Out (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": "17:22"
                }
            ]
        },
        {
            "main": {
                "artist": "Madison Mars",
                "title": "Theme O",
                "number": "11",
                "timestamp": "18:07"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Wildest Dreams",
                    "number": "w/",
                    "timestamp": "18:11"
                },
                {
                    "artist": "Dem Slackers",
                    "title": "Meow",
                    "number": "w/",
                    "timestamp": "18:42"
                }
            ]
        },
        {
            "main": {
                "artist": "JayKode & Party Thieves",
                "title": "Origin",
                "number": "12",
                "timestamp": "19:27"
            },
            "played_with": [
                {
                    "artist": "Kanye West",
                    "title": "I Love Kanye",
                    "number": "w/",
                    "timestamp": "19:51"
                }
            ]
        },
        {
            "main": {
                "artist": "Third Eye Blind",
                "title": "Semi-Charmed Life (Kasum Remix)",
                "number": "13",
                "timestamp": "20:32"
            },
            "played_with": [
                {
                    "artist": "Hozier",
                    "title": "Someone New",
                    "number": "w/",
                    "timestamp": "20:40"
                },
                {
                    "artist": "Fall Out Boy",
                    "title": "Uma Thurman",
                    "number": "w/",
                    "timestamp": "20:59"
                },
                {
                    "artist": "Michael Jackson",
                    "title": "Man In The Mirror",
                    "number": "w/",
                    "timestamp": "21:42"
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso ft. Matthew Koma",
                "title": "Years (Marco V Instrumental Edit)",
                "number": "14",
                "timestamp": "22:25"
            },
            "played_with": [
                {
                    "artist": "Kelly Clarkson",
                    "title": "Since U Been Gone (Acappella)",
                    "number": "w/",
                    "timestamp": "22:26"
                },
                {
                    "artist": "R. Kelly",
                    "title": "Ignition (Remix) (Kamaura Remix)",
                    "number": "w/",
                    "timestamp": "22:40"
                },
                {
                    "artist": "Village People",
                    "title": "Y.M.C.A.",
                    "number": "w/",
                    "timestamp": "23:25"
                },
                {
                    "artist": "MikeWave",
                    "title": "Come Try This (Electrixx Remix)",
                    "number": "w/",
                    "timestamp": "23:55"
                }
            ]
        },
        {
            "main": {
                "artist": "NIGHTOWLS",
                "title": "33",
                "number": "15",
                "timestamp": "24:24"
            },
            "played_with": [
                {
                    "artist": "Linkin Park",
                    "title": "In The End (Acappella)",
                    "number": "w/",
                    "timestamp": "24:25"
                },
                {
                    "artist": "Dr. Dre ft. Eminem",
                    "title": "Forgot About Dre",
                    "number": "w/",
                    "timestamp": "25:10"
                }
            ]
        },
        {
            "main": {
                "artist": "TroyBoi & Flosstradamus",
                "title": "Soundclash",
                "number": "16",
                "timestamp": "25:38"
            },
            "played_with": [
                {
                    "artist": "Chingy ft. Snoop Dogg & Ludacris",
                    "title": "Holidae Inn",
                    "number": "w/",
                    "timestamp": "25:50"
                }
            ]
        },
        {
            "main": {
                "artist": "Dada Life",
                "title": "One Last Night On Earth (East & Young Remix)",
                "number": "17",
                "timestamp": "26:15"
            },
            "played_with": [
                {
                    "artist": "Basement Jaxx",
                    "title": "Where's Your Head At? (Acappella)",
                    "number": "w/",
                    "timestamp": "26:22"
                },
                {
                    "artist": "K'Naan",
                    "title": "Wavin' Flag",
                    "number": "w/",
                    "timestamp": "26:37"
                },
                {
                    "artist": "Sage The Gemini ft. IAMSU!",
                    "title": "Gas Pedal (Acappella)",
                    "number": "w/",
                    "timestamp": "27:07"
                }
            ]
        },
        {
            "main": {
                "artist": "Alesso ft. Tove Lo",
                "title": "Heroes (We Could Be) (Instrumental Mix)",
                "number": "18",
                "timestamp": "27:38"
            },
            "played_with": [
                {
                    "artist": "Tom Petty",
                    "title": "Free Fallin'",
                    "number": "w/",
                    "timestamp": "27:45"
                },
                {
                    "artist": "The Who",
                    "title": "Baba O'Riley (Acappella)",
                    "number": "w/",
                    "timestamp": "28:31"
                }
            ]
        },
        {
            "main": {
                "artist": "Alex Adair",
                "title": "Make Me Feel Better (Don Diablo & CID Remix)",
                "number": "19",
                "timestamp": "29:15"
            },
            "played_with": [
                {
                    "artist": "Destiny's Child",
                    "title": "Say My Name (Acappella)",
                    "number": "w/",
                    "timestamp": "29:16"
                },
                {
                    "artist": "G-Eazy & Bebe Rexha",
                    "title": "Me, Myself & I (Acappella)",
                    "number": "w/",
                    "timestamp": "29:45"
                }
            ]
        },
        {
            "main": {
                "artist": "Jay Hardway",
                "title": "Electric Elephants",
                "number": "20",
                "timestamp": "30:54"
            },
            "played_with": [
                {
                    "artist": "Iyaz",
                    "title": "Replay",
                    "number": "w/",
                    "timestamp": "31:02"
                },
                {
                    "artist": "Gym Class Heroes ft. Adam Levine",
                    "title": "Stereo Hearts",
                    "number": "w/",
                    "timestamp": "31:16"
                },
                {
                    "artist": "Avicii ft. Simon Aldred",
                    "title": "Waiting For Love (Acappella)",
                    "number": "w/",
                    "timestamp": "31:54"
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Lady Antebellum",
                "title": "Something Better (Two Friends Remix)",
                "number": "21",
                "timestamp": "32:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii ft. Simon Aldred",
                "title": "Waiting For Love",
                "number": "22",
                "timestamp": "33:44"
            },
            "played_with": [
                {
                    "artist": "Galantis",
                    "title": "No Money (Acappella)",
                    "number": "w/",
                    "timestamp": "33:47"
                },
                {
                    "artist": "Rednex",
                    "title": "Cotton Eye Joe",
                    "number": "w/",
                    "timestamp": "34:16"
                },
                {
                    "artist": "Daft Punk",
                    "title": "Harder Better Faster Stronger (Acappella)",
                    "number": "w/",
                    "timestamp": "35:01"
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Ktpearl",
                "title": "Forever",
                "number": "23",
                "timestamp": "35:17"
            },
            "played_with": [
                {
                    "artist": "Lukas Graham",
                    "title": "7 Years",
                    "number": "w/",
                    "timestamp": "35:18"
                },
                {
                    "artist": "Galantis",
                    "title": "No Money (Acappella)",
                    "number": "w/",
                    "timestamp": "36:18"
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii & Nicky Romero ft. Noonie Bao",
                "title": "I Could Be The One (Original Instrumental Mix)",
                "number": "24",
                "timestamp": "36:50"
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. Ktpearl",
                    "title": "Forever (Acappella)",
                    "number": "w/",
                    "timestamp": "37:05"
                },
                {
                    "artist": "Tori Kelly",
                    "title": "Should've Been Us",
                    "number": "w/",
                    "timestamp": "37:50"
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. ROZES",
                "title": "Roses (Two Friends Remix)",
                "number": "25",
                "timestamp": "38:12"
            },
            "played_with": [
                {
                    "artist": "Martin Solveig ft. Dragonette",
                    "title": "Hello (Acappella)",
                    "number": "w/",
                    "timestamp": "38:20"
                },
                {
                    "artist": "Journey",
                    "title": "Don't Stop Believin' (Acappella)",
                    "number": "w/",
                    "timestamp": "38:50"
                }
            ]
        },
        {
            "main": {
                "artist": "Jonas Aden",
                "title": "Temple",
                "number": "26",
                "timestamp": "39:50"
            },
            "played_with": [
                {
                    "artist": "Sum 41",
                    "title": "Fat Lip",
                    "number": "w/",
                    "timestamp": "39:55"
                },
                {
                    "artist": "Shakira",
                    "title": "Whenever, Wherever",
                    "number": "w/",
                    "timestamp": "40:09"
                },
                {
                    "artist": "Khrebto",
                    "title": "The Mad Citizens",
                    "number": "w/",
                    "timestamp": "40:24"
                },
                {
                    "artist": "Natasha Bedingfield",
                    "title": "These Words",
                    "number": "w/",
                    "timestamp": "41:01"
                },
                {
                    "artist": "Elephante ft. Trouze & Damon Sharpe",
                    "title": "Age Of Innocence",
                    "number": "w/",
                    "timestamp": "41:27"
                }
            ]
        },
        {
            "main": {
                "artist": "Dropgun",
                "title": "Together As One",
                "number": "27",
                "timestamp": "42:09"
            },
            "played_with": [
                {
                    "artist": "Bee Gees",
                    "title": "Stayin' Alive (Acappella)",
                    "number": "w/",
                    "timestamp": "42:23"
                },
                {
                    "artist": "Mike Williams",
                    "title": "Sweet & Sour",
                    "number": "w/",
                    "timestamp": "42:55"
                },
                {
                    "artist": "Troye Sivan",
                    "title": "Youth",
                    "number": "w/",
                    "timestamp": "43:27"
                },
                {
                    "artist": "Hellberg",
                    "title": "Slumber Party",
                    "number": "w/",
                    "timestamp": "44:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Elephante",
                "title": "Black Ivory",
                "number": "28",
                "timestamp": "44:43"
            },
            "played_with": [
                {
                    "artist": "Selena Gomez",
                    "title": "Good For You",
                    "number": "w/",
                    "timestamp": "44:54"
                }
            ]
        },
        {
            "main": {
                "artist": "Ti\u00ebsto & Oliver Heldens",
                "title": "Wombass",
                "number": "29",
                "timestamp": "45:46"
            },
            "played_with": [
                {
                    "artist": "Bebe Rexha",
                    "title": "I Can't Stop Drinking About You",
                    "number": "w/",
                    "timestamp": "45:54"
                },
                {
                    "artist": "Ke$ha",
                    "title": "Tik Tok (Acappella)",
                    "number": "w/",
                    "timestamp": "47:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Zomboy",
                "title": "Beast In The Belly",
                "number": "30",
                "timestamp": "47:39"
            },
            "played_with": [
                {
                    "artist": "Eminem",
                    "title": "My Name Is (Acappella)",
                    "number": "w/",
                    "timestamp": "48:12"
                }
            ]
        },
        {
            "main": {
                "artist": "Tony Junior and Riggi & Piros",
                "title": "Make You Go",
                "number": "31",
                "timestamp": "48:53"
            },
            "played_with": [
                {
                    "artist": "Far East Movement ft. The Cataracs & DEV",
                    "title": "Like A G6 (Acappella)",
                    "number": "w/",
                    "timestamp": "49:01"
                },
                {
                    "artist": "Curbi",
                    "title": "51",
                    "number": "w/",
                    "timestamp": "49:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "Runaway (U & I)",
                "number": "32",
                "timestamp": "50:01"
            },
            "played_with": [
                {
                    "artist": "Walk The Moon",
                    "title": "Shut Up And Dance (Acappella)",
                    "number": "w/",
                    "timestamp": "50:18"
                },
                {
                    "artist": "Post Malone",
                    "title": "White Iverson (Acappella)",
                    "number": "w/",
                    "timestamp": "50:50"
                }
            ]
        },
        {
            "main": {
                "artist": "Kap Slap & Gazzo",
                "title": "Rewind (Justin Caruso Remix)",
                "number": "33",
                "timestamp": "51:34"
            },
            "played_with": [
                {
                    "artist": "MNEK & Zara Larsson",
                    "title": "Never Forget You (Acappella)",
                    "number": "w/",
                    "timestamp": "51:41"
                },
                {
                    "artist": "Imagine Dragons",
                    "title": "I Bet My Life",
                    "number": "w/",
                    "timestamp": "52:32"
                }
            ]
        },
        {
            "main": {
                "artist": "Robin Schulz ft. Francesco Yates",
                "title": "Sugar",
                "number": "34",
                "timestamp": "52:45"
            },
            "played_with": [
                {
                    "artist": "Maroon 5",
                    "title": "Sugar (Acappella)",
                    "number": "w/",
                    "timestamp": "52:46"
                }
            ]
        },
        {
            "main": {
                "artist": "Matoma & The Notorious B.I.G. ft. Ja Rule & Ralph Tresvant",
                "title": "Old Thing Back",
                "number": "35",
                "timestamp": "53:38"
            },
            "played_with": [
                {
                    "artist": "Twenty One Pilots",
                    "title": "Stressed Out (Acappella)",
                    "number": "w/",
                    "timestamp": "53:48"
                },
                {
                    "artist": "Dilated Peoples ft. Kanye West",
                    "title": "This Way",
                    "number": "w/",
                    "timestamp": "54:09"
                }
            ]
        },
        {
            "main": {
                "artist": "Point Point",
                "title": "All This",
                "number": "36",
                "timestamp": "55:27"
            },
            "played_with": [
                {
                    "artist": "Sebastian Ingrosso & Alesso ft. Ryan Tedder",
                    "title": "Calling (Lose My Mind) (Acappella)",
                    "number": "w/",
                    "timestamp": "55:30"
                }
            ]
        },
        {
            "main": {
                "artist": "Flo Rida",
                "title": "My House",
                "number": "37",
                "timestamp": "57:06"
            },
            "played_with": [
                {
                    "artist": "Kygo ft. Parson James",
                    "title": "Stole The Show (Acappella)",
                    "number": "w/",
                    "timestamp": "57:07"
                },
                {
                    "artist": "Ti\u00ebsto ft. Michel Zitron",
                    "title": "Red Lights (Acappella)",
                    "number": "w/",
                    "timestamp": "57:27"
                },
                {
                    "artist": "Beyonc\u00e9",
                    "title": "Irreplaceable (Acappella)",
                    "number": "w/",
                    "timestamp": "57:48"
                },
                {
                    "artist": "Mike Posner",
                    "title": "I Took A Pill In Ibiza (SeeB Remix)",
                    "number": "w/",
                    "timestamp": "58:08"
                },
                {
                    "artist": "The Chainsmokers ft. ROZES",
                    "title": "Roses (Acappella)",
                    "number": "w/",
                    "timestamp": "58:20"
                },
                {
                    "artist": "Katy Perry",
                    "title": "Roar (Acappella)",
                    "number": "w/",
                    "timestamp": "58:29"
                },
                {
                    "artist": "Dido",
                    "title": "Thank You (Acappella)",
                    "number": "w/",
                    "timestamp": "59:00"
                }
            ]
        }
    ]
}