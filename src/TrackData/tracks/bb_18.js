export const bb18 = {
    "title": "Two Friends - Big Bootie Mix 018 2020-10-26",
    "tracks": [
        {
            "main": {
                "artist": "The Who",
                "title": "Baba O'Riley",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "Mr. Brightside (Two Friends Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RL Grime ft. 24hrs",
                    "title": "UCLA",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & OneRepublic",
                    "title": "Lose Somebody",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RL Grime ft. 24hrs",
                    "title": "UCLA",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z & Kanye West",
                    "title": "Niggas In Paris (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Cash Cash & Andy Grammer",
                "title": "I Found You",
                "number": "02",
                "timestamp": "2:22"
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "Read My Mind",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "One Direction",
                    "title": "Perfect",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars X Lucas & Steve",
                    "title": "Lunar",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fetty Wap ft. Monty",
                    "title": "My Way (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis & Dolly Parton ft. Mr. Probz",
                "title": "Faith",
                "number": "03",
                "timestamp": "3:47"
            },
            "played_with": [
                {
                    "artist": "Maroon 5",
                    "title": "Misery",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quinn XCII",
                    "title": "Stacy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "SAINt JHN",
                    "title": "Roses (Imanbek Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lodato",
                    "title": "Home",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "3LAU ft. Yeah Boy",
                "title": "On My Mind",
                "number": "04",
                "timestamp": "5:08"
            },
            "played_with": [
                {
                    "artist": "Britney Spears",
                    "title": "Toxic (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bazzi",
                    "title": "Young & Alive",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars ft. Klara",
                    "title": "I Will Let You Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RetroVision ft. Brenton Mattheus",
                    "title": "You & Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Coldplay ft. Beyonc\u00e9",
                    "title": "Hymn For The Weekend",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Stadiumx & Sebastian Wibe ft. Mingue",
                "title": "We Are Life",
                "number": "05",
                "timestamp": "6:33"
            },
            "played_with": [
                {
                    "artist": "Jason Paige",
                    "title": "Pok\u00e9mon Theme",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "NOTD & Felix Jaehn ft. Georgia Ku & Captain Cuts",
                    "title": "So Close",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "FISHER",
                    "title": "Losing It (Yvng Jalape\u00f1o Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Doja Cat",
                    "title": "Boss Bitch",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis & Ship Wrek ft. Pink Sweat$",
                "title": "Only A Fool",
                "number": "06",
                "timestamp": "7:52"
            },
            "played_with": [
                {
                    "artist": "24kGoldn",
                    "title": "City Of Angels",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Harry Styles",
                    "title": "Adore You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madison Mars ft. Klara",
                    "title": "I Will Let You Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Weeknd ft. Daft Punk",
                    "title": "I Feel It Coming (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Sam Vesso",
                "title": "Looking At You (BLVD. Remix)",
                "number": "07",
                "timestamp": "9:16"
            },
            "played_with": [
                {
                    "artist": "Icona Pop ft. Charli XCX",
                    "title": "I Love It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nelly ft. Paul Wall & ALI & Big Gipp",
                    "title": "Grillz",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jack Harlow",
                    "title": "Whats Poppin",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "King Arthur & TRM",
                "title": "Talking About Love",
                "number": "08",
                "timestamp": "10:25"
            },
            "played_with": [
                {
                    "artist": "French Montana ft. Swae Lee",
                    "title": "Unforgettable (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Phoenix",
                    "title": "Lisztomania",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Raven & Kreyn & FractaLL",
                    "title": "Killed It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tyga & Curtis Roach",
                    "title": "Bored In The House",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Justus ft. Maria Mathea",
                "title": "Time To Say Goodbye",
                "number": "09",
                "timestamp": "11:35"
            },
            "played_with": [
                {
                    "artist": "Sugar Ray",
                    "title": "When It's Over",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West ft. Teyana Taylor",
                    "title": "Dark Fantasy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alan Walker & Ava Max",
                    "title": "Alone, Pt. II (RetroVision Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Madonna",
                    "title": "Like A Prayer (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "BRKLYN ft. Jocelyn Alice",
                "title": "Nobody But You",
                "number": "10",
                "timestamp": "13:00"
            },
            "played_with": [
                {
                    "artist": "Fergie ft. Ludacris",
                    "title": "Glamorous",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "J. Cole",
                    "title": "Work Out",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Brooks & KSHMR ft. TZAR",
                    "title": "Voices",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shawn Mendes & Camila Cabello",
                    "title": "Se\u00f1orita",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ryan Riback ft. Ryann",
                "title": "Kinder Eyes",
                "number": "11",
                "timestamp": "14:10"
            },
            "played_with": [
                {
                    "artist": "Afroman",
                    "title": "Crazy Rap",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "24kGoldn",
                    "title": "Valentino",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Matisyahu",
                    "title": "One Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jon Bellion ft. Travis Mendes",
                    "title": "All Time Low (BOXINLION Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "KYLE ft. Lil Yachty",
                    "title": "iSpy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Him ft. Amber Van Day",
                "title": "Tragic",
                "number": "12",
                "timestamp": "15:40"
            },
            "played_with": [
                {
                    "artist": "Chris Brown",
                    "title": "Forever",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Beatles",
                    "title": "Hello, Goodbye",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "twoDB & Andrew Marks",
                    "title": "Amsterdam",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Charlie Puth ft. Tyga",
                    "title": "One Call Away",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Afrojack ft. Mike Taylor",
                "title": "SummerThing!",
                "number": "13",
                "timestamp": "17:03"
            },
            "played_with": [
                {
                    "artist": "Jeremy Zucker",
                    "title": "Comethru",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dan + Shay",
                    "title": "All To Myself",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bassjackers",
                    "title": "All My Life (Lucas & Steve Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Andra Day",
                    "title": "Rise Up",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix ft. Bonn",
                "title": "No Sleep",
                "number": "14",
                "timestamp": "18:35"
            },
            "played_with": [
                {
                    "artist": "Iann Dior ft. Trippie Redd",
                    "title": "Gone Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "I Write Sins Not Tragedies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix ft. Bonn",
                    "title": "No Sleep (DubVision Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bruce Springsteen",
                    "title": "Born in the U.S.A",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Midnight Kids ft. klei",
                "title": "Find Our Way",
                "number": "15",
                "timestamp": "19:57"
            },
            "played_with": [
                {
                    "artist": "DaBaby ft. Roddy Ricch",
                    "title": "ROCKSTAR",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone ft. 21 Savage",
                    "title": "Rockstar (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nickelback",
                    "title": "Rockstar",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo ft. Brando",
                    "title": "Congratulations",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Fischer",
                    "title": "This City",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Ruby Prophet",
                "title": "These Are The Days (Instrumental Mix)",
                "number": "16",
                "timestamp": "21:20"
            },
            "played_with": [
                {
                    "artist": "Lewis Capaldi",
                    "title": "Before You Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Love Lockdown (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mesto ft. Aloe Blacc",
                    "title": "Don't Worry",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lady GaGa & Ariana Grande",
                    "title": "Rain On Me",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Riggi & Piros & VENIICE with Rani",
                "title": "My Feelings",
                "number": "17",
                "timestamp": "22:42"
            },
            "played_with": [
                {
                    "artist": "Andrew McMahon In The Wilderness",
                    "title": "Cecilia And The Satellite",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Derulo",
                    "title": "Watcha Say",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Birdy",
                    "title": "Keeping Your Head Up",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Cheat Codes",
                "title": "On My Life",
                "number": "18",
                "timestamp": "24:06"
            },
            "played_with": [
                {
                    "artist": "Avril Lavigne",
                    "title": "What The Hell",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Counting Crows",
                    "title": "Big Yellow Taxi",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "24kGoldn ft. Iann Dior",
                    "title": "Mood",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Merk & Kremont",
                "title": "Turn It Around",
                "number": "19",
                "timestamp": "25:07"
            },
            "played_with": [
                {
                    "artist": "Ghost Town DJ's",
                    "title": "My Boo (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Christina Aguilera",
                    "title": "Genie In A Bottle",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jonas Blue & RetroVision",
                    "title": "All Night Long",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. Sam Vesso",
                    "title": "Looking At You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Lady Antebellum",
                "title": "Something Better (Two Friends Remix)",
                "number": "20",
                "timestamp": "26:30"
            },
            "played_with": [
                {
                    "artist": "Kygo & Chelsea Cutler",
                    "title": "Not Ok",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Noah Kahan",
                    "title": "Hurt Somebody",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Breathe Carolina",
                    "title": "Dead (Raven & Kreyn Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nicki Minaj ft. Drake & Lil Wayne",
                    "title": "Truffle Butter (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "VAVO & TalkSick",
                "title": "Like Nobody (Kastra Remix)",
                "number": "21",
                "timestamp": "27:32"
            },
            "played_with": [
                {
                    "artist": "Big Sean ft. Drake & Kanye West",
                    "title": "Blessings",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chiddy Bang ft. Icona Pop",
                    "title": "Mind Your Manners",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chance The Rapper ft. Knox Fortune",
                    "title": "All Night (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Kid Quill & New Beat Fund",
                "title": "No Drama (Skulkids Remix)",
                "number": "22",
                "timestamp": "28:57"
            },
            "played_with": [
                {
                    "artist": "Ellie Goulding & Juice WRLD",
                    "title": "Hate Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "a-ha",
                    "title": "Take On Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Derulo",
                    "title": "Ridin' Solo",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lana Del Rey",
                "title": "Summertime Sadness (Cedric Gervais Remix)",
                "number": "23",
                "timestamp": "29:54"
            },
            "played_with": [
                {
                    "artist": "Edward Maya & Vika Jigulina",
                    "title": "Stereo Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Belinda Carlisle",
                    "title": "Heaven Is A Place On Earth",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis & Hook N Sling ft. Dotan",
                    "title": "Never Felt A Love Like This (Raven & Kreyn Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Little Mix",
                    "title": "Shout Out To My Ex",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The All-American Rejects",
                "title": "Gives You Hell",
                "number": "24",
                "timestamp": "31:19"
            },
            "played_with": [
                {
                    "artist": "KYLE ft. Kehlani",
                    "title": "Playinwitme",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rae Sremmurd",
                    "title": "Come Get Her",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sammy Adams ft. Mike Posner",
                    "title": "LA Story",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blackbear",
                    "title": "IDFC",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Justin Bieber",
                    "title": "Let Me Love You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Halsey",
                    "title": "Be Kind (Kayze Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "Not Afraid",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mokita & Goldhouse",
                "title": "I Don't Wanna Know",
                "number": "25",
                "timestamp": "33:00"
            },
            "played_with": [
                {
                    "artist": "Justin Bieber ft. Quavo",
                    "title": "Intentions",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maggie Lindemann",
                    "title": "Pretty Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Castion & Krozz",
                    "title": "Bright Lights",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zara Larsson",
                    "title": "Lush Life",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "One More Chance",
                "number": "26",
                "timestamp": "34:17"
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "Yesterday",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Akon",
                    "title": "Don't Matter",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kastra & Alex Byrne",
                    "title": "Circles",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The 1975",
                    "title": "Chocolate",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Sam Vesso",
                "title": "Looking At You",
                "number": "27",
                "timestamp": "35:36"
            },
            "played_with": [
                {
                    "artist": "Kygo & Whitney Houston",
                    "title": "Higher Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Puff Daddy ft. Faith Evans & 112",
                    "title": "I'll Be Missing You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bastille",
                    "title": "Good Grief",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kygo & Whitney Houston",
                "title": "Higher Love",
                "number": "28",
                "timestamp": "36:47"
            },
            "played_with": [
                {
                    "artist": "Benny Blanco ft. Halsey & Khalid",
                    "title": "Eastside",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Veronicas",
                    "title": "4Ever",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Delaney Jane & Flo Rida & Shaun Frank & Sigala",
                    "title": "You Don't Know Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fergie",
                    "title": "London Bridge",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Zedd ft. Matthew Koma",
                "title": "Spectrum",
                "number": "29",
                "timestamp": "37:55"
            },
            "played_with": [
                {
                    "artist": "Melanie Martinez",
                    "title": "Dollhouse",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber ft. Ludacris",
                    "title": "Baby",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Salasnich & KPLR ft. Kacy Moon",
                    "title": "Live Your Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bleachers",
                    "title": "I Wanna Get Better",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Robbie Williams",
                "title": "The Days",
                "number": "30",
                "timestamp": "39:14"
            },
            "played_with": [
                {
                    "artist": "Topic ft. A7S",
                    "title": "Breaking Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "AJR",
                    "title": "Burn The House Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Robbie Williams",
                    "title": "The Days (Aventry Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Brooks ft. Alida",
                    "title": "Waiting For Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ZAYN & Taylor Swift",
                    "title": "I Don't Wanna Live Forever",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kid Cudi ft. MGMT & Ratatat",
                "title": "Pursuit Of Happiness (Steve Aoki Remix)",
                "number": "31",
                "timestamp": "40:38"
            },
            "played_with": [
                {
                    "artist": "Augustana",
                    "title": "Boston",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin & Seven Lions ft. Noah Kahan",
                    "title": "Need Your Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kid Cudi vs. Martin Garrix",
                    "title": "Pursuit Of Proxy (Riot Ten Mashup)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kid Cudi ft. MGMT & Ratatat",
                    "title": "Pursuit Of Happiness (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "Inside My Head (Voices)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jeremih",
                    "title": "Birthday Sex",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Juice WRLD & Marshmello",
                "title": "Come & Go",
                "number": "32",
                "timestamp": "42:00"
            },
            "played_with": [
                {
                    "artist": "Matoma ft. Josie Dunne",
                    "title": "Sunday Morning",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "will.i.am & Britney Spears",
                    "title": "Scream & Shout (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "TVBOO",
                    "title": "Skrawberries",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cardi B ft. Megan Thee Stallion",
                    "title": "WAP",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dualities ft. Ynnox",
                "title": "A Place For Us",
                "number": "33",
                "timestamp": "43:07"
            },
            "played_with": [
                {
                    "artist": "Lauv & Troye Sivan",
                    "title": "I'm So Tired...",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shaggy",
                    "title": "It Wasn't Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The 1975",
                    "title": "The Sound",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Hogland ft. Kiddo",
                "title": "Letting Go",
                "number": "34",
                "timestamp": "44:32"
            },
            "played_with": [
                {
                    "artist": "NOTD ft. Bea Miller",
                    "title": "I Wanna Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "OneRepublic",
                    "title": "Good Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto",
                    "title": "Grapevine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Doja Cat ft. Nicki Minaj",
                    "title": "Say So (Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Don Diablo ft. Jessie J",
                "title": "Brave",
                "number": "35",
                "timestamp": "45:37"
            },
            "played_with": [
                {
                    "artist": "Cast Of High School Musical",
                    "title": "We're All In This Together (High School Musical OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Whitesnake",
                    "title": "Here I Go Again",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DAZZ & CALVO & Salena Mastroianni",
                    "title": "I Just Got 2",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marvin Gaye",
                    "title": "I Heard It Through The Grapevine",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Young Bombs",
                "title": "Starry Eyes",
                "number": "36",
                "timestamp": "46:55"
            },
            "played_with": [
                {
                    "artist": "Juice WRLD & Marshmello",
                    "title": "Come & Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gorillaz",
                    "title": "Feel Good Inc. (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "T.I.",
                    "title": "Whatever You Like",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cola Splash",
                    "title": "Curry Drinker (Djemba Djemba Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mac Miller",
                    "title": "Donald Trump",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "NOTD ft. Bea Miller",
                "title": "I Wanna Know",
                "number": "37",
                "timestamp": "48:12"
            },
            "played_with": [
                {
                    "artist": "Plies ft. Akon",
                    "title": "Hypnotized",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Evanescence",
                    "title": "Bring Me To Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Surfaces",
                    "title": "Sunday Best",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Party Pupils ft. Gary Go",
                "title": "West Coast Tears",
                "number": "38",
                "timestamp": "49:24"
            },
            "played_with": [
                {
                    "artist": "Doja Cat",
                    "title": "Say So",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "B.o.B. ft. Rivers Cuomo",
                    "title": "Magic",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Axwell \u039b Ingrosso ft. Trevor Guthrie",
                    "title": "Dreamer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ASHE",
                    "title": "Moral Of The Story",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zac Brown Band",
                    "title": "Toes",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Justice Skolnik",
                "title": "Sunshine",
                "number": "39",
                "timestamp": "50:48"
            },
            "played_with": [
                {
                    "artist": "Dixie D'Amelio",
                    "title": "Be Happy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Whethan & Oliver Tree",
                    "title": "When I'm Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Illenium ft. X Ambassadors",
                    "title": "In Your Arms",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Kelsea Ballerini",
                    "title": "This Feeling (Pilton Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avril Lavigne",
                    "title": "Girlfriend",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix ft. John Martin",
                "title": "Higher Ground",
                "number": "40",
                "timestamp": "52:00"
            },
            "played_with": [
                {
                    "artist": "Luniz ft. Michael Marshall",
                    "title": "I Got 5 On It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD",
                    "title": "Robbery",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Simple Plan",
                    "title": "I'm Just A Kid",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "Gold Dust",
                "number": "41",
                "timestamp": "53:24"
            },
            "played_with": [
                {
                    "artist": "U2",
                    "title": "With Or Without You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hinder",
                    "title": "Lips Of An Angel",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "KPLR",
                    "title": "Back & Forth",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo ft. Maty Noyes",
                    "title": "Stay",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends",
                "title": "Hell",
                "number": "42",
                "timestamp": "54:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Codeko",
                "title": "Bad At Being Alone",
                "number": "43",
                "timestamp": "56:14"
            },
            "played_with": [
                {
                    "artist": "Boys Like Girls",
                    "title": "The Great Escape",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "RL Grime & Juelz",
                    "title": "Formula",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "D.R.A.M. ft. Lil Yachty",
                    "title": "Broccoli",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Phoebe Ryan",
                "title": "All We Know",
                "number": "44",
                "timestamp": "57:23"
            },
            "played_with": [
                {
                    "artist": "Clean Bandit ft. Jess Glynne",
                    "title": "Real Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Elton John",
                    "title": "Can You Feel The Love Tonight (The Lion King OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Usher ft. Pitbull",
                    "title": "DJ Got Us Falling In Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & OneRepublic",
                    "title": "Lose Somebody",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lukas Graham",
                    "title": "Love Someone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sigala ft. Bryn Christopher",
                    "title": "Sweet Lovin' (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "3LAU ft. Bright Lights",
                    "title": "How You Love Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber ft. Big Sean",
                    "title": "As Long As You Love Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Usher ft. Young Jeezy",
                    "title": "Love In This Club",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Mraz",
                    "title": "I'm Yours",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Halsey",
                    "title": "Bad At Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Icona Pop ft. Charli XCX",
                    "title": "I Love It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Weeknd",
                    "title": "Can't Feel My Face (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Backstreet Boys",
                    "title": "As Long As You Love Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mike Posner",
                    "title": "Cooler Than Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "R. City ft. Adam Levine & Kardinal Offishall & Popcaan & Agent Sasco",
                    "title": "Locked Away",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Haddaway",
                    "title": "What Is Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Loud Luxury X Anders",
                    "title": "Love No More",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "James Arthur",
                    "title": "Say You Won't Let Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Third Eye Blind",
                    "title": "Never Let You Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "James Bay",
                    "title": "Let It Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Ellie Goulding",
                    "title": "I Need Your Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gryffin & Seven Lions ft. Noah Kahan",
                    "title": "Need Your Love",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}