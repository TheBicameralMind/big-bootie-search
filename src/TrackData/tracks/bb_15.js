export const bb15 = {
    "title": "Two Friends - Big Bootie Mix Volume 15 2019-04-03",
    "tracks": [
        {
            "main": {
                "artist": "Queen",
                "title": "Bohemian Rhapsody (Two Friends Intro Edit)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Loud Luxury ft. Brando",
                    "title": "Body",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Disclosure ft. Sam Smith",
                    "title": "Latch (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone",
                    "title": "White Iverson (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nirvana",
                    "title": "Smells Like Teen Spirit (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zedd & Alessia Cara",
                    "title": "Stay (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "Found You",
                "number": "02",
                "timestamp": "2:11"
            },
            "played_with": [
                {
                    "artist": "Gryffin ft. Bipolar Sunshine",
                    "title": "Whole Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "The Rock Show",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JoJo",
                    "title": "Too Little, Too Late",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bastille",
                "title": "Pompeii (Audien Remix)",
                "number": "03",
                "timestamp": "3:35"
            },
            "played_with": [
                {
                    "artist": "Earth, Wind & Fire",
                    "title": "September (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Neighbourhood",
                    "title": "Sweater Weather",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "John Legend",
                    "title": "All Of Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "Bring The Beat Back",
                "number": "04",
                "timestamp": "5:07"
            },
            "played_with": [
                {
                    "artist": "Halsey",
                    "title": "Without Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jamie Foxx ft. T-Pain",
                    "title": "Blame It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ALOK & Shapeless",
                    "title": "Who Gives",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Big Sean ft. E40",
                    "title": "IDFWU (I Don't Fuck With You) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Kelsea Ballerini",
                "title": "This Feeling (The Lost Boys Remix)",
                "number": "05",
                "timestamp": "6:27"
            },
            "played_with": [
                {
                    "artist": "Ariana Grande",
                    "title": "Thank U, Next",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Good Charlotte",
                    "title": "The Anthem",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dan + Shay",
                    "title": "Speechless",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "Peanut Butter Jelly",
                "number": "06",
                "timestamp": "7:51"
            },
            "played_with": [
                {
                    "artist": "Ke$ha",
                    "title": "Tik Tok (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello ft. Bastille",
                    "title": "Happier",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bassic",
                    "title": "You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Paramore",
                    "title": "Ain't It Fun",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Marshmello ft. Bastille",
                "title": "Happier",
                "number": "07",
                "timestamp": "9:16"
            },
            "played_with": [
                {
                    "artist": "Mason Ramsey",
                    "title": "Famous",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "Scar Tissue",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Yikes",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta & Brooks & Loote",
                "title": "Better When You're Gone",
                "number": "08",
                "timestamp": "10:32"
            },
            "played_with": [
                {
                    "artist": "Uncle Kracker",
                    "title": "Follow Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Journey",
                    "title": "Don't Stop Believin'",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Beyonc\u00e9 ft. JAY Z",
                    "title": "Crazy In Love",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Raven & Kreyn",
                "title": "Biscuit",
                "number": "09",
                "timestamp": "11:55"
            },
            "played_with": [
                {
                    "artist": "Red Hot Chili Peppers",
                    "title": "Dani California",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "5 Seconds of Summer",
                    "title": "Youngblood",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nicki Minaj",
                    "title": "Super Bass",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis",
                "title": "You",
                "number": "10",
                "timestamp": "13:17"
            },
            "played_with": [
                {
                    "artist": "Porter Robinson & Madeon",
                    "title": "Shelter (Acappella)",
                    "number": "w/",
                    "timestamp": "14:08"
                },
                {
                    "artist": "Loud Luxury X Anders",
                    "title": "Love No More",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taylor Swift",
                    "title": "Delicate",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "Anthem (We Love House Music)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Silk City ft. Dua Lipa",
                    "title": "Electricity",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lucas & Steve",
                "title": "Home",
                "number": "11",
                "timestamp": "14:55"
            },
            "played_with": [
                {
                    "artist": "The Chainsmokers ft. Kelsea Ballerini",
                    "title": "This Feeling",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Safety Dance",
                    "title": "Men Without Hats",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jay Hardway & Mesto",
                    "title": "Save Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello & Anne-Marie",
                    "title": "FRIENDS",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Kelsea Ballerini",
                "title": "This Feeling",
                "number": "12",
                "timestamp": "16:13"
            },
            "played_with": [
                {
                    "artist": "Bill Withers",
                    "title": "Lean On Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Missy Elliott",
                    "title": "Work It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson & Madeon",
                    "title": "Shelter (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Kelsea Ballerini",
                    "title": "This Feeling (Young Bombs Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fetty Wap ft. Remy Boyz",
                    "title": "679",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DJ Fresh ft. Ce'Cile",
                "title": "Gold Dust (Flux Pavilion Remix)",
                "number": "13",
                "timestamp": "17:45"
            },
            "played_with": [
                {
                    "artist": "Wiz Khalifa",
                    "title": "We Dem Boyz (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "WTF!? & Dead Prez",
                    "title": "It's Bigger Than Hip Hop UK",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dante Klein",
                "title": "Lost At Sea",
                "number": "14",
                "timestamp": "18:41"
            },
            "played_with": [
                {
                    "artist": "Daughtry",
                    "title": "No Surprise",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gnarls Barkley",
                    "title": "Crazy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Owl City & Carly Rae Jepsen",
                    "title": "Good Time",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Faul & Wad ft. Vertue",
                "title": "Tokyo",
                "number": "15",
                "timestamp": "20:00"
            },
            "played_with": [
                {
                    "artist": "The Fray",
                    "title": "How To Save A Life (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Beatles",
                    "title": "Ticket To Ride",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Foo Fighters",
                    "title": "Learn To Fly (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Marshmello ft. Wrabel",
                    "title": "Ritual",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "NOTD & Felix Jaehn ft. Georgia Ku & Captain Cuts",
                "title": "So Close",
                "number": "16",
                "timestamp": "21:17"
            },
            "played_with": [
                {
                    "artist": "Mark Ronson ft. Miley Cyrus",
                    "title": "Nothing Breaks Like A Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The All-American Rejects",
                    "title": "Dirty Little Secret",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "NOTD & Felix Jaehn ft. Georgia Ku & Captain Cuts",
                    "title": "So Close (Dwilly Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Akon ft. Eminem",
                    "title": "Smack That",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Benny Blanco & Calvin Harris",
                "title": "I Found You",
                "number": "17",
                "timestamp": "22:22"
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "Yellow Submarine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rihanna",
                    "title": "Bitch Better Have My Money (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Benny Blanco & Calvin Harris",
                    "title": "I Found You (Pete Down Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rudimental ft. Ed Sheeran",
                    "title": "Lay It All On Me (Sultan + Shepard Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Benny Blanco & Calvin Harris",
                    "title": "I Found You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jack U\u0308 ft. Justin Bieber",
                    "title": "Where Are \u00dc Now (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alan Walker ft. Gavin James",
                "title": "Tired",
                "number": "18",
                "timestamp": "23:47"
            },
            "played_with": [
                {
                    "artist": "ODESZA ft. Zyra",
                    "title": "Say My Name",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Post Malone & Swae Lee",
                    "title": "Sunflower",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Years & Years",
                    "title": "King (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alan Walker ft. Gavin James",
                    "title": "Tired (Axollo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Miranda Cosgrove",
                    "title": "Leave It All To Me (iCarly Theme Song)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Louis The Child ft. Evalyn",
                "title": "Fire",
                "number": "19",
                "timestamp": "25:05"
            },
            "played_with": [
                {
                    "artist": "Bryce Vine",
                    "title": "Drew Barrymore",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Adele",
                    "title": "Send My Love (To Your New Lover)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MIMS",
                    "title": "This Is Why I'm Hot (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix ft. Khalid",
                "title": "Ocean (Don Diablo Remix)",
                "number": "20",
                "timestamp": "26:26"
            },
            "played_with": [
                {
                    "artist": "Two Friends",
                    "title": "Take It Off",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Adele",
                    "title": "Water Under The Bridge",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vicetone ft. Cozi Zuehlsdorff",
                    "title": "Nevada",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5",
                    "title": "She Will Be Loved",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Disco Killerz & Liquid Todd ft. Hannah Rose",
                "title": "In The Music",
                "number": "21",
                "timestamp": "27:46"
            },
            "played_with": [
                {
                    "artist": "Calvin Harris & Rag'n'Bone Man",
                    "title": "Giant",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "High School Musical Cast",
                    "title": "Breaking Free",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. JUSCOVA & James Delaney",
                    "title": "With My Homies (Jordan Jay Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rain Man ft. OLY",
                    "title": "Bring Back The Summer",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Imagine Dragons",
                "title": "Bad Liar (Salasnich Remix)",
                "number": "22",
                "timestamp": "29:10"
            },
            "played_with": [
                {
                    "artist": "One Direction",
                    "title": "Drag Me Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. JUSCOVA & James Delaney",
                    "title": "With My Homies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dada Life",
                    "title": "Happy Violence",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Sia",
                    "title": "Titanium (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "3LAU & Paris & Simo ft. Bright Lights",
                "title": "Escape",
                "number": "23",
                "timestamp": "30:18"
            },
            "played_with": [
                {
                    "artist": "Jeff Buckley",
                    "title": "Hallelujah",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Charlee",
                    "title": "Inside Out",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cobra Starship ft. Sabi",
                    "title": "You Make Me Feel",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Panic! At The Disco",
                "title": "High Hopes (Two Friends Remix)",
                "number": "24",
                "timestamp": "31:42"
            },
            "played_with": [
                {
                    "artist": "Queen",
                    "title": "Don't Stop Me Now",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Louis Armstrong",
                    "title": "What A Wonderful World",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avril Lavigne",
                    "title": "Complicated",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kaskade & Adam K ft. SunSun",
                "title": "Raining",
                "number": "25",
                "timestamp": "33:05"
            },
            "played_with": [
                {
                    "artist": "Pink Floyd",
                    "title": "Another Brick In The Wall, Pt. 2 (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vengaboys",
                    "title": "Boom, Boom, Boom, Boom!!",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Wolfgang Gartner",
                    "title": "Illmerica",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Linkin Park",
                    "title": "In The End (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vicetone ft. Meron Ryan",
                "title": "Walk Thru Fire",
                "number": "26",
                "timestamp": "34:42"
            },
            "played_with": [
                {
                    "artist": "Gryffin ft. Elley Duh\u00e9",
                    "title": "Tie Me Down",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Glen Eisley",
                    "title": "Sweet Victory",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo ft. Justin Jesso",
                    "title": "Stargazing",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Raven & Kreyn",
                "title": "Muffin",
                "number": "27",
                "timestamp": "35:48"
            },
            "played_with": [
                {
                    "artist": "The Four Tops",
                    "title": "I Can't Help Myself",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Selena Gomez ft. Gucci Mane",
                    "title": "Fetish",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Crazy Town",
                    "title": "Butterfly",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Louis The Child ft. K.Flay",
                "title": "It's Strange",
                "number": "28",
                "timestamp": "37:11"
            },
            "played_with": [
                {
                    "artist": "Rupert Holmes",
                    "title": "Escape (The Pi\u00f1a Colada Song)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Smith",
                    "title": "Stay With Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Biz Markie",
                    "title": "Just A Friend",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sheck Wes",
                "title": "Mo Bamba (Bishu Remix)",
                "number": "29",
                "timestamp": "38:31"
            },
            "played_with": [
                {
                    "artist": "Travis Scott ft. Drake",
                    "title": "Sicko Mode",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Adele",
                "title": "Someone Like You (Vicetone Remix)",
                "number": "30",
                "timestamp": "39:35"
            },
            "played_with": [
                {
                    "artist": "The Lonely Island",
                    "title": "Jizz In My Pants",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Weezer",
                    "title": "Buddy Holly",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Throttle",
                    "title": "Wanderlust",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "ABBA",
                    "title": "Dancing Queen",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Don Diablo & Matt Nash ft. Noonie Bao",
                "title": "Starlight (Could You Be Mine)",
                "number": "31",
                "timestamp": "41:09"
            },
            "played_with": [
                {
                    "artist": "Sean Kingston",
                    "title": "Me Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Modern English",
                    "title": "I Melt With You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo & Matt Nash ft. Noonie Bao",
                    "title": "Starlight (Could You Be Mine) (Otto Knows Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shawn Mendes",
                    "title": "Treat You Better",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jordan Jay",
                "title": "Love For You",
                "number": "32",
                "timestamp": "42:31"
            },
            "played_with": [
                {
                    "artist": "James Newton Howard ft. Jennifer Lawrence",
                    "title": "The Hanging Tree (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ed Sheeran",
                    "title": "Thinking Out Loud",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nick Jonas",
                    "title": "Jealous (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "David Guetta ft. Kid Cudi",
                "title": "Memories",
                "number": "33",
                "timestamp": "43:33"
            },
            "played_with": [
                {
                    "artist": "Loleatta Holloway",
                    "title": "Love Sensation (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Clean Bandit ft. Demi Lovato",
                    "title": "Solo",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Kid Cudi",
                    "title": "Memories (Joe Maz Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & Don Diablo ft. Thomas Troelsen",
                    "title": "Chemicals",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Offspring",
                    "title": "Pretty Fly (For A White Guy)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Krewella",
                "title": "Live For The Night",
                "number": "34",
                "timestamp": "44:58"
            },
            "played_with": [
                {
                    "artist": "Lady GaGa & Bradley Cooper",
                    "title": "Shallow",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Smash Mouth",
                    "title": "I'm A Believer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Louis The Child & Icona Pop",
                    "title": "Weekend",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Andreas Moe",
                "title": "Fade Into Darkness",
                "number": "35",
                "timestamp": "46:05"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "You Belong With Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Andreas Moe",
                    "title": "Fade Into Darkness (Dave Martin Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Miley Cyrus",
                    "title": "Wrecking Ball (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends ft. Dani Poppitt",
                "title": "Dollar Menu",
                "number": "36",
                "timestamp": "47:27"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Calvin Harris & Rag'n'Bone Man",
                "title": "Giant",
                "number": "37",
                "timestamp": "49:00"
            },
            "played_with": [
                {
                    "artist": "Beyonc\u00e9",
                    "title": "Single Ladies (Put A Ring On It) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Macklemore & Ryan Lewis ft. Mary Lambert",
                    "title": "Same Love",
                    "number": "w/",
                    "timestamp": "49:28"
                },
                {
                    "artist": "Calvin Harris & Rag'n'Bone Man",
                    "title": "Giant (Audien Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Taio Cruz",
                    "title": "Dynamite",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "Get Up",
                "number": "38",
                "timestamp": "50:16"
            },
            "played_with": [
                {
                    "artist": "Rihanna",
                    "title": "Rude Boy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lustra",
                    "title": "Scotty Doesn't Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Malaa",
                    "title": "Notorious",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Wayne ft. Swizz Beatz",
                    "title": "Uproar",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Smash Mouth",
                "title": "All Star",
                "number": "39",
                "timestamp": "51:33"
            },
            "played_with": [
                {
                    "artist": "Zedd & Maren Morris & Grey",
                    "title": "The Middle",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Smash Mouth",
                    "title": "All Star (Jajani Bootleg)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Smash Mouth",
                    "title": "All Star (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Going Deeper",
                    "title": "Mighty Fun Splasher",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DEV ft. The Cataracs",
                    "title": "Bass Down Low",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ti\u00ebsto & The Chainsmokers",
                "title": "Split (Only U)",
                "number": "40",
                "timestamp": "53:05"
            },
            "played_with": [
                {
                    "artist": "Fort Minor ft. Holly Brook & Jonah Matranga",
                    "title": "Where'd You Go (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cardi B",
                    "title": "Money",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sagan",
                    "title": "Dance With Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Young Money ft. Lloyd",
                    "title": "BedRock",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Hardwell ft. Jake Reese",
                "title": "Run Wild (Manse Remix)",
                "number": "41",
                "timestamp": "54:48"
            },
            "played_with": [
                {
                    "artist": "The Cars",
                    "title": "Just What I Needed",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jonas Brothers",
                    "title": "Sucker",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tim Berg ft. Amanda Wilson",
                    "title": "Seek Bromance",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix & Troye Sivan",
                "title": "There For You",
                "number": "42",
                "timestamp": "56:09"
            },
            "played_with": [
                {
                    "artist": "Bob Marley",
                    "title": "Stir It Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mac Miller",
                    "title": "Best Day Ever",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Troye Sivan",
                    "title": "There For You (Fairlane Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Can't Tell Me Nothing (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers",
                "title": "Bloodstream",
                "number": "43",
                "timestamp": "57:30"
            },
            "played_with": [
                {
                    "artist": "Black Eyed Peas",
                    "title": "Where Is The Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Khalid",
                    "title": "Location",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Hoobastank",
                    "title": "The Reason",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kings Of Leon",
                    "title": "Sex On Fire (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MAX",
                    "title": "Lights Down Low",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ellie Goulding & Diplo & Swae Lee",
                    "title": "Close To Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Shawn Mendes",
                    "title": "In My Blood",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson",
                    "title": "Sad Machine",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}