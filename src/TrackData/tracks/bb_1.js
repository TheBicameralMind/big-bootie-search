export const bb1 = {
    "title": "Two Friends - Big Bootie Mix Volume 1 2012-05-31",
    "tracks": [
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "Feel So Close",
                "number": "01",
                "timestamp": ""
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "OutKast",
                "title": "Roses",
                "number": "02",
                "timestamp": "0:12"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Tommy Trash vs. Calvin Harris ft. Pnau",
                "title": "Feel So Close 2 Unite Us (Dannic Mashup)",
                "number": "03",
                "timestamp": "1:11"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii & Tom Geiss",
                "title": "Empire Of The Sound",
                "number": "04",
                "timestamp": "3:52"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Britney Spears ft. Nicki Minaj & Kesha",
                "title": "Till The World Ends",
                "number": "05",
                "timestamp": "4:26"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Sia",
                "title": "Titanium (Alesso Remix)",
                "number": "06",
                "timestamp": "6:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Sia",
                "title": "Titanium",
                "number": "07",
                "timestamp": "7:13"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Elton John",
                "title": "Your Song",
                "number": "08",
                "timestamp": "7:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "David Guetta ft. Sia",
                "title": "Titanium (dBerrie Remix)",
                "number": "09",
                "timestamp": "10:02"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Levels",
                "number": "10",
                "timestamp": "12:37"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Loona",
                "title": "Vamos A La Playa (Scotty Radio Edit)",
                "number": "11",
                "timestamp": "13:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Erik Arbores",
                "title": "On The Move (Club Mix)",
                "number": "12",
                "timestamp": "17:20"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Katy Perry ft. Snoop Dogg",
                "title": "California Gurls",
                "number": "13",
                "timestamp": "17:47"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Ehrencrona",
                "title": "This Is So Good",
                "number": "14",
                "timestamp": "20:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Kesha",
                "title": "Blow",
                "number": "15",
                "timestamp": "20:47"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Sex Ray Vision",
                "title": "Blender",
                "number": "16",
                "timestamp": "22:42"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "T-Pain ft. Lil Wayne",
                "title": "Can't Believe It",
                "number": "17",
                "timestamp": "22:46"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Nadia Ali",
                "title": "Rapture (Avicii New Generation Mix)",
                "number": "18",
                "timestamp": "24:46"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fareoh & Archie",
                "title": "2012 (Twenty Twelve)",
                "number": "19",
                "timestamp": "26:37"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Carly Rae Jepsen",
                "title": "Call Me Maybe",
                "number": "20",
                "timestamp": "27:17"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bingo Players",
                "title": "Mode",
                "number": "21",
                "timestamp": "29:53"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Fountains Of Wayne",
                "title": "Stacy's Mom",
                "number": "22",
                "timestamp": "30:09"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Duck Sauce",
                "title": "Barbra Streisand",
                "number": "23",
                "timestamp": "34:12"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Eminem ft. Nate Dogg",
                "title": "Shake That (Acappella)",
                "number": "24",
                "timestamp": "34:23"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Starkillers & Alex Kenji ft. Nadia Ali",
                "title": "Pressure (Alesso Remix)",
                "number": "25",
                "timestamp": "37:03"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bloodhound Gang",
                "title": "The Bad Touch (Acappella)",
                "number": "26",
                "timestamp": "37:29"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Eagles",
                "title": "Hotel California",
                "number": "27",
                "timestamp": "39:29"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Dyro & Jacob van Hage",
                "title": "EMP",
                "number": "28",
                "timestamp": "41:34"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Chris Brown",
                "title": "Yeah 3X (Acappella)",
                "number": "29",
                "timestamp": "42:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hardwell & Dannic",
                "title": "Kontiki",
                "number": "30",
                "timestamp": "45:32"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Afrojack ft. Eva Simons",
                "title": "Take Over Control (Acappella)",
                "number": "31",
                "timestamp": "46:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "R3HAB",
                "title": "The Bottle Song",
                "number": "32",
                "timestamp": "48:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "The Jackson 5",
                "title": "I Want You Back",
                "number": "33",
                "timestamp": "49:06"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Mikael Weermets & Audible ft. Max'C",
                "title": "Free (Dimitri Vangelis & Wyman Remix)",
                "number": "34",
                "timestamp": "50:20"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Mike Posner",
                "title": "Please Don't Go",
                "number": "35",
                "timestamp": "50:36"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Bengali",
                "title": "Not Another Avicii Track",
                "number": "36",
                "timestamp": "52:28"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Robyn",
                "title": "Dancing On My Own (Acappella)",
                "number": "37",
                "timestamp": "52:48"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii ft. Andreas Moe",
                "title": "Fade Into Darkness",
                "number": "38",
                "timestamp": "54:05"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Hardwell",
                "title": "Cobra",
                "number": "39",
                "timestamp": "56:31"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Lil Wayne ft. T-Pain",
                "title": "Got Money",
                "number": "40",
                "timestamp": "56:59"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Swedish House Mafia ft. John Martin",
                "title": "Save The World",
                "number": "41",
                "timestamp": "59:14"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Shaggy",
                "title": "It Wasn't Me",
                "number": "42",
                "timestamp": "59:30"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Swedish House Mafia ft. John Martin",
                "title": "Save The World (Alesso Remix)",
                "number": "43",
                "timestamp": "1:01:44"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Project 46",
                "title": "Iceberg (Unoriginal Mix)",
                "number": "44",
                "timestamp": "1:03:47"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Rihanna ft. Calvin Harris",
                "title": "We Found Love (Acappella)",
                "number": "45",
                "timestamp": "1:04:29"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Jack Back ft. David Guetta & Nicky Romero & Sia",
                "title": "Wild One Two",
                "number": "46",
                "timestamp": "1:06:33"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "LMFAO ft. Lauren Bennett & Goonrock",
                "title": "Party Rock Anthem (Acappella)",
                "number": "47",
                "timestamp": "1:06:47"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Journey",
                "title": "Don't Stop Believin' (Felix Voya Remix)",
                "number": "48",
                "timestamp": "1:09:21"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Levels (Acappella)",
                "number": "49",
                "timestamp": "1:10:48"
            },
            "played_with": []
        }
    ]
}