export const bb12 = {
    "title": "Two Friends - Big Bootie Mix Volume 12 2017-09-08",
    "tracks": [
        {
            "main": {
                "artist": "Carmen Twillie ft. Lebo M.",
                "title": "Circle Of Life (The Lion King OST)",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Post Malone ft. Quavo",
                    "title": "Congratulations (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. James Delaney",
                    "title": "Emily",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avicii ft. Andreas Moe",
                    "title": "Fade Into Darkness (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Manse ft. Alice Berg",
                "title": "Freeze Time (Instrumental Mix)",
                "number": "02",
                "timestamp": "2:07"
            },
            "played_with": [
                {
                    "artist": "AJR",
                    "title": "Weak",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis & Hook N Sling",
                    "title": "Love On Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Ellie Goulding",
                    "title": "Outside (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris ft. Ellie Goulding",
                "title": "Outside",
                "number": "03",
                "timestamp": "3:45"
            },
            "played_with": [
                {
                    "artist": "Michael Jackson",
                    "title": "Beat It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jay Karama",
                    "title": "Keep Calm",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mako",
                "title": "Our Story",
                "number": "04",
                "timestamp": "5:22"
            },
            "played_with": [
                {
                    "artist": "Martin Garrix & Bebe Rexha",
                    "title": "In The Name Of Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cascada",
                    "title": "Everytime We Touch",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Chromeo",
                    "title": "Jealous (I Ain't With It) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Madonna",
                "title": "Ghosttown (Don Diablo Remix)",
                "number": "05",
                "timestamp": "7:03"
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Good Riddance",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sean Kingston",
                    "title": "Take You There",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Dropgun",
                    "title": "Ninja",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "Just Lose It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Armin van Buuren ft. BullySongs",
                "title": "Freefall (Manse Remix)",
                "number": "06",
                "timestamp": "8:29"
            },
            "played_with": [
                {
                    "artist": "Twenty One Pilots",
                    "title": "Tear In My Heart",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Florian Picasso",
                    "title": "Make Up Your Mind",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "NGHTMRE & Boombox Cartel",
                "title": "Aftershock",
                "number": "07",
                "timestamp": "9:53"
            },
            "played_with": [
                {
                    "artist": "Blink-182",
                    "title": "What's My Age Again?",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Aryay",
                    "title": "The Lawnmower",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake ft. Kanye West & Lil Wayne & Eminem",
                    "title": "Forever (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "KYLE ft. Lil Yachty",
                "title": "iSpy (Two Friends Remix)",
                "number": "08",
                "timestamp": "11:15"
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "Can't Buy Me Love",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blink-182",
                    "title": "Bored To Death",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Going Deeper",
                "title": "Little Big Adventure",
                "number": "09",
                "timestamp": "12:41"
            },
            "played_with": [
                {
                    "artist": "Creedence Clearwater Revival",
                    "title": "Have You Ever Seen The Rain?",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Afrojack ft. Stephen Wrabel",
                    "title": "Ten Feet Tall (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Panic! At The Disco",
                    "title": "Victorious",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Vicetone ft. Kat Nestel",
                "title": "Nothing Stopping Me",
                "number": "10",
                "timestamp": "13:57"
            },
            "played_with": [
                {
                    "artist": "The Beach Boys",
                    "title": "Wouldn't It Be Nice",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Harry Styles",
                    "title": "Sign Of The Times",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Breathe Carolina",
                    "title": "Blackout",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Krewella",
                "title": "Alive",
                "number": "11",
                "timestamp": "15:14"
            },
            "played_with": [
                {
                    "artist": "Halsey",
                    "title": "Now Or Never",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Snake ft. Bipolar Sunshine",
                    "title": "Middle (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Roy Orbison",
                    "title": "Pretty Woman",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "NGHTMRE & Carmada ft. Xavier Dunn",
                "title": "Embrace",
                "number": "12",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Ludacris",
                    "title": "Get Back",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jonas Aden",
                "title": "Feel My Soul",
                "number": "13",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Chance The Rapper ft. 2 Chainz & Lil Wayne",
                    "title": "No Problem",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Killers",
                    "title": "Somebody Told Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. MAX",
                    "title": "Pacific Coast Highway (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix & Troye Sivan",
                "title": "There For You (Madison Mars Remix)",
                "number": "14",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "When You Were Young (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kenny Loggins",
                    "title": "Footloose (Footloose OST)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Usher ft. Pitbull",
                    "title": "DJ Got Us Falling In Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "Waves",
                "number": "15",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Chainsmokers",
                    "title": "Honest (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sam Hunt",
                    "title": "House Party",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ke$ha",
                    "title": "Die Young (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "ASTRVL",
                "title": "Elevate",
                "number": "16",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Neighbourhood",
                    "title": "Sweater Weather",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Matoma ft. Astrid S",
                    "title": "Running Out",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The All-American Rejects",
                    "title": "Dirty Little Secret",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision",
                "title": "Here We Go",
                "number": "17",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Mike Posner",
                    "title": "Cooler Than Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Major Lazer ft. Wild Belle",
                    "title": "Be Together (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "A$AP Rocky ft. Drake & Kendrick Lamar & 2 Chainz",
                "title": "Fuckin' Problems",
                "number": "18",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Hardwell",
                    "title": "Spaceman (Carnage Festival Trap Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Twista",
                    "title": "Overnight Celebrity",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Passion Pit",
                "title": "Sleepyhead (Two Friends Remix)",
                "number": "19",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Lauv",
                    "title": "I Like Me Better",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Fifth Harmony ft. Ty Dolla $ign",
                "title": "Work From Home (Young Bombs Remix)",
                "number": "20",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Galantis",
                    "title": "Rich Boy (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Nelly Furtado",
                    "title": "Say It Right (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Disclosure ft. Sam Smith",
                    "title": "Latch (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "JAUZ & Crankdat ft. Slushii",
                "title": "I Hold Still",
                "number": "21",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Idina Menzel",
                    "title": "Let It Go",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta & Showtek ft. VASSY",
                    "title": "BAD (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Brooks",
                    "title": "Pinball",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Thomas Gold ft. M. Bronx",
                "title": "Saints & Sinners (Manse Remix)",
                "number": "22",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Ed Sheeran",
                    "title": "Castle On The Hill (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "All Time Low",
                    "title": "Dear Maria, Count Me In",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alexandra Stan",
                    "title": "Mr. Saxobeat",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Coldplay",
                "title": "A Sky Full Of Stars",
                "number": "23",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Two Friends ft. James Delaney",
                    "title": "Emily",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Coldplay",
                    "title": "A Sky Full Of Stars (Syn Cole Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vicetone ft. Kat Nestel",
                    "title": "Nothing Stopping Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DVBBS & Jay Hardway",
                "title": "Voodoo",
                "number": "24",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Good Charlotte",
                    "title": "Dance Floor Anthem",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sara Bareilles",
                    "title": "Love Song",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Quintino",
                    "title": "Winner",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Martin Garrix",
                "title": "Pizza",
                "number": "25",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Notorious B.I.G. ft. Mase & P. Diddy",
                    "title": "Mo Money Mo Problems",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Robin S",
                    "title": "Show Me Love (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo ft. Maluca",
                    "title": "My Window",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RL Grime & What So Not & Skrillex",
                "title": "Waiting",
                "number": "26",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Eminem",
                    "title": "Cleanin' Out My Closet",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RetroVision & Humain",
                "title": "Sunday",
                "number": "27",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Pat Benatar",
                    "title": "Hit Me With Your Best Shot",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Avril Lavigne",
                    "title": "Here's To Never Growing Up",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Killers vs. Cazzette",
                    "title": "Shot At Night",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix & Dua Lipa",
                    "title": "Scared To Be Lonely (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Osen & Navaz",
                "title": "Wings",
                "number": "28",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Poison",
                    "title": "Every Rose Has Its Thorn",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Rascal Flatts",
                    "title": "Life Is A Highway",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Valentino Khan",
                    "title": "Pump",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "LMFAO",
                    "title": "I'm In Miami Bitch (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Alan Walker ft. Iselin Solheim",
                "title": "Faded (Instrumental Version)",
                "number": "29",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Lonely Island ft. Akon",
                    "title": "I Just Had Sex",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Alan Walker ft. Iselin Solheim",
                    "title": "Faded (Ti\u00ebsto Northern Lights Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Black Eyed Peas",
                    "title": "I Gotta Feeling (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mako",
                "title": "Smoke Filled Room",
                "number": "30",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Clean Bandit ft. Jess Glynne",
                    "title": "Rather Be (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cheat Codes ft. Demi Lovato",
                    "title": "No Promises",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Mako",
                    "title": "Smoke Filled Room (Elephante Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris ft. Florence Welch",
                "title": "Sweet Nothing",
                "number": "31",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Selena Gomez ft. Gucci Mane",
                    "title": "Fetish",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5",
                    "title": "Payphone",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Florence Welch",
                    "title": "Sweet Nothing (Qulinez Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lux Holm",
                "title": "Omega",
                "number": "32",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Killers",
                    "title": "Spaceman (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5",
                    "title": "Maps (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jordi Rivera & Sonny Bass",
                    "title": "Bubblegum",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "OneRepublic",
                    "title": "Stop & Stare (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bingo Players",
                "title": "Mode (Jay Hardway Remix)",
                "number": "33",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Britney Spears ft. Nicki Minaj & Kesha",
                    "title": "Till The World Ends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ylvis",
                    "title": "The Fox (What Does the Fox Say?)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Frank Ocean & Migos",
                    "title": "Slide (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Manse ft. Emily Harder",
                "title": "All Around",
                "number": "34",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "No Doubt",
                    "title": "Hella Good (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lost Kings ft. Katelyn Tarver",
                    "title": "You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. SirenXX",
                    "title": "KANYE (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gryffin & Illenium ft. Daya",
                "title": "Feel Good",
                "number": "35",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "OneRepublic",
                    "title": "If I Lose Myself (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Daniel Powter",
                    "title": "Bad Day",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Audien ft. Lady Antebellum",
                    "title": "Something Better",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Curbi X Mesto",
                "title": "BRUH",
                "number": "36",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Linkin Park ft. Kiiara",
                    "title": "Heavy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & KSHMR ft. VASSY",
                    "title": "Secrets (Don Diablo VIP Mix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "DUNE",
                "title": "Heiress Of Valentina (Alesso Remix)",
                "number": "37",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Oasis",
                    "title": "Champagne Supernova",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Jensen",
                    "title": "Solo Dance",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bassjackers & Brooks",
                    "title": "Joyride",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lady GaGa",
                    "title": "The Cure",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Swedish House Mafia ft. John Martin",
                "title": "Save The World",
                "number": "38",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Katy Perry",
                    "title": "I Kissed A Girl (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Third Eye Blind",
                    "title": "Jumper",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Swedish House Mafia ft. John Martin",
                    "title": "Save The World",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Porter Robinson & Madeon",
                "title": "Shelter",
                "number": "39",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "The Chainsmokers ft. Phoebe Ryan",
                    "title": "All We Know",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Porter Robinson & Madeon",
                    "title": "Shelter (Rvmor Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jay Hardway",
                "title": "Golden Pineapple",
                "number": "40",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Dua Lipa",
                    "title": "New Rules (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Snakehips ft. Tinashe & Chance The Rapper",
                    "title": "All My Friends",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don Diablo",
                    "title": "Momentum",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Lucas & Steve x Mike Williams x Curbi",
                "title": "Let's Go",
                "number": "41",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Shawn Mendes",
                    "title": "There's Nothing Holdin' Me Back (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Owl City & Carly Rae Jepsen",
                    "title": "Good Time",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Christina Aguilera",
                    "title": "Come On Over (All I Want Is You)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers",
                "title": "Honest",
                "number": "42",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Mean",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Macy Gray",
                    "title": "I Try",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers",
                    "title": "Honest (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris ft. Frank Ocean & Migos",
                    "title": "Slide (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers",
                    "title": "Honest (Savi Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Justin Bieber",
                    "title": "Sorry (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers",
                    "title": "Honest (Virtu Remix)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}