export const bb14 = {
    "title": "Two Friends - Big Bootie Mix Volume 14 2018-10-10",
    "tracks": [
        {
            "main": {
                "artist": "Darude",
                "title": "Sandstorm",
                "number": "01",
                "timestamp": ""
            },
            "played_with": [
                {
                    "artist": "Ti\u00ebsto & Dzeko ft. Preme & Post Malone",
                    "title": "Jackie Chan",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ti\u00ebsto & Dzeko ft. Preme & Post Malone",
                    "title": "Jackie Chan",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tove Lo",
                    "title": "Talking Body (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lil Wayne ft. Drake & Future",
                    "title": "Love Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Killers",
                    "title": "Smile Like You Mean It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Galantis & Throttle",
                "title": "Tell Me You Love Me",
                "number": "02",
                "timestamp": "2:12"
            },
            "played_with": [
                {
                    "artist": "Louis The Child ft. Wafia",
                    "title": "Better Not",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "LIT",
                    "title": "My Own Worst Enemy",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jonas Blue ft. Jack & Jack",
                "title": "Rise",
                "number": "03",
                "timestamp": "3:51"
            },
            "played_with": [
                {
                    "artist": "Rihanna ft. JAY Z",
                    "title": "Umbrella",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Good Charlotte",
                    "title": "Lifestyles Of The Rich & Famous",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sage The Gemini",
                    "title": "Now & Later (Henry Fong Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ludacris ft. Pharrell Williams",
                    "title": "Money Maker",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Raven & Kreyn",
                "title": "Bubble",
                "number": "04",
                "timestamp": "5:30"
            },
            "played_with": [
                {
                    "artist": "Camila Cabello",
                    "title": "Never Be The Same",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jonas Blue ft. Jack & Jack",
                    "title": "Rise",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jonas Brothers",
                    "title": "Year 3000",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Mesto",
                "title": "Give Me Love",
                "number": "05",
                "timestamp": "6:53"
            },
            "played_with": [
                {
                    "artist": "Post Malone ft. Ty Dolla $ign",
                    "title": "Psycho",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Bazzi",
                    "title": "Mine",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Lonely Island ft. Justin Timberlake",
                    "title": "Dick In A Box",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala & Ella Eyre & Meghan Trainor ft. French Montana",
                "title": "Just Got Paid",
                "number": "06",
                "timestamp": "8:16"
            },
            "played_with": [
                {
                    "artist": "Loud Luxury ft. Brando",
                    "title": "Body",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Kid Cudi",
                    "title": "Memories (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Menshee",
                    "title": "Shining",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Echosmith",
                    "title": "Cool Kids (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Flux Pavilion",
                "title": "I Can't Stop (Ekali Tribute)",
                "number": "07",
                "timestamp": "9:38"
            },
            "played_with": [
                {
                    "artist": "Ginuwine",
                    "title": "Pony (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Flux Pavilion",
                    "title": "I Can't Stop",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Eminem",
                    "title": "The Real Slim Shady (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Jason Paige",
                "title": "Pok\u00e9mon Theme",
                "number": "08",
                "timestamp": "10:42"
            },
            "played_with": [
                {
                    "artist": "Mike Posner",
                    "title": "Cooler Than Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jason Paige",
                    "title": "Pok\u00e9mon Theme (Lycus Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Lucas & Steve",
                    "title": "Anywhere",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Caesars",
                    "title": "Jerk It Out",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Great Good Fine Ok",
                "title": "Let You Go",
                "number": "09",
                "timestamp": "12:08"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Blank Space (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Anne-Marie",
                    "title": "2002",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Great Good Fine Ok",
                    "title": "Let You Go (Steve James Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z ft. Justin Timberlake",
                    "title": "Holy Grail (acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ti\u00ebsto & Sevenn",
                "title": "BOOM",
                "number": "10",
                "timestamp": "13:30"
            },
            "played_with": [
                {
                    "artist": "Cardi B ft. Bad Bunny & J Balvin",
                    "title": "I Like It",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sam Smith",
                "title": "Too Good At Goodbyes (Galantis Remix)",
                "number": "11",
                "timestamp": "14:22"
            },
            "played_with": [
                {
                    "artist": "Panic! At The Disco",
                    "title": "High Hopes",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Weezer",
                    "title": "Beverly Hills",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Lift Yourself",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Andrew Rayel ft. Jonathan Mendelsohn",
                "title": "Home (Manse Remix)",
                "number": "12",
                "timestamp": "15:32"
            },
            "played_with": [
                {
                    "artist": "Tom Petty",
                    "title": "Learning To Fly",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DJ Khaled ft. Justin Bieber & Chance The Rapper & Quavo",
                    "title": "No Brainer",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Boehm ft. Laurell",
                    "title": "Outside Of The Lines (Boehm VIP Mix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Blackbear",
                    "title": "Do Re Mi",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Calvin Harris",
                "title": "My Way",
                "number": "13",
                "timestamp": "16:58"
            },
            "played_with": [
                {
                    "artist": "OutKast",
                    "title": "Roses",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Gwen Stefani",
                    "title": "Hollaback Girl (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris",
                    "title": "My Way (Ti\u00ebsto Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends ft. James Delaney",
                    "title": "Emily (Clarx Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fifth Harmony ft. Ty Dolla $ign",
                    "title": "Work From Home",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Black Eyed Peas",
                "title": "I Gotta Feeling",
                "number": "14",
                "timestamp": "18:21"
            },
            "played_with": [
                {
                    "artist": "The All-American Rejects",
                    "title": "Swing, Swing",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Don McLean",
                    "title": "American Pie",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Black Eyed Peas",
                    "title": "I Gotta Feeling (DJ Golden Scorpion Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Merk & Kremont ft. DNCE",
                    "title": "Hands Up (Sunstars Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Tyga ft. Offset",
                    "title": "Taste",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Craig David & Sigala",
                "title": "Ain't Giving Up",
                "number": "15",
                "timestamp": "19:58"
            },
            "played_with": [
                {
                    "artist": "Zedd ft. Jon Bellion",
                    "title": "Beautiful Now (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "We The Kings",
                    "title": "Check Yes, Juliet",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "lovelytheband",
                    "title": "Broken",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Sandro Cavazza",
                "title": "Without You",
                "number": "16",
                "timestamp": "21:23"
            },
            "played_with": [
                {
                    "artist": "Plain White T's",
                    "title": "Hey There Delilah",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Deep Blue Something",
                    "title": "Breakfast At Tiffany\u2019s",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sean Kingston",
                    "title": "Fire Burning",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "M83",
                "title": "Midnight City (Eric Prydz Private Remix)",
                "number": "17",
                "timestamp": "22:42"
            },
            "played_with": [
                {
                    "artist": "Avicii ft. Sandro Cavazza",
                    "title": "Without You (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Kanye West ft. Lupe Fiasco",
                "title": "Touch The Sky (Two Friends Remix)",
                "number": "18",
                "timestamp": "23:50"
            },
            "played_with": [
                {
                    "artist": "The Beatles",
                    "title": "Eight Days A Week",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Owl City",
                    "title": "Fireflies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fall Out Boy",
                    "title": "Thnks Fr Th Mmrs (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Holl & Rush & Jordan Jay",
                "title": "Another Day",
                "number": "19",
                "timestamp": "25:27"
            },
            "played_with": [
                {
                    "artist": "Matchbox Twenty",
                    "title": "Unwell",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West & Lil Pump ft. Adele Givens",
                    "title": "I Love It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Beyonc\u00e9",
                    "title": "Formation",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Gazzo ft. Matt McAndrew",
                "title": "Love Drunk",
                "number": "20",
                "timestamp": "26:32"
            },
            "played_with": [
                {
                    "artist": "Taylor Swift",
                    "title": "Our Song",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "David Guetta ft. Rihanna",
                    "title": "Who's That Chick?",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cash Cash ft. Conor Maynard",
                    "title": "All My Love (Audien Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jax Jones ft. RAYE",
                    "title": "You Don't Know Me (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Rihanna ft. Calvin Harris",
                "title": "We Found Love",
                "number": "21",
                "timestamp": "27:47"
            },
            "played_with": [
                {
                    "artist": "Ti\u00ebsto & Dzeko ft. Preme & Post Malone",
                    "title": "Jackie Chan",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Zac Brown Band",
                    "title": "Chicken Fried",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "MARNIK & Blazars",
                    "title": "King In The North",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "DVBBS & CMC$ ft. Gia Koka",
                    "title": "Not Going Home",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dropout",
                "title": "Slowly (Two Friends Remix)",
                "number": "22",
                "timestamp": "29:10"
            },
            "played_with": [
                {
                    "artist": "Paramore",
                    "title": "Still Into You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Miley Cyrus",
                    "title": "Party In The USA",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Miley Cyrus",
                "title": "Party In The USA",
                "number": "23",
                "timestamp": "30:40"
            },
            "played_with": [
                {
                    "artist": "Flume ft. Tove Lo",
                    "title": "Say It (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ookay",
                    "title": "Thief",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Miley Cyrus",
                    "title": "Party In The USA (Dew Tailor Edit)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sagan",
                    "title": "Happiness",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5",
                    "title": "This Summer's Gonna Hurt (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "graves & Coolights",
                "title": "Say Things (Crankdat Re-Crank)",
                "number": "24",
                "timestamp": "32:18"
            },
            "played_with": [
                {
                    "artist": "Katy Perry",
                    "title": "Hot N Cold",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Childish Gambino",
                    "title": "Freaks And Geeks",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "The Chainsmokers ft. Daya",
                "title": "Don't Let Me Down",
                "number": "25",
                "timestamp": "33:26"
            },
            "played_with": [
                {
                    "artist": "Dan + Shay",
                    "title": "Tequila",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Daya",
                    "title": "Don't Let Me Down (T-Mass Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Uncle Kracker",
                    "title": "Drift Away",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii ft. Salem Al Fakir",
                "title": "You Make Me (Instrumental Mix)",
                "number": "26",
                "timestamp": "34:43"
            },
            "played_with": [
                {
                    "artist": "Bon Jovi",
                    "title": "Livin' On A Prayer (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Khalid & Normani",
                    "title": "Love Lies",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kanye West",
                    "title": "Stronger",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Krewella",
                "title": "Enjoy The Ride",
                "number": "27",
                "timestamp": "36:20"
            },
            "played_with": [
                {
                    "artist": "3 Doors Down",
                    "title": "Kryptonite",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Years & Years",
                    "title": "Desire (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Krewella",
                    "title": "Enjoy The Ride (Vicetone Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Black Eyed Peas",
                    "title": "My Humps",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Dada Life",
                "title": "Rolling Stones T-Shirt (Instrumental Mix)",
                "number": "28",
                "timestamp": "37:48"
            },
            "played_with": [
                {
                    "artist": "G-Eazy & Kehlani",
                    "title": "Good Life",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Drew Love",
                    "title": "Somebody",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Codeko",
                    "title": "Lunar",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "OneRepublic",
                    "title": "Wherever I Go",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala ft. Paloma Faith",
                "title": "Lullaby",
                "number": "29",
                "timestamp": "39:04"
            },
            "played_with": [
                {
                    "artist": "Kiiara",
                    "title": "Messy",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Life Of Dillon",
                    "title": "Overload",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sugar Ray",
                    "title": "Fly",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sigala ft. Paloma Faith",
                    "title": "Lullaby (CALVO Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fergie",
                    "title": "London Bridge",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Cash Cash ft. Abir",
                "title": "Finest Hour",
                "number": "30",
                "timestamp": "40:27"
            },
            "played_with": [
                {
                    "artist": "Incubus",
                    "title": "Drive (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Drake",
                    "title": "Best I Ever Had",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Chainsmokers ft. Halsey",
                    "title": "Closer (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Bruno Mars",
                "title": "Treasure (Audien Remix)",
                "number": "31",
                "timestamp": "41:38"
            },
            "played_with": [
                {
                    "artist": "Bon Jovi",
                    "title": "You Give Love A Bad Name",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Foster The People",
                    "title": "Sit Next To Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Garrix ft. Khalid",
                    "title": "Ocean (Don Diablo Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Jay Sean ft. Lil Wayne",
                    "title": "Down",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Ti\u00ebsto ft. Michel Zitron",
                "title": "Red Lights",
                "number": "32",
                "timestamp": "42:46"
            },
            "played_with": [
                {
                    "artist": "Post Malone",
                    "title": "Better Now",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kardinal Offishall ft. Akon",
                    "title": "Dangerous",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "TUJAMO",
                    "title": "Drop That Low (When I Dip)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Childish Gambino",
                    "title": "This Is America",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Party Pupils ft. Audien",
                "title": "This Is How We Do It",
                "number": "33",
                "timestamp": "44:25"
            },
            "played_with": [
                {
                    "artist": "Travie McCoy ft. Bruno Mars",
                    "title": "Billionaire",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Vice ft. Jon Bellion",
                    "title": "Obsession",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Maroon 5 ft. SZA",
                    "title": "What Lovers Do",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "John Newman",
                "title": "Love Me Again",
                "number": "34",
                "timestamp": "45:40"
            },
            "played_with": [
                {
                    "artist": "Galantis ft. Uffie",
                    "title": "Spaceship",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Calvin Harris & Dua Lipa",
                    "title": "One Kiss",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Fun. ft. Janelle Monae",
                    "title": "We Are Young (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "John Newman",
                    "title": "Love Me Again (Steerner Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Selena Gomez",
                    "title": "Back To You",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Louis The Child ft. Caroline Ailin",
                "title": "Last To Leave",
                "number": "35",
                "timestamp": "47:08"
            },
            "played_with": [
                {
                    "artist": "AJR",
                    "title": "I'm Not Famous",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Metro Station",
                    "title": "Shake It",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Ellie Goulding",
                    "title": "On My Mind",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Big Sean",
                    "title": "Dance (A$$) (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Red Hot Chili Peppers",
                "title": "Otherside (Two Friends Remix)",
                "number": "36",
                "timestamp": "48:25"
            },
            "played_with": []
        },
        {
            "main": {
                "artist": "Katy Perry",
                "title": "Birthday (Cash Cash Remix)",
                "number": "37",
                "timestamp": "49:50"
            },
            "played_with": [
                {
                    "artist": "Yeah Yeah Yeahs",
                    "title": "Heads Will Roll (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Kooks",
                    "title": "Naive",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Two Friends",
                    "title": "Bandaid",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Avicii",
                "title": "Levels",
                "number": "38",
                "timestamp": "51:08"
            },
            "played_with": [
                {
                    "artist": "One Direction",
                    "title": "What Makes You Beautiful",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kelis",
                    "title": "Milkshake (Acappella)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Train",
                    "title": "Hey, Soul Sister",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Sigala",
                "title": "Easy Love",
                "number": "39",
                "timestamp": "52:30"
            },
            "played_with": [
                {
                    "artist": "Ed Sheeran",
                    "title": "Galway Girl",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Martin Jensen",
                    "title": "Solo Dance",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Curbi & Fox Stevenson",
                    "title": "Hoohah",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Sugarhill Gang",
                    "title": "Rapper's Delight",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Audien ft. Cecilia Gault",
                "title": "Higher",
                "number": "40",
                "timestamp": "53:53"
            },
            "played_with": [
                {
                    "artist": "Maroon 5 ft. Cardi B",
                    "title": "Girls Like You",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Walk The Moon",
                    "title": "One Foot",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kygo & Miguel",
                    "title": "Remind Me To Forget",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Cheat Codes ft. Demi Lovato",
                "title": "No Promises",
                "number": "41",
                "timestamp": "55:10"
            },
            "played_with": [
                {
                    "artist": "Bebe Rexha ft. Florida Georgia Line",
                    "title": "Meant To Be",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "John Denver",
                    "title": "Take Me Home, Country Roads",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Cheat Codes ft. Demi Lovato",
                    "title": "No Promises (Bassjackers Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Galantis",
                    "title": "Mama Look At Me Now (Culture Code Remix)",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Katrina & The Waves",
                    "title": "Walking On Sunshine",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "Two Friends",
                "title": "Bandaid (Wild Cards Remix)",
                "number": "42",
                "timestamp": "56:38"
            },
            "played_with": [
                {
                    "artist": "Green Day",
                    "title": "Basket Case",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Muse",
                    "title": "Madness",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Sia ft. Sean Paul",
                    "title": "Cheap Thrills",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        },
        {
            "main": {
                "artist": "RL Grime ft. Daya",
                "title": "I Wanna Know",
                "number": "43",
                "timestamp": "57:45"
            },
            "played_with": [
                {
                    "artist": "Lukas Graham",
                    "title": "Mama Said",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "The Fray",
                    "title": "You Found Me",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Juice WRLD",
                    "title": "Lucid Dreams",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Carrie Underwood",
                    "title": "Before He Cheats",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "Kesha",
                    "title": "Praying",
                    "number": "w/",
                    "timestamp": ""
                },
                {
                    "artist": "JAY-Z ft. Kanye West & Rihanna",
                    "title": "Run This Town",
                    "number": "w/",
                    "timestamp": ""
                }
            ]
        }
    ]
}