import { mixes, trackNameMap } from './tracks'

export function search(term, by) {
    let matches = []
    for (const mix in mixes) {
        if (by === 'title') {
            let mix_matches = findTitle(mixes[mix].tracks, term)
            if (mix_matches.length === 0) continue
            
            let match = {cards: []}
            match.mix_name = trackNameMap[mix]

            mix_matches.forEach(el => el.total = mixes[mix].tracks.length)
            match.cards.push(...new Set(mix_matches))

            matches.push(match)

        } else if (by === 'artist') {
            let mix_matches = findArtist(mixes[mix].tracks, term)
            if (mix_matches.length === 0) continue
            
            let match = {cards: []}
            match.mix_name = trackNameMap[mix]

            mix_matches.forEach(el => el.total = mixes[mix].tracks.length)
            match.cards.push(...new Set(mix_matches))

            matches.push(match)
        }
    }
    return matches
}

function findArtist(tracklist, term) {
    let matches = []
    tracklist.forEach(el => {
        if (el.main.artist.toLowerCase().includes(term.toLowerCase())) {
            matches.push(el)
        } else if (el.played_with) {
            el.played_with.forEach(el2 => {
                if (el2.artist.toLowerCase().includes(term.toLowerCase())) {
                    matches.push(el)
                }
            })
        }
    })
    return matches
}

function findTitle(tracklist, term) {
    let matches = []
    tracklist.forEach(el => {
        if (el.main.title.toLowerCase().includes(term.toLowerCase())) {
            matches.push(el)
        } else if (el.played_with) {
            el.played_with.forEach(el2 => {
                if (el2.title.toLowerCase().includes(term.toLowerCase())) {
                    matches.push(el)
                }
            })
        }
    })
    return matches
}