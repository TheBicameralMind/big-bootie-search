import React, { Component, Fragment } from 'react'
import { Accordion, Card, List, Icon } from 'semantic-ui-react'
import { evaluate, round } from 'mathjs'

export default class ResultsList extends Component {
    render() {
        console.log(this.props.content)
        let panels = this.props.content.map(el1 => ({
            key: `<<${Math.random()}>>`,
            title: {
                content: el1.mix_name
            },
            content: {
                content: (
                    <Card.Group> {
                        el1.cards.map(el => {
                            return (
                                <Card fluid>
                                    <Card.Content>
                                        <Card.Header>
                                            <Icon name='music' /> {`${el.main.artist} - ${el.main.title}`}
                                        </Card.Header>
                                        <List ordered relaxed items={
                                            el.played_with.map(el2 => ({
                                                key: `${el2.artist} - ${el2.title} <<${Math.random()}>>`,
                                                content: `${el2.artist} - ${el2.title}`
                                            }))
                                        } />
                                    </Card.Content>
                                    <Card.Content extra>
                                        {`Section ${el.main.number} of ${el.total} (~${round(evaluate(`((${el.main.number} - 1) / ${el.total}) * 100`))}%)${el.main.timestamp ? '; ' + el.main.timestamp : ''}`}
                                    </Card.Content>
                                </Card>
                            )
                        })
                    } </Card.Group>
                )
            }
        }))

        if (this.props.content.length > 0) {
            return (
                <Accordion fluid styled panels={panels} exclusive={false} />
            )
        } else {
            return <Fragment />
        }
    }
}
