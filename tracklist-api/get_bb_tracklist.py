import json

from scraper import parse_tracklist

tracks = [
    'https://www.1001tracklists.com/tracklist/z0mhsf1/two-friends-big-bootie-mix-019-2021-05-03.html',
    'https://www.1001tracklists.com/tracklist/9l2wdv1/two-friends-big-bootie-mix-018-2020-10-26.html',
    'https://www.1001tracklists.com/tracklist/zwf3n2t/two-friends-big-bootie-mix-volume-17-2020-04-09.html',
    'https://www.1001tracklists.com/tracklist/261s43wt/two-friends-big-bootie-mix-volume-16-2019-10-11.html',
    'https://www.1001tracklists.com/tracklist/237tdqmk/two-friends-big-bootie-mix-volume-15-2019-04-03.html',
    'https://www.1001tracklists.com/tracklist/1yl70ql1/two-friends-big-bootie-mix-volume-14-2018-10-10.html',
    'https://www.1001tracklists.com/tracklist/qj4v0wt/two-friends-big-bootie-mix-volume-13-2018-04-04.html',
    'https://www.1001tracklists.com/tracklist/1fsnxchk/two-friends-big-bootie-mix-volume-12-2017-09-08.html',
    'https://www.1001tracklists.com/tracklist/2nvzlh2k/two-friends-big-bootie-mix-episode-11-2017-04-11.html',
    'https://www.1001tracklists.com/tracklist/w1mgcjt/two-friends-big-bootie-mix-volume-10-2016-10-06.html',
    'https://www.1001tracklists.com/tracklist/1n81jy3k/two-friends-big-bootie-mix-volume-9-2016-04-20.html',
    'https://www.1001tracklists.com/tracklist/8ktvhkt/two-friends-big-bootie-mix-vol.-8-2015-09-28.html',
    'https://www.1001tracklists.com/tracklist/66wusst/two-friends-big-bootie-mix-volume-7-2015-04-07.html',
    'https://www.1001tracklists.com/tracklist/3b0k6zk/two-friends-big-bootie-mix-volume-6-2014-08-18.html',
    'https://www.1001tracklists.com/tracklist/hy83dh1/two-friends-big-bootie-mix-volume-5-2014-02-18.html',
    'https://www.1001tracklists.com/tracklist/4fjz021/two-friends-big-bootie-mix-vol.-4-2013-08-15.html',
    'https://www.1001tracklists.com/tracklist/2ckm8bjk/two-friends-big-bootie-mix-volume-3-2013-03-19.html',
    'https://www.1001tracklists.com/tracklist/qgvujwt/two-friends-big-bootie-mix-volume-2-2012-11-03.html',
    'https://www.1001tracklists.com/tracklist/1kh4dbd1/two-friends-big-bootie-mix-volume-1-2012-05-31.html'
]

tracklists = [parse_tracklist(url) for url in tracks[::-1]]

for num, tracklist in enumerate(tracklists):
    with open(f'bb_{num + 1}.json', 'w') as out:
        json.dump(tracklist, out)
    print(f'Wrote Big Bootie {num + 1}')