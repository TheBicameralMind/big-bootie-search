import argparse
import requests
import re
from bs4 import BeautifulSoup
from fake_headers import Headers
from collections import namedtuple
import json


###############  https://stackoverflow.com/a/39235373  ###############
def isnamedtupleinstance(x):
    _type = type(x)
    bases = _type.__bases__
    if len(bases) != 1 or bases[0] != tuple:
        return False
    fields = getattr(_type, '_fields', None)
    if not isinstance(fields, tuple):
        return False
    return all(type(i)==str for i in fields)

def unpack(obj):
    if isinstance(obj, dict):
        return {key: unpack(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [unpack(value) for value in obj]
    elif isnamedtupleinstance(obj):
        return {key: unpack(value) for key, value in obj._asdict().items()}
    elif isinstance(obj, tuple):
        return tuple(unpack(value) for value in obj)
    else:
        return obj
###############  https://stackoverflow.com/a/39235373  ###############

def parse_tracklist(url):
    # response = requests.get(url, headers=Headers().generate())
    response = requests.get(url, headers=Headers().generate())
    soup = BeautifulSoup(response.text, "html.parser")
    if "Error" in soup.title.text:
        raise Exception("Error: Captcha? https://www.1001tracklists.com/")

    mix_title = soup.find('h1', {'id': 'pageTitle'}).text.strip()

    Song = namedtuple('Song', 'artist title number timestamp')
    Segment = namedtuple('Segment', 'main played_with')

    song_rows = soup.find_all('div', {'id': re.compile(r'tlp_[0-9]+')})

    songs = []
    for song in song_rows:
        try:
            number = song.find('span', {'id': re.compile(r'tlp([0-9]+)_tracknumber_value')}).text.strip()
            timestamp = song.find('div', {'id': re.compile(r'cue_([0-9]+)')}).text.strip()

            artist = song.find('meta', {'itemprop': 'byArtist'}).attrs['content']
            title = song.find('meta', {'itemprop': 'name'}).attrs['content'].split(f'{artist} - ')[1]

            songs.append(Song(artist, title, number, timestamp))
        except:
            continue

    current_segment = None
    segments = []
    for song in songs:
        if song.number != 'w/':
            if current_segment:
                segments.append(current_segment)
            current_segment = Segment(song, [])
            continue

        current_segment.played_with.append(song)

    if segments[-1].main != song:
        segments.append(current_segment)

    final_mapping = {'title': mix_title, 'tracks': [unpack(segment) for segment in segments]}

    return final_mapping

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('url', help='1001 Tracklists URL to scrape')
    argparser.add_argument('outfile', help='Destination for 1001 Tracklists JSON data')

    args = argparser.parse_args()

    final = parse_tracklist(args.url)

    with open(args.outfile, 'w') as outf:
        json.dump(final, outf)

    print(f'Wrote tracklist for {final["title"]} to {args.outfile}')