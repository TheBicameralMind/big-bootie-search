module.exports = function override(config, env) {
    config.module.rules.push({
        test: /SearchTracks\.js$/,
        use: { loader: 'workerize-loader' }
      })
    return config;
  }